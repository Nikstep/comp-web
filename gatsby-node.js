const path = require('path')

const formatSlug = slug => {
  const reg = /(\s|\W)/gm

  return slug.replace(reg, '-').toLowerCase()
}

exports.createPages = async ({ graphql, actions }) => {
  const { createPage } = actions
  const caseStudyTemplate = path.resolve('src/templates/caseStudy.tsx')
  const methodStageTemplate = path.resolve('src/templates/methodStage.tsx')
  const methodStageStepTemplate = path.resolve(
    'src/templates/methodStageStep.tsx'
  )
  const researchArticleTemplate = path.resolve(
    'src/templates/researchArticle.tsx'
  )

  const caseStudyQueryResult = await graphql(`
    query {
      allRestApiCaseStudies {
        nodes {
          caseStudyBody {
            _xcomponent
            text
            alignment
            textAlignment
            id
            caption
            media {
              url
            }
            isBanner
          }
          client
          description
          endpointId
          id
          industry {
            title
          }
          cover {
            url
          }
          logo {
            url
          }
          name
          ourApproach
          ourMission
          outcomes {
            id
            list_item
          }
          shortDescription
        }
      }
      allStrapiCaseStudy {
        nodes {
          strapiId
          logo {
            publicURL
          }
          cardImage {
            publicURL
          }
          cover {
            publicURL
          }
          industry {
            id
            title
          }
          isFeatured
          client
          id
          shortDescription
          description
          name
        }
      }
    }
  `)

  const METHOD_STAGES = [
    {
      name: 'Value proposition',
      description:
        'Initial idea validation and market testing where we look at the applicability of a specific idea to the particular market and target demographics.',
    },
    {
      name: 'Product definition',
      description:
        'When we find a market-verified value proposition, we move to define the product and ways of taking it to the market. This usually means building prototypes, smoke tests and understanding launch channels and features.',
    },
    {
      name: 'MVP Launch',
      description:
        'Developers and designers are engaged in building out an MVP while we simultaneously move to launch with early customers.',
    },
    {
      name: 'Further iterations',
      description:
        'When the MVP launch goes well, we move onto scaling up the product based on market feedback. This stage typically includes supporting internal operational processes and product modifications based on early customer feedback.',
    },
  ]

  const { data: guides } = await graphql(`
    query {
      allMarkdownRemark(
        sort: { fields: frontmatter___index }
        filter: { frontmatter: { type: { eq: "Guide" } } }
      ) {
        group(field: frontmatter___category) {
          fieldValue
          nodes {
            html
            frontmatter {
              category
              description
              index
              title
            }
          }
        }
      }
    }
  `)

  const { data: research } = await graphql(`
    {
      allMarkdownRemark(
        sort: { fields: frontmatter___date }
        filter: { frontmatter: { type: { eq: "Research" } } }
      ) {
        nodes {
          frontmatter {
            date(formatString: "x")
            description
            title
            coverImage {
              childImageSharp {
                fixed(jpegQuality: 90, width: 1800) {
                  src
                }
              }
            }
          }
          html
        }
      }
    }
  `)

  caseStudyQueryResult.data.allRestApiCaseStudies.nodes.forEach(item => {
    const {
      cover,
      logo,
    } = caseStudyQueryResult.data.allStrapiCaseStudy.nodes.find(
      node => node.strapiId === item.endpointId
    )

    const coverPublicUrl = cover ? cover.publicURL : undefined
    const logoPublicUrl = logo ? logo.publicURL : undefined

    createPage({
      path: `work/${formatSlug(item.name)}`,
      component: caseStudyTemplate,
      context: {
        approach: item.ourApproach,
        caseStudyBody: item.caseStudyBody,
        client: item.client,
        coverPublicUrl,
        description: item.description,
        endpointId: item.endpointId,
        id: item.id,
        industry: item.industry,
        logoPublicUrl,
        mission: item.ourMission,
        name: item.name,
        outcomes: item.outcomes,
        shortDescription: item.shortDescription,
        caseStudies: caseStudyQueryResult.data.allStrapiCaseStudy.nodes,
      },
    })
  })

  METHOD_STAGES.forEach(({ name, description }) => {
    const articles = guides.allMarkdownRemark.group.find(
      ({ fieldValue }) => fieldValue === name
    )

    createPage({
      path: `method/${formatSlug(name)}`,
      component: methodStageTemplate,
      context: {
        name,
        description,
        articles,
      },
    })

    articles.nodes.forEach(({ frontmatter, html }) => {
      createPage({
        path: `method/${formatSlug(name)}/${formatSlug(frontmatter.title)}`,
        component: methodStageStepTemplate,
        context: {
          frontmatter,
          html,
        },
      })
    })
  })

  research.allMarkdownRemark.nodes.forEach(({ frontmatter, html }) =>
    createPage({
      path: `research/${formatSlug(frontmatter.title)}`,
      component: researchArticleTemplate,
      context: {
        frontmatter,
        html,
      },
    })
  )
}
