module.exports = {
  siteMetadata: {
    title: 'U+ Turning ideas into real products',
    description:
      'We focus on creating new experiences for existing or new customers using a standardized product development process. We typically work wih with Corporate Innovation Labs or Corporate VCs or Startups directly. Delivering Experience & Mindset, Process, Resources.',
    author: '@usertech',
    siteUrl: 'https://u.plus',
  },
  plugins: [
    'gatsby-plugin-react-helmet',
    'gatsby-plugin-styled-components',
    'gatsby-transformer-remark',
    {
      resolve: 'gatsby-source-filesystem',
      options: {
        name: 'images',
        path: `${__dirname}/src/assets/images`,
      },
    },
    {
      resolve: 'gatsby-source-filesystem',
      options: {
        path: `${__dirname}/src/assets/methodGuides`,
      },
    },
    {
      resolve: 'gatsby-source-filesystem',
      options: {
        path: `${__dirname}/src/assets/researchArticles`,
      },
    },
    {
      resolve: 'gatsby-source-filesystem',
      options: {
        path: `${__dirname}/src/assets/researchArticles`,
      },
    },
    {
      resolve: 'gatsby-source-strapi',
      options: {
        apiURL: 'https://strapi.u.plus',
        contentTypes: ['person', 'industry', 'job', 'case-study'],
        queryLimit: 1000,
      },
    },
    {
      resolve: 'gatsby-source-rest-api',
      options: {
        endpoints: [
          'https://strapi.u.plus/case-studies',
          'https://strapi.u.plus/blog-posts',
        ],
      },
    },
    {
      resolve: 'gatsby-source-graphql',
      options: {
        typeName: 'STRAPI',
        fieldName: 'strapi',
        url: 'https://strapi.u.plus/graphql',
      },
    },
    {
      resolve: 'gatsby-plugin-manifest',
      options: {
        name: 'U+ Web',
        short_name: 'U+',
        start_url: '/',
        background_color: '#ffffff',
        theme_color: '#0000FF',
        display: 'browser',
        icon: 'src/assets/images/favicon.png',
      },
    },
    {
      resolve: 'gatsby-plugin-react-svg',
      options: {
        rule: {
          include: /assets/,
        },
      },
    },
    'gatsby-transformer-sharp',
    'gatsby-plugin-sharp',
    {
      resolve: `gatsby-transformer-remark`,
      options: {
        plugins: [
          'gatsby-remark-external-links',
          {
            resolve: `gatsby-remark-images`,
            options: {
              maxWidth: 800,
              linkImagesToOriginal: false,
              showCaptions: true,
              quality: 90,
              wrapperStyle: 'margin: 4rem 0;',
            },
          },
          {
            resolve: `gatsby-remark-footnotes`,
            options: {
              footnoteBackRefPreviousElementDisplay: 'inline',
              footnoteBackRefDisplay: 'inline',
              footnoteBackRefInnerText: '^',
              footnoteBackRefAnchorStyle: `text-decoration: none;`,
              footnoteBackRefInnerTextStartPosition: 'front',
            },
          },
        ],
      },
    },
    'gatsby-plugin-transition-link',
    {
      resolve: `gatsby-plugin-portal`,
      options: {
        key: 'strip',
        id: 'strip',
      },
    },
    {
      resolve: `gatsby-plugin-portal`,
      options: {
        key: 'cookieBar',
        id: 'cookieBar',
      },
    },
    {
      resolve: `gatsby-plugin-portal`,
      options: {
        key: 'menu',
        id: 'menu',
      },
    },
    {
      resolve: 'gatsby-plugin-ts',
      options: {
        codegen: false,
      },
    },
    {
      resolve: `gatsby-plugin-google-analytics`,
      options: {
        trackingId: 'UA-11466664-1',
        anonymize: true,
        respectDNT: true,
        pageTransitionDelay: 250,
      },
    },
    'gatsby-plugin-sitemap',
  ],
}
