---
type: Guide
category: MVP Launch
index: 3
title: Internal development team or External agency
description: Technological agencies are a versatile tool to have in your toolbox. They can do a range of things, starting with creating a prototype in a matter of hours to taking care of all your technological needs so you can focus on what you’re best at. As with almost everything in the startup world, making a good decision depends on a sober analysis of your current situation and the resources that are available to you.
---

### Dream team?

Today, having a highly professional and skilled development team is a fortune in itself. It gives your company many advantages and maybe just the competitive edge it needs. It saves you lots of money. You can take measures to ensure the product is quality and the whole development is more agile with fewer communication barriers. Unfortunately, though, those advantages come only if you already have a team that’s highly skilled, highly professional, and highly experienced. The establishment of a development team is actually the hardest part. Nowadays, finding great developers is harder than getting enough sleep, and you need great developers if you want to succeed in the startup world.

### You need a lead developer

If you’re not the “tech guy”, you need someone who will be the tech guy for you. This person must have wide-ranging knowledge across multiple platforms (web frontend, backend, iOS, android, data science… whatever platform you’re planning to develop on) and be able to make the big engineering decisions. He should also be a good manager and even better teacher. In engineering, one technological decision can be the difference between finishing the product 6 months earlier and not finishing at all; that’s why it’s so important to have a great lead developer.

Do you already have your lead developer? Great! With years of practice in this crazy field, he has probably made lots of connections and should be able to recruit or at least recommend you senior developers. No recruiting agency can even get close to the results of a lead developer’s personal connections, plus, in the early stages of your startup, he can also take the role of a senior developer on one of the platforms.

### You need a senior developer for each platform

First of all, let me debunk a common misconception. Two junior developers do not make up for one senior developer, not even three, not even five. A senior developer is much more than just a fast coder. Not having one will probably result in your startup throwing in the towel before finishing its MVP, and if you’re actually lucky enough to finish an MVP, it’ll be buggy, non-scalable, and prime for rewriting.

This may sound a bit scary, but don’t worry. Different outcomes can arise and there is a solution for all of them. You either:

### Have a lead developer and senior developers for each platform and trust them.

Great, you’re in the sweet spot, and if you followed the tips outlined here, they are probably responsible professionals and ready to take care of what’s necessary. In this case, think of agencies as a complementary tool to your A-team. In a perfect world, this A-team should now be working on everything, but sometimes smaller things that need doing will pop up or something unexpected that needs immediate attention happens. So what do you do? Wait until your main product is finished? That could take months. Assign one of your engineers a different task for the next three days? That would only make him unfocused. Make somebody stay overtime? Tired and unhappy employees=bad quality.

### Hire an agency

You can’t really afford wasting the precious time and expertise of your A-team. Consider these scenarios:

- You need your landing page redesigned before meeting an investor later this week.

- Your senior Android developer quit a month before release.

- You’ve got a genius idea for a smaller app that would support your main product.

A great response to all of these scenarios would be to hire an agency. You don’t have to do everything yourself. Making sure critical parts of your app are delivered by people you trust the most is a great idea, but those people can’t do everything all the time, and it would be a pity to waste an opportunity because you didn’t have enough people at the time.

### You have some of the people mentioned above, but are missing some.

That’s a tough one. You have qualified people you want to work with, but not enough of them. Trying to find key people might take months without the right connections, and nobody has that much free time on their hands. What you need to do is to focus on the platforms you can do right now and grow your team steadily. Consider this example:

You’re making a killer mobile app and you have your lead developer specialized in backend, one senior developer specialized in iOS (and a few junior developers to help them out), however, there is also this weird platform called Android, which is used by a significant portion of your userbase, but it’s been a month now, and you can’t find a suitable candidate to lead Android development. Should you?

a) Wait months until you find one

b) Bail on 30% of your potential customers

c) GIve up and get a job at McDonald’s

d) Hire an agency

Don’t get me wrong, the first thing you should do is start looking for great Android developers. Just don’t bet everything on finding one in the next month—not even mentioning the fact that you want to try out your developers before giving them the important task of laying the foundation stones of your app. Hire an agency to start working on the Android app, let your new Android developers collaborate with the agency when you find them and then just take after the agency when your new Android team has proven capable.

I can’t stress enough how much money and time this can save you.

### You have neither the lead developer nor senior developers

This is okay as well. Focus on what you’re good at and outsource the technological part to somebody more experienced. Do some deep research into what agency would best suit your needs, and when you find the one you’re truly comfortable with, trust them to do their job.

This is literally what agencies are for and what they love doing most.
