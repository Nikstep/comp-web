---
type: Guide
category: MVP Launch
index: 5
title: Launch Metrics
description: In the previous stage, you prepared a marketing strategy according to the STDC framework. In this stage, you’ll focus on the SEE, THINK, and DO parts. CARE you can leave for later. Now we’re dealing mainly with the acquisition. The goal is to make your marketing costs lower than your profit. If you do, that's half the battle.
---

### CLV

You must first determine Customer lifetime value (CLV). And how to calculate it correctly? You just have to guess because you do not have enough data yet. There are many complex patterns but use common sense. Calculate it simply by multiplying the expected number of months and revenue from each user. This number must be less than the Acquisition Cost.

### Acquisition cost

Acquisition cost isn’t easy to calculate because the user can reach you through many channels. For a more accurate calculation, complex attribution models are used, but this is a waste of time at this stage. At the end of the week, simply look at how much you've invested in all your campaigns and how much you've earned your customers. If your marketing expense is higher than your earnings, don’t panic. This may be because users are interested in the product, but you have barriers in the acquisition process they cannot overcome. It is important to focus on optimizing the conversion rate in this case.

### Analytics

Set up Google Analytics properly and especially conversion points. You do not have to measure everything from the beginning, but the conversion, yes. Especially the conversion between visit and sign up and between sign up and purchase. A funnel will be created and you will quickly see where your are losing customers.

It's about quickly testing the various channels and measuring. If you invest in PPC for a long time and don’t get paid customers, it’s a sign that no one wants your product. You messed up validation phase, or have problems with UX. Then it is better to stop the channel and evaluate the problem.
