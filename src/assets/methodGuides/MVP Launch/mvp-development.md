---
type: Guide
category: MVP Launch
index: 10
title: MVP Development
description: It is happening! You are finally developing your product! Congratulations! It is now imperative that you follow the development process exactly.
---

### IT project-management tools

It is necessary to choose a tool that’s ready for software development. From the developer’s perspective, it should at least support the following features, which can save a lot of time for both developers and managers:

- GIT(or any other version control system) integration

- Workflow support

  - Based on the events from integrated GIT

You can also find many tools when you google “issue-tracking software.” Based on our experience, we suggest using Atlassian's Jira because it’s very feature-rich and supports high customization if anything is needed.

### Development process

There is no way to describe the optimal development process. This really depends on the size of the team, given technology, product, given timeline, given budget, etc. However, there are at least a few major best practices in development that should always be included or used.

**Where to store code and version control**

When dealing with code, you should use a VCS (version control system) and most people use a CVS (concurrent version system). This allows developers to work concurrently on the code and handle cooperation flawlessly. You can easily switch back and forth between different versions of source code. A popular CVS is Git, which was written by Linus Torvalds, who built it for the Linux Kernel project. It’s designed to be quick and manage large codebases effortlessly. It was also popularized by services like Github, Gitlab, or Bitbucket. Git is the most popular CVS today, so it’s definitely a good way to go when considering where to store your code. We recommend using Git for these reasons:

- Almost any issue-tracking tool supports Git integration

- It has all the features needed for correct workflow

- here a lot of cloud services offering Git hosting:

  - Bitbucket

  - Github

The bigger question than which CVS to use, is whether to use a managed service, or take care of the infrastructure on your own. For smaller, less critical projects, you can use services like Github or Gitlab. For larger projects, it might be wise to save the code on your dedicated servers. A Gitlab project, for example, offers the opportunity to use it on your own server. The disadvantage of this approach is that you need to manage the machine, but advantage is that you have full control over the system which might be required if you have sensitive code or proprietary code that you don’t want anybody to read and misuse.

**Testing**

There are two ways to test your application

- Manual

- Automatic

And both should always be included! Developers usually try to avoid writing automated tests due to lack of time, but that doesn’t make any sense. The truth is that no code is bug-free even if you have the best possible developers. Writing automated tests can consume more time in the early stages of the project, but in the long run, saves hundreds of hours. It helps the developers ensure that their changes don’t break anything in the existing code and prevents publishing code that won’t work correctly.

**Code reviews**

This is the best practice for involving more developers into the development of each feature to ensure:

- The coding style follows your team’s standard

- The code is written with best practices in mind

- The code is understandable by others

- The code doesn’t have obvious bugs

More than just one person understands the code, so changes can be made if needed

In general, this means that the code is checked by at least one other developer before it’s included in the main code base. This workflow can be easily implemented using GIT and best practices as covered in this article http://nvie.com/posts/a-successful-git-branching-model/

**Deployment and continuous integration**

Deployment always has to be automated. Deployment usually consists of many steps that need to be done in a given order, and to avoid any mistakes it should be automated and done using a single command. Using the right tool depends on the technology that is chosen.

Continuous integration helps with the integration of changes into the code. It can do a lot of useful things, but at least you should make sure it:

- Automatically checks for any changes in the code

- Runs the automated tests

- Deploys on the changes in the test server

You can choose between a lot of tools and services to implement CI and they all support the basic features mentioned above. We would suggest using one of these:

- Jenkins - If you want to run the CI on your server
- Travis CI or CircleCI - If you want to use a CI server as a SaaS

  
