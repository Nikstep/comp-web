---
type: Guide
category: MVP Launch
index: 7
title: Designing the System Architecture
description: Before you develop the project, you have to think about what which tools and technologies you’re going to use. It always helps to write it down and think twice. Reworking something which is already done in this stage can be very pricey.
---

### Architecture

The monolithic and service-oriented approaches are the two most common design patterns that developers usually choose.

**Monolithic**

This means the software is developed as a single application, with a single codebase.

**Service-oriented**

The software is split into multiple components/services/applications. Each one can be written using different technologies which communicate with each other using APIs.

**Which one should I choose?**

Both have pros and cons. With service-oriented design, you will end up with code that is :

- Easy to manage the codebase

- Easier to scale

  - You don’t have to scale the whole system, you can scale only the services that are problematic

- Easier to deploy

  - The services are independent

- Services can be reused in different applications

On the other hand the monolithic option is:

- Cheaper at the early stages of a project or for the MVP phase

- A faster application because it does not include communication overhead between the separated services

It can’t be definitively stated that one option is better than the other, but if you’re looking for an architectural approach to you startup, which you want to grow and maintain, we would suggest using service-oriented architecture because monolithic brings these disadvantages:

- Long-term maintenance is usually hard because of the complexity of the monolithic component

- It is difficult to replace some functional parts with new ones, because it’s hardcoded within the project

- It’s harder to scale such a project for higher demands

BUT, you always have to be very careful how you choose to split the application, as it can be easily “overengineered,” adding useless complexity. There’s no easy rule to go on, but at least follow these steps:

- Having too many services can be counterproductive, so keep the number of services low

- Don’t forget the communication overhead between services

- Keep the services completely isolated from each other. It’s usually bad design if you need to share codebase between the services.

**3rd party services**

Yes. Use them! Using managed 3rd party services can save a lot of time and even money and it lowers the amount of resources you need for development and maintenance. In any case, always be careful and do the following:

- Carefully estimate your usage and traffic because it’s usually related to the pricing

- Check the SLA they offer

  - (Including security policies so you can be compliant with GDPR etc.)

- Check the credibility of the services because your business will depend on them and replacing such service means more investment
