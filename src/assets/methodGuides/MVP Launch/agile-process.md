---
type: Guide
category: MVP Launch
index: 8
title: Agile Process
description: Everyone wants to be Agile, and it’s more than just a buzzword. Here we extract its most important aspects to help you set up processes to develop quickly and with good quality.
---

### Quick with quality

At the heart of the Agile process is flexibility. Rather than having a more traditional, rigidly structured development process, where every step is completely dependent on previous steps being completed, Agile looks at the whole picture and tackles different parts of it. Having an adaptable, self-sufficient team is important for this to occur.

**Some important factors**

1. Test each other’s code.

2. Make sure to avoid a single point of failure by having at least two developers know the code and configurations.

3. Automate your processes. Writing Build Verification Tests (BVT) and moving towards Unit Testing(UT) saves a lot of time and prevents human error. It is recommended that you start doing this before your code base becomes too large.

4. It is very important to build a sandbox testing environment where you can work with untested code changes and deploy here daily.

5. Have daily meetings. This is important for keeping the team focused and achieving milestones. Do scrums--daily meetings that are short and precise to keep everyone focused and on track. Keep a backlog of the all the requirements and feedback.

6. The team should be able to function without much founder supervision. Allow people to learn, fail, make mistakes, and, most importantly, iterate to get the best version of the product. As long as they know the overall goal, this trial and error should eventually yield the desired results.

### Business validation tips

The software development team should be integrated with the product, customer engagement, and sales team. Ultimately, business development and software development are united. Rather than micromanaging, as a founder you should make sure to define the purpose and principles of the startup, so everyone knows what they’re striving for. However, you need to make sure to pay attention to the right elements. When you’re in the early stages, allowing the customer to pick features he or she might want could subtly steer you in the wrong direction until you’re fully off-course.

Try the following:

- Problem validation: Does the problem exist for a wide audience?

- Feedback analysis: Be selective and prioritize customer feedback by filtering out the noise (bad or inappropriate suggestions).

You have to approach your startup with some distance. Don’t get hung up on pet features that you personally love. After all: it’s about your customers.
