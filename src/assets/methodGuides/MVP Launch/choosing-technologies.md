---
type: Guide
category: MVP Launch
index: 9
title: Choosing Technologies
description: Choosing a technology is like choosing a car. You have to consider how fast you want to go, how far, how many people you want to take with you, and how much luggage you’ll take with you. Also, how big your budget is.
---

### Choosing the infrastructure

There are two options to go with: cloud or dedicated server.

**Dedicated server**

This is a physical server that you purchase or rent, and run in a server house or in your own environment. This solution is usually used by large companies where you can have specific requirements on data security and data geolocation.

**Cloud**

Instead of purchasing dedicated servers, you can purchase a cloud server where you don’t pay for the hardware itself, but pay for a virtual environment and usually just get billed for the actual usage of the servers.

**Which one should you choose?**

- Assuming you are not a large corporation, we suggest using cloud servers for these reasons:

  - Quick access to additional resources if needed

- Adding more memory and computing power is usually just a matter of a few clicks

- Auto-scaling

- High security standards provided by the cloud providers

- Lower maintenance costs

- Cloud providers usually provide a larger set of managed services like databases, messaging, auth, queues, etc.

  - Saves implementation costs

  - Lower maintenance requirements

But always check the SLA of the provider especially related to the:

- Availability

  - Usually 99.99% per month

    - It means that the server can be down ~4mins a month

- Pricing. It’s usually based on

  - CPU usage

  - Data transfer

- Geographical location of the data

Recommended providers:

- Amazon AWS

### Choosing the programming language

Basically any language can be used. At a high level, they all do the same things, but the real question is about the community around the language and what that community can provide. Building an MVP usually requires rapid development, which can be supported by your chosen language in the following ways:

**Available resources**

- Some languages are more popular than others and the cost of developers can be different
- Different countries= different resources

**Available open source libraries and frameworks**

**Active community**

- Support

**Active development of the language itself**

- Performance and security updates
  Based on these points, the most common backend languages are:

- PHP

- Python

- Java

- Node.js

- .net

- Ruby

Choose your backend technologies according to the size of your startup/project. Do not choose obscure technologies or frameworks that will cause you trouble finding people. Some very good, popular, and proven frameworks for mid-sized projects are Django and Ruby on Rails. Both have been vetted by large companies.

If you need something smaller but specialized, for example, real-time responses, use lighter event-driven frameworks that can communicate through websockets, like aiohttp.

Choosing one of these languages is ultimately only a matter of available resources. They all have active communities, with large, open-source frameworks that can save a lot of resources in any stage of development. Always keep in mind to use the latest stable version of the chosen technology and don’t forget to check the license of any open-source code before using it. https://choosealicense.com/

### Frontend development for web

Choosing the frontend technology is not about the language itself, because for web technologies you always go with HTML/CSS and Javascript. The question is more about choosing the right framework, which really depends on specific needs.

Technologies evolve quickly, so these suggested frameworks are probably already outdated, nevertheless:

- HTML/CSS

  - Bootstrap

    - recommended for responsive web pages

- Javascript

  - ReactJS

    - recommended for pages with dynamic content

### Mobile development

Most applications are developed for the two most common platforms: Android and iOS. Here we suggest using a Hybrid framework. What does that mean? Usually, when developing mobile apps, you need a different developer for Android and iOS because the platforms use different technologies. This basically means twice the amount of resources are needed to develop the app. But there’s usually a clever way to solve this issue with Hybrid frameworks: with these frameworks you can have a single code base for both platforms, so you don’t have to use as many resources. We recommend going with the React Native hybrid framework.

### Tools to support development

Here’s a list of tools and processes you should pay attention to because they can save time and improve the overall performance of the development team:

- Jira

  - An issue-tracking and project-management tool

    - The biggest advantage here is the integration with source code versioning services like bitbucket to automatically track the workflow

  - Continuous integration and automated testing

    - Choosing the right tools is just a matter of supported features and pricing

### Open-source libraries

Nightmare or life-saver? It can be either. There are a lot of good quality libraries that can save you a lot of time. To choose the right ones, follow these simple rules:

- Always check how active the development community is

  - Check the recent changes in the code

  - Check the number of downloads and stars on Github

- If possible, go with a stable version of the library

  - Always check if the license is compliant with your requirements
