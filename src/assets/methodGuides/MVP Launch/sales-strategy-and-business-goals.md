---
type: Guide
category: MVP Launch
index: 6
title: Sales Strategy and Business Goals
description: There are many different ways to achieve product/market fit.  There are certain popular approaches, but one shouldn’t be chosen at random. While it’s okay to set ambitious goals, the strategies you devise should actually be executable. Any sales strategy should be centered around communicating your value to your customers. This sounds easy, but is surprisingly one of the things at which many startups fail.
---

### Consider your sales team

- What function does sales serve for your organization?

- How does the sales department work with other departments?

- What role does the sales team have in helping your startup hit its business goals?

- What resources do sales require to get the job done?

- What recommendations does sales leadership have?

### Designing and executing your sales strategy

Something to consider when devising a sales strategy: there’s an old business statistic that says, “80% of your business comes from 20% of your customers.” It is potentially five times cheaper to retain a current customer than acquire a new one. With that in mind, here are some steps to use when designing a versatile sales plan.

- Ask yourself about goals for the future and anticipate milestones along the way.

- Make a list of all potential markets.

- Take this list and define criteria against which to “filter” it. This includes variables such as size and growth potential of customers, transaction volumes vs. size, brand value, how much effort and time it might take to acquire clients, and their technology integration capability.

- Score the list against the criteria. Put unattractive markets to one side and rank attractive ones in order of overall appeal.

- Come up with a short list of prioritized markets.

- Define sales channels, process, and evaluation criteria for acquiring customers from each market.

According to Shoprocket’s CEO, Anthony Gale, “Actually taking some time to identify all the markets available but focus on the most appropriate ones is really valuable”. Otherwise, he says “In a startup environment it’s very easy to disappear off in random directions, without the physical and financial resources to execute on them all and understand where real value lies.” Entrepreneur and investor Mark Suster reminds us that “the best teams are hyper-focused” and often defined by what they choose not to do: “The scarcest resource in your company is management bandwidth. Spend it wisely,” he says.

### Cash flow

Overall, 90% of startups fail, and many of them go down because their lack of focus prevents them from building a customer base, they have unrefined business models, or they struggle to create cash reserves. Of the 82% of that fail, it’s because of cash flow issues. Cash flow is the net amount of cash and cash-equivalents moving in and out of a business.

Cash-flow issues include:

- Not having enough cash on hand to pay bills

- Disorganized accounting systems

- The inability to achieve profitability

A good sales strategy is a safeguard against cash-flow issues.

### Creating business goals

The goals you should focus most on are the ones that are going to get your sales team healthy, for example, if your startup is bleeding money, make sure your expense tracking and policy is up to par. Identify your sales team’s KPIs. Maybe you should hire new sales reps? Measure your sales team’s health by following these steps, then you’ll be able to figure out a way to reach the next level of success.

Creating business goals is a data-driven way to measure your sales strategy’s impact. If you’re hitting your goals, you’ve identified a way to bring all the different departments of your team together to achieve a shared vision. If you’re not hitting your business goals, you may need to rethink your strategy.

When making goals, include all internal stakeholders, from the CEO down to your customer service reps.

Goals should reflect your company’s identity. Really understanding what your startup is all about (see previous articles in defining your vision) is key to formulating appropriate goals. You should feel that it’s possible to hit your goals, but at the same time they shouldn’t be too easy.

Ask yourself questions like:

- What are my startup’s core values?

- Why does my startup exist?

- What problems is my startup looking to solve?

- Why am I the only solution to this problem?

Business goals should be a combination of broad and specific. Figure out what problem your startup is solving, then make a “best case” scenario to see if you’re actually solving it.

Feel free to be a little ambitious; your goals should explain your reason for getting into business: your value proposition.

Your team and your customers need reminders as to why you’re the only unique solution to the problem you’re solving.

### The SMART system

The SMART system helps you create a realistic picture of how your startup’s different departments will interact to achieve your business goals.

- Specific: Goals that are specific

- Measurable: Goals that you’re going to be able to measure

- Achievable: Goals that you’re team is going to be able to achieve

- Realistic: Using a best, medium, worst case scenario and seeing how you land

- Time-based: Give your goals an end date. Then create new ones.

A startup is like an ecosystem, and marketing, product, finance, and other divisions will need to work together to effectively hit business goals around profit, growth, and service, etc.

Once you have an understanding of your sales process, you’ve probably identified opportunities for improvement: what hasn’t been working, what needs to be changed. Ask yourself tough questions like: has management been effective? This process needs to include ALL stakeholders, including departments outside of sales. Figure out ways to get your team to work together better.

Don’t be too attached to a certain sales plan. If it doesn’t work, try something else. If your team isn’t hitting your outbound lead numbers, measure the different channels they’ve used to acquire leads and figure out a way to mix it up.
