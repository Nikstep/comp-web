---
type: Guide
category: MVP Launch
index: 1
title: Setting OKRs
description: Now that you’ve put the pieces in place, you’re going to need to define proper leadership and methods of bringing about your goals as a company.
---

### What are OKRs

OKR stands for Objectives and Key Results. OKRs are a goal-setting methodology that makes personal and team goals transparent for the whole organization while providing individual guidance. OKRs were first taken up by Intel and made famous by Google. Many high-growth companies use OKRs.

### Why it’s important to set OKRs

- Motivation of employees and management

- Autonomy in decision-making

- Understanding one’s place in the hierarchy and how each individual’s contributions hold up

OKRs create transparent goal systems for the whole company. Each key result needs to be measurable, which also means it can be scored at the end of a period - usually quarter. This creates an individual signpost/compass and helps with prioritization. Everybody knows their motivation and targets for the quarter and can find them in the company ledger.

Having OKRs set and agreed upon by everyone lends a great deal of autonomy, but also responsibility, to each individual. Anything that helps to achieve OKRs is on the right track (within company standards, codes of conduct, etc.). Anything that does not is usually deprioritized. This can be decided based on each individual’s level without the need of executive decisions from the top of the company.

### What makes good OKRs

- Maximum of 5 objectives, ideally 3

- Max 5 key results per objective

- KR must be measurable

- Up to 60% of OKRs should be defined from your direct reports

- They have to be aligned with the company strategy

### How to communicate OKRs to the company

If you are just now starting with OKRs, first pick a small group of people (less than 10) to test them out on. Make the test public to the rest of the company so they know what’s coming. Typically, if you try to set OKRs for everybody and your organization has not done this before, you are likely to fail and not see the benefits. Gradual rollout in this situation is better. The benefits and methodology of OKRs should be communicated to everyone in a company-wide presentation.
