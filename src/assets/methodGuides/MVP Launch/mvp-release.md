---
type: Guide
category: MVP Launch
index: 11
title: MVP Release
description: Releasing the application means your troubles are over, right? NO! You’re almost there, but there are still some important things you need to make sure you do.
---

### Technical side

Releasing is very simple, but you should always follow this checklist before any production release:

- Have security testing done before any release.
- Try to do the testing on a system that is very close to the production environment.
- Have automated functional testing ready.
- Set up your monitoring. The app can be much more popular than you expected and - you need to be notified if more server resources are needed.
- Set up some analytics to see how your app performs.
- Prepare your support team. There is always something to support even though the app is supposed to be bug-free and user-friendly.
- Always keep your infrastructure up to date. Set up a procedure to update the infrastructure when any security fixes are released.

### It’s alive!

Once your project is live, don't let it die. You need people to keep it breathing. They have to visit your shop, visit your website, see your activities. The project lives from people's attention. If you don’t bring people there, you can expect to close up shop and realize you’ve wasted much time and money.

### Hello, World

First, you have to admit that you are absolutely nobody, no one knows you, so you will have to pay for their attention. Introduce yourself to world. Make some noise! Setup your social media profiles promote you posts, and lead them to your business. Setup the PPC and business profiles, so people can easily find you on the internet. In case your business brings something new to the market, write a press release and try to connect with servers who will be glad that they can write about you. Make friends, you will need them.
