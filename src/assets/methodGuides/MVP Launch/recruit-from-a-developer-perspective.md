---
type: Guide
category: MVP Launch
index: 4
title: Recruit from a Developer's Perspective
description: Want to make your ads interesting enough to catch the attention of developers? I am a developer, and I do not respond to 19 out of 20 ads from recruiters. Programmers are a tough sell since the tech market determined the high value of this work. If the programmer is overwhelmed with bids, you need to find smarter ways to get their attention. I’ll tell you what would work for me.
---

### Some initial things to look for

You can recognize a skilled developer by the large portfolio of apps or projects they’ve done, not only in previous jobs, but also in their own time. This is what distinguishes a skilled developer from a “normal” one. A skilled developer will have (among other things) the interest and enthusiasm to develop their own projects even when coming home from work late in the evening. Most of the time, these “hobby” projects are of a different type than the developer does in his work, so that proves he has a good overview of the large field of technology.

### You don’t have to code to speak their language, but it helps

The best job posts are written by senior employees who understand what the job requires. Someone who won’t mistake Java with JavaScript should highlight why the work is interesting or what sweet new technologies the future team member will get to use. Working with uninteresting technologies can be detrimental for you. Unless you’re an unflexible corporation, you should follow new trends and implement the newest technologies. Otherwise, forget about getting a good programmer at an affordable salary. (At U+ we actually use a lean startup method to help such corporations get cool digital products out the door faster).

### Show compelling work

One of the best motivations is when your product can help people, and the developer team identifies with it and feels it’s something they can put their heart into. But this may mean that a foodie developer might prefer to code a startup for sharing coffee cup lids instead of an information system for improving executive efficiency. But c’est la vie. We, developers, are fickle creatures with unique proclivities.

Occasionally, I see ads where big companies try to speak the language of the young and look cool. Then, after a young person gets seduced by said company (usually through the promise of much money), they force him to wear a tie, sit in a cubicle, and create tickets in SAP. The employee will eventually get burned out. But then he has that mortgage to pay each month, so he stays with it. Create a better motivation than just money or personal benefit. Show developers that you offer inspiring, rewarding work where they’ll improve the lives of others. You should communicate how big of an impact this job can have on society. It is good to try to show any benefits you can. Even though you might not be saving lives, you can help and influence big groups of people through your work.

### Look in the right places

You can't usually find good programmers through public employment services or web ads. Capable developers are probably already employed and won’t bother to find a new job if they are already comfortable in their current position. If their employer pampers them, you are probably out of luck. But if you can find some underrated talent in a large corporation, you may have a future colleague on your hands.

#### Explore developer’s social communities

Courses, lectures, conferences, special Facebook groups, or referrals from colleagues and friends. Generally, it’s good recruitment if you are doing technology lectures to network with the developer community (this approach has proven to be very good). It is also a good idea to give rewards for recommending new colleagues. People like to work with their friends, so they will try to convince them to work for the company.

#### Academia

If you have a master's degree, you can be a thesis advisor or an external supervisor or directly negotiate with the university management for cooperation. However, you should choose the students carefully and making sure you get those who really like the work they are involved in and aren’t just in it for the degree. I understand that a recruiter cannot consult on a diploma thesis, but some of the programmers could. It can also be interesting to teach at the university a few hours a week. Approach capable students with job offers. Most of these students will feel honored for the opportunity and will be content working for a lower wage than an older professional. However, make sure you don’t exploit them like some "student companies" do because these students will eventually realize their own value.

#### Online

Everyone is on LinkedIn, however, if you find a talented person, it’s not a good idea to call them immediately and try to push them to make a decision or arrange a meeting with you. Capable people are contacted almost daily, possibly by over ten recruiters, five of whom write personal messages, and three of whom try to call them directly because they think what they have an offer you can’t refuse. Calling is too much, because you're disturbing people from their current activities. Also, developers tend to be rather shy.

Personalized text is transparently fake. Sometimes, an even bigger blunder can occur, if for example, you leave the wrong name in the greeting. One recruiter contacted my colleague with a great offer “just for him”, except she called him Peter, even though his name is actually Daniel. He answered her politely that he is not really interested and called her Matt. And that was that.

Another good idea is to pay for advertisements on specialized online magazines (like root.cz). These places are visited by developers because they read articles and contribute to a discussion boards on these servers. You can place your ad in a groups on Facebook, which focus on a particular language/framework/topic. But first check, if the group allows job ads to be posted there.

A slightly different option from the previous ones is if you’re knowledgeable, contribute to discussions on forums/reddit/stackoverflow and include a link to your jobs page in your profile description or post footer. If you contribute often with high quality content, people will look at your profile and visit your website.

#### Personal attention

You should do some legwork and check out profiles outside of LinkedIn. It takes a lot of time, but the results may be worth it. I was once approached by a recruiter who read an article about my trip to India on my blog and invited me to her favorite Indian restaurant (since I mentioned in the article a few times I’m a fan of Indian cuisine). Even though I wasn’t looking for a new job at that time, I met her, and we spent three hours talking about everything possible over lunch. This was a great demonstration of personal attention, and I really enjoyed it. Another company took me to a rock concert. Get creative.

### What to engage

**Money isn’t everything, but it’s important.** Don’t save money on good employees, because it is they who can attract other capable colleagues. I know a lot of companies that invest a lot of money in beautiful offices, spend a lot on benefits, but whose employees are paid much worse than their equally experienced colleagues. Of course it is logical: their salaries are lower because of the benefits, but it’s not exactly the right approach. Offices should be good, developers should have peace and space for work, summers in the office should be experienced without sweaty armpits, but it doesn’t make sense to focus only on benefits. Many people will admit they’d rather have more money on their account than a “free” multisport card (after all, it’s not actually free).

**Be upfront about your company’s drawbacks are and what you need help with.** You should not hide anything because it will always come out later and the probation period is long enough to reveal it anyway (with freelance there is no probation period, but also no commitment). Honesty is really important, and a connection is created when, for example, a new employee can help you when you need to do good devstack and no one else can.

**Show colleagues that your new employee can learn from.** Competent people would rather join a company with other competent people and interesting colleagues. It’s even perhaps best if this person contacts the candidate personally and offers him/her to meet in a cafe and talk about their future cooperation. I know a lot of people who had meetings like that.

#### Keep it informal.

Feel free to invite the candidate for a coffee or beer, be friendly, speak casually, and be genuine. I personally think it’s good to try it at a neutral place. Offices are perhaps too formal, and personally I prefer to get to know people on more casual terms before visiting the workspace.

### Perks

“Sick days”, “young dynamic collective”, “opportunity to participate in decisions”, and “competitive salary”, are things offered by every IT company or startup. And if not, they will still write it in their job offer. These buzzwords should be givens for your company, so it doesn’t make sense to mention them as an extra advantage. You have to think up something more unique about why your company is a special opportunity.

**Mention interesting benefits that are not offered by every company, such as:**

- **Flexibility and freedom in work and the possibility to work remotely.**

- **Work-life balance.** Show people that you will not keep them at work against their will. Don’t force them to work on weekends. If the deadline is really close, don’t be afraid to offer them a bigger salary if they work in their free time. In my opinion this works really great.

- **Sports and fitness.** Do you offer yoga, climbing, or running? Mention these and invite a candidate to a session before hiring.

- **Games.** Programmers have to procrastinate too, so it’s good to have some entertainment for them. This will also make people more excited about coming to the office.

- **Food.** The old Romans knew well (as our current politicians do) that bread and circuses are a great way to gain popularity. It's not that different with programmers. Do you offer free fruit and breakfast in the office? In addition, you can encourage programmers to be at work earlier with periodic breakfast spreads.

- **Personal development.** Participation in language courses, conferences, company cooking courses or anything else is a welcome benefit, definitely worth mentioning.

- **Team-building events.** Do you organize trips, wine tastings, climbing, canoeing, or team buildings where you eat organic goat cheese on a scenic farm? Personally, I can’t resist such things.

- **Free hardware.** If you’re cool enough to offer Macs or PCs to employees according to their preference, you should probably mention it.

- **Phone plan.** Many companies still do not provide phone plans to their employees, which is a shame because a company is able to negotiate significantly better conditions than an individual, and it’s another perk that could keep employees in the company (at least in a place like the Czech Republic, where no one wants to look for better conditions in the overpriced market of Czech mobile operators).

- **Coffee equals code.** It’s maybe a bit literal of a statement, but I believe it’s true. So, if you have a La Marzocco coffee machine and beans from Doubleshot, you should certainly be proud of it. Tea is terrific as well. You could get a tea subscription for your office or find a tea guru. A great example is one company that has a tea room right in their office and uses it as a place for recruitment at conferences.
