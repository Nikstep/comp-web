---
type: Guide
category: MVP Launch
index: 14
title: Product/Market fit evaluation
description: Here you have to evaluate that you have found the product/market fit. Here are metrics that will help you know when you achieved it.
---

### How to evaluate

There are five metrics any online business can measure to empirically verify if they achieved Product / Market fit. They are 

1. Bounce Rate, 
2. Time on Site, 
3. Pages per Visit, 
4. Returning Visitors, 
5. Customer Lifetime Value. 

Low bounce rates means a visitor's expectation is being met. High Time on Site and Pages per Visit indicate that the experience of the user is satisfactory. High Returning Visitor reflects the lasting impact a product has on their customers, causing them to come back, and Customer Lifetime Value measures the profitability each customer brings to the company. If these 5 metrics are above average and your 40% rule is met, you'll know you have a Product / Market Fit company.

But what’s more important is how it feels. You will achieve product/market fit if you see happy faces on your customers, if the phones are ringing off the hook in the office, and of course if you’re acquiring enough new customers that you don't have time to do the support and you need to hire another employee to do it.

Also, your marketing cost must be lower than acquisition costs and your marketing activities scalable.

On the other hand, you may never achieve market fit. The majority of startups never get to this point, and it's also important to know when you have to pivot or close your business. After a while, if you see that you have ten customers and almost no revenue, then you should step back, do the validation process again, and pivot. In the majority of cases, this means that your value proposition is not strong enough to gain customers.
