---
type: Guide
category: MVP Launch
index: 12
title: Post-launch Support
description: 'This is a very valuable part of the process. Rather than a burden, view it as a firsthand opportunity to get information from people, but be careful: you must be selective about how you use it.'
---

### Support provided by founder

Providing support is very important and should be provided directly by the startup founder in the early stages of your company. Users give valuable feedback on the product which the founder needs to hear firsthand because only he can quickly push changes.

### The trap

Be careful when trying to fulfill the wishes of all your customers. It’s common for visitors to tell you that they will pay when you add this or that feature or when you make custom adjustments for them. This a trap into which many startups have fallen and met their end. You can end up trying to customize your product to the demands of one customer and lose sight of your overall business and never scale.

We recommend you record all requests from users and prioritize based on your OKR. A great tool that can help with this is productboard.com.

### Have a conversation

We also encourage you to respond with care and have conversations with your customers. Don’t be afraid to call the first paying customers and ask them about their experience, they will be happy to share some comments and ideas for improvement.

We use the intercom.io tool, which has chat and automatic messages. Every new customer receives an automatic welcome message asking for feedback

### Include phone number

Don’t be afraid to include a phone number in your contacts, you want people to call you and ask. The more user information, the better. It’s important to have a conversation, and if you can handle it properly and solve the problem, the user will have confidence in you, the relationship, and be willing to pay you.

### Be polite

Communicate quickly and with grace. If the client does not understand something, it may not be his fault, but a UX problem in your product. The worst thing is arrogance and ignorance, which can give you a bad reputation and also speed up your demise.

### Handover

When the moment comes when the founder doesn’t have a time to do support, it’s cause for celebration. This means you already have enough customers to give the role to someone else. Choose a person who has a high degree of empathy, which is essential for proper support.
