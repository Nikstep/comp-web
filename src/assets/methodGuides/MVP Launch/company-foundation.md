---
type: Guide
category: MVP Launch
index: 2
title: Company Foundation
description: As a founder, company incorporation has two main targets. The first is your protection, and the second is defining ownership and roles.
---

### Protection

Companies’ forms and their legal regulation can vary in different countries and jurisdictions, but all of them offer several versions with limited liability for the owners. Some examples are Ltd. or LLC companies, but also joint-stock companies. Limited liability means that if a startup fails or has financial problems, you will lose only your contributions to the company; no one should be able to touch your private property and wealth (though this doesn’t apply to every country’s laws). In general, there are very few situations when companies with unlimited liability offer benefits to the owners.

This point is important especially because it can be temptingly easy for founders to conclude contracts (suppliers, clients, financing etc.) in their name before the company is legally incorporated, with the intention to change them under the company in the future. This can be extremely risky and dangerous since the founder becomes personally responsible for the contract and has full liability.

Regarding the difference between Ltd. companies and stock companies: in general stock companies enable easier transactions with equity, voting etc., but on the other hand, they’re more demanding in terms of administration and legal compliance. Due to this, they are more appropriate for more mature and valuable companies, while a Ltd. is sufficient enough for the early stages.

### Defining ownership

Defining ownership and conditions can be more complicated than it seems. The evolution of a startup can be very fast and dynamic. Co-founders and others you start the company with will very likely change after six months or a year. Also, the involvement and contributions of people might be of a different nature, and people may have different expectations. Due to these differences, it is necessary to clarify everything as soon as possible, preferably in the form of a contract. This is called a shareholders’ or co-founders’ agreement. This contract should include not only equity shares, but also vesting rules. That means it’s legally binding, that, for example, if one of the co-founders decides to leave the startup in its first year, he will lose his whole share, or part of his share will be passed to the others. It should also cover other situations, such as new co-founders entering the company or how you might get equity motivation, e.g., key employees, and how much. Other provisions are, for example, restrictions on sales of the share for some period and without prior approval of the other shareholders.
