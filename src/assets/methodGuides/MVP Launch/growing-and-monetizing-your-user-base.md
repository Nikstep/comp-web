---
type: Guide
category: MVP Launch
index: 13
title: Growing and Monetizing Your User Base
description: '<p>Some would say that it is foolish to attempt to write about growing and monetizing your user base in the same blog post. They would say that this topic is too general and large, and that it really depends on the product, or at least which category it falls into; for example, a mobile game app and a desktop software app would be marketed and sold in very different ways.</p><p>Those people would be right.</p><p>Therefore, I am here to simply share the pillars that have worked for me in the past. These are methods that any owners, sales, and marketing teams should definitely have on their radars.</p>'
---

### Growing your user base

To grow your user base, you really need to focus on timeline and quality. How fast do you want to grow your user base (realistically), and what kind of users do you want to attract? A lot of people can grow a user base quite quickly but at a lower quality level. What does this mean? This means you can get a lot of users on your network, or using your product, but their LTV (life-time value) might be very low, as your churn rate will be high.

#### Referrals

Referrals are the best way to get any product or business off the ground for two very simple reasons. One, referrals are free. Two, referrals have the highest rate of success due to the fact that people tend to trust those in their networks. Think about it: how many people do you know in relationships that met because of friends or through an introduction by someone they trust (maybe it was you)?

If you can trust your connections when it comes to finding your future partner, I’m pretty sure you’ll try a product they recommend as well.

#### Content

Content creation is a great channel for growing a user base and creating trust with your users. However, it must be mentioned that this channel does require more time and diligence. Generating new, relevant content is not that easy. However, what’s often forgotten when starting a content plan is that not every piece has to be fresh and unique. While you do need to keep your topics interesting and targeted to your audience, you can constantly think of how to reuse content that has already been created.

A tactic I have seen work again and again is to create an overarching topic that can be broken down into sections (i.e., blog posts). After the last section or topic is launched and promoted, you can then pull all the sections into a beautifully designed pdf, which you can ask for contact information to download.

We created a great example of this strategy at a Java tooling company for developers. In this company, we saw a 500% increase in traffic, largely due to a very aggressive content plan. Because that audience was so niche and ate up quality content especially focused around their jobs and technologies, we were even able to get writers from other companies in the industry. We really focused on comparing products and tools, as well as growth and involvement based on geography.

#### Networks

Networks, both physical and virtual, are a very time-consuming channel when building your user base. However, if you can remain very active, they will slowly start to turn into your referral network. Being seen often on social networks and in person at meetups/events will start to establish your personal and company brand as a serious endeavor, willing to put in the hard work needed to succeed.

#### Advertising

Here I would focus more around lead generation than the old school branding techniques of advertising. When it comes to lead generation, you need to look at your revenue and profit goals and the total cost of development of your product, and decide what you would be willing to spend per lead. In the beginning, you will want to diversify your sources to find out where the highest number, as well as the quality of leads is coming from, and then decide whether or not it makes sense to continue with the other networks or placements. I have seen both direct ad buying (you know, like they did in the 80s) and network advertising work like gangbusters.

In one specific situation, again at the Java tooling company, we were buying directly from a development forum and spending $2,000/month, and receiving around 500 signups from this source. While using Adwords, we were spending around $10,000 to get 1,500 signups. However, we quickly noticed the adwords leads were converting to paying customers at a much better rate, and their LTV was nearly 3x of the development forum. With this data, many marketers would drop advertising on the forum. However, we made the decision to keep running on the forum, and once we started to get some branding awareness, we noticed our numbers running in the right direction. It did take some time, I might add.

#### Conferences and events

Simply put: invest your money and go to conferences and exhibitions. Although technology and advertising have come a long way, nothing builds bonds better than the good old face-to-face interaction. Companies use pictures, avatars, special wording on their sites - all this to humanize their business and products. These are great and they work, but the best way to humanize your digital product is still to use an actual human.

Sometimes it’s old school, depending on the conference, but order your booth, get some comfortable shoes, shake 400 hands, and get 100 business cards. You don’t have to have the biggest booth, or spend the most money; let’s face it, you’re not going to be larger than Microsoft or Nokia on the showfloor. Be creative when designing your booth and people will remember. Staff it well, and people will have a conversation with you.

In my 14 years of running marketing and attending countless shows, designing booths, and the works - I have to say that my favorite and most successful booth cost 15,000 Euros in total, was the smallest one in the room, but voted best booth on the show floor.

Finally, don’t forget to use your own user base to help you spread the word of your products or company. They obviously like what you are offering or they would not be users.

### Monetizing your user base

Monetization of your user base really depends on the type of products that you are selling to your user base. But no matter what kind of product you are offering, if there is no value for your users, then you are going to have a very difficult time with monetization.

#### Subscription

Subscription models are used for various types of products, typically where data, creative assets, or information is updated on a regular basis. You pay for access to a system for a specific period of time. Think Netflix (tv and movies), Spotify Premium (music), or Crunchbase Premium (company or contact information).

#### Freemium

This model has become very popular in the past ten years by giving away partial access or limited features of a product. Essentially, you are able to use the product and get a good understanding of what it does, and in many cases, like Spotify, you can settle for using only the free version. In the Freemium model, the provider of the product holds back what they consider to be the best function or features for those who agree to pay a fee. Think Spotify Free (music), Trello (project management), or MailChimp (email marketing).

Addiction formed in the free stage is a strong method for monetizing products.

#### Ad model

The advertising model works very well for products that are looking to build a very large user base, while at the same time collecting as much information on them as possible. If you use the internet, you are using products on a daily basis that have advertising models generating revenue for them. In many cases you don’t even have to click on an ad for the company to generate revenue, as simply displaying an ad costs money in some models as well. Think Facebook, Google, or almost all online media publications (NYTimes, CNN, etc.)

#### Free + consulting

This is an older model, which I must admit I did not become aware of for quite some time. In the product-based consulting model, all products are given away for free and typically open-sourced for complete access. These products typically exist in the tech world. In this model the product or products are free, and the company follows up by offering their consulting services around the products themselves, as well as giving industry knowledge. Think Red Hat (software).

#### The buyout

Give your product away for free to as many people as possible and hope it goes viral and a company makes an offer to buy you before you run out of runway or get more investment. (Not recommended strategically, but it has happened). Think WhatsApp (free messaging and calling).

Some products already have their monetization model built in, or at least have some precedent based on the industry and similar product offerings. Social media is a great example of this, as their goal is to get as many users on their system connecting with one another. Thus, brands start to see huge numbers of their target audiences in one location, causing them to reach out to the networks to run their ads.

Remember: trying to monetize your products too early can be quite dangerous. Ask yourself: would you have ever used Spotify if you had to pay \$1 just to try it?

So, these are some pillars of success when growing your user base, and different monetization models that you can use to keep you in the black. You ultimately have to look at your own product, vision, and goals to determine which growth and monetization techniques fit you best.
