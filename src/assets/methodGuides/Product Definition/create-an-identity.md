---
type: Guide
category: Product definition
index: 6
title: Creating an Identity
description: Everyone has something of a unique style which makes them identifiable. As they say, “clothes make the man.” So too does visual identity make a brand. Think about this while building your startup. Brand and visual identity should be united.
---

### How to pick a name

Your startup’s name is crucial. Aside from a logo, it is the first thing that people will encounter and it will make the first impression. It must be short and memorable. It should also correspond, at least a little, to what your startup does.

What's in a name?

The name also should be unique so it can be easily searchable. You don’t want to choose a common word that will be hard to find through search engines. Also, consider if the domain name for your name is free. A good, memorable domain is a big advantage for your project. If it seems reasonable, you can combine the domain and it’s ending to form the name of your startup. A popular domain with an ending suitable for this purpose is the .io domain, which is used a lot by startups.

The name should also be easily pronounceable by native English speakers. Do not choose local names in your language if you intend your startup to go international.

- Make sure it's easy to write and pronounce.

- Try to translate it. (It might mean something inappropriate in other languages).

- Define how you want to position yourself.

- Register the unique domain name and sign up for a Web host. Although free Web hosts and portfolio sites are available, having your own domain name lends a bit of credibility to your business or online persona. Don't rely on only your social network page as your primary online profile. Having a domain name ensures that your place on the Web is secure, no matter what online trends come and go, and gives you higher credibility.

- Let your domain name drive the search. Below are some tools you can utilize:

  - [Wordoid](https://wordoid.com/) - Pick a short and catchy name for your business

  - [Naminum](http://www.naminum.com/) - The leading startup, company, and website name generator on the web

  - [Domainr](https://domainr.com/) - Fast, free, domain name search, short URLs

### Logos

- Mood board

  - Before the whole design process of a logo or a website, meet over a moodboard, or create it yourself. It’s a set of all the artwork that inspires you or might contribute to the final logo design. In terms of colors, shapes, or overall design, it’s the goal of how your logo/site should look. Dozens of styles exist. It’s important that you and the designer are on the same page and compare moodboards with other websites, fonts, colors, pictures, or even architecture, industrial design etc., so you know each other’s vision for the project.

  - Know your target audience and research appropriate visual styles, e.g., you probably wouldn’t use graffiti for a kindergarten logo. Be prepared to answer questions such as: "If your brand was a person, how would that person look?"; “what feelings/emotions should be included?” The designer must know your business really well to prepare a suitable brand.

- Simplicity is the key

  - Make the appearance understandable, easy to discern, and simple, however it shouldn’t be too vivid or too visible.

- Logo = A symbol, something that will represent your brand.
  Logotype = A typographical logo, made only by text and possibly some twist inside the text, without a proper symbol. Unless you’re Nike, you should be okay with using a logotype.

- Brand Manual vs. logo manual

  - Logo manual is the basic manual you will need – it needs to define colors, fonts, shape, and the construction of a logo. It also defines the rules of how to place the logo somewhere, with safe zone around the logo, minimum size etc. It’s a technical document of how to treat a logo and how it’s made.

  - Brand manual is an extension of a logo manual, where all of the brand materials are defined, such as business cards, letterheads, posters, stickers and anything you want. The purpose of a brand manual is to have all your brand needs in one place and you will no longer need a designer to maintain your brand, you should have everything in there and just change the contents yourself, or at the printing house.

- Style tiles

  - If a designer is preparing you website design, always get a style tile from him. It’s a definition of all elements used in the design, so you can build your own pages in the future, and they’ll still be consistent with the rest of the design. It defines used fonts and sizes (like H1, H2 etc.), buttons in all stages (hover, click, default), forms, colors, spacings, etc. It’s like a Lego kit you can build a new page from.

### Colors

Even changing the saturation or hue of a color can make a big difference in the mood of your design. Plus, colors mean different things in different contexts and countries.

### Claim

- The main claim is about emotions. Don't put: “We are digital product development company and tech investor.” You can use it as an explanatory subtitle, but the main claim is supposed to trigger emotions and be somewhat memorable. Make them intrigued and interested to find out more. Examples: “Launching ideas into reality.” “It's time to tame the chaos of your payroll!” “Make your work a thrilling experience.” “We make your employees happier than ever!” “Where Work Happens” (Slack).

- Painkiller vs. vitamin vs. candy. There’s a pretty popular school of thought that suggests the best slogans should tap into a potential user’s pain. If not, then even if your product is selling something very useful, it might only be viewed as a vitamin, meaning it doesn’t address acute frustrations. Finally, you might be selling something totally addictive, but which is mostly a diversion—thus, candy.
