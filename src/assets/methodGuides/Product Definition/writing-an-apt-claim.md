---
type: Guide
category: Product definition
index: 5
title: Writing an Apt Claim
description: '<p>Exhibit A:</p><q>Mozart is the greatest composer of all. Beethoven created his music, but the music of Mozart is of such purity and beauty that one feels he merely found it—that it has always existed as part of the inner beauty of the universe waiting to be revealed.</q><p>—Albert Einstein</p>
<p>This may seem an odd quote to start with, but if one is permitted a bit of mixing of high and low, I believe a palpable parallel exists. And now Exhibit B:</p><q>You cannot create demand. You can only channel demand. Demand is there. Demand is enormous. The bigger the demand, the better your ad is. You are getting in a boat and letting the stream carry you. Just don’t think that you can paddle up against the stream.</q><p>—Eugene Schwartz </p>
'
---

### Copywriting is an art

Is it really so bizarre that arguably the greatest and most influential composer of all time would share a method with the most pioneering adman of the 20th century? Odd that Mozart and Schwartz both found a way to direct the flow of human passion, be it in expression or consumption?

If human beings are creatures of desire and demand, then perhaps there’s a lesson here: great ad copy is not hammered into shape, but pulled as a set of patterns from the already existing flow of demand. This doesn’t make it easy. Not everyone is Mozart. Schwartz himself admitted there were many more innately ingenious copywriters than him. But what he and you can arm yourself with is knowledge about the product.

According to Schwartz, (though it's head-slapping unimpeachable), when writing an individual claim, you have to know as much about the product as humanly possible. For a quick demonstration of the difference between how a bit of research can distinguish lazy boilerplate and slightly less lazy copy, see this succinct [article](https://medium.com/words-for-life/how-to-elevate-your-copywriting-from-good-to-great-225f6ac176ba).

This can be furthered by also saying that you have to know as much about the market as possible. Different products ride on top of different streams of desire and demand of a different strength and kind. You have to know what you’re dealing with.

### Channeling desire

The startup world updates Schwartz with its own tripartite analogy, but at bottom, it’s the same shtick. Candy vs. vitamin vs. painkiller. These are different kinds of products for different kinds of markets and draw different types of claims. The claim can dictate which your product is. Especially in untested markets, revolutionary products can go from novelties to life-support systems. When channeling pain in a claim, think about the headache-inducing whatever that your product can solve. Then, crucially, don’t just highlight the pain—that won’t lead anywhere— but incorporate the remedy (what you offer) into the claim. Is that a lot to pack into a few words? You bet. It’s going to take some iterating to land on the right claim. But first, you must correctly identify your own product. Again, getting to know the product as much as possible is everything (especially if you’re a hired copywriter and not just the CEO writing the claim yourself). For example, the true tragedy would be having a painkiller on your hands and only marketing it as a candy. The candy of course is addiction, inability to be sated. Perhaps the strongest products actually come from a mixing of these three comestibles. SF venture capitalist Kevin Fong claims an addictive painkiller is the ideal product. Again, your product might be an addictive painkiller, but it’s the great responsibility of the copywriter to forge its identity as such.

### CTA

It used to be whole ads, now it’s whole webpages. In the past it was called “direct response advertising”; mail something in, etc. In today’s webpages, if people see the claim on the top and click through the rest, you’re onto something good. A good CTA (call to action) piques curiosity and anticipation and should be more fully considered in the wholistic construction of a landing page along with the claim/slogan and other layout. After all, the admen of yesteryear were limited: they had to get your out the door and to a store or a mailbox with their writing and would’ve killed to be able to put clickable links in their newspaper ads. However, back then people weren’t as deluged with so much information and ads every day, so getting your attention was easier.

### The right words

Your slogan should be short and sweet. General and descriptive. Specific but encompassing. The more expansive your company, the more difficult it will be to write an apt claim. One fears the reduction to or heightened focus on a single aspect of the whole company when given the strictures of only a handful of words. If you’re building your startup from the ground up, you maybe won’t have a designated copywriter on your team when creating the first landing pages. Until you go through this process yourself, feel free to laugh at any company’s dumb slogan, but after attempting it yourself—and being brutally honest with whether the words you’ve selected actually fit and actually enhance and sell what you’ve got—then you simply cannot appreciate the Rubik’s cube quality of wrangling one of these haikus out of the ether of unborn slogans.

### Levels of market sophistication

Just as books and movies are targeted to certain audiences, when writing a claim you need to consider how much your audience knows; which products have come before. Incorrectly identifying your startup’s market can lead to failure. Have they heard it all before and do they need to really have their paradigm shaken up? Or is your product so new that the market doesn’t even exist yet? How much do you need to “educate” your future user base? This will likely be something you tweak and re-address along the path to building a startup, but this knowledge goes back to the fundamentals of your product brainstorming.
