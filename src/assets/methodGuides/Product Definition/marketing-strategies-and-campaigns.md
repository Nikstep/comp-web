---
type: Guide
category: Product definition
index: 2
title: Marketing Strategies and Campaigns
description: There you go, you big entrepreneur! You’ve brought your idea to life! Congratulations! Now let's show the rest of the world how amazing your project is.
---

### STDC

Did you know your potential customers have “shopping” habits? And even better: Did you know you can use them for your marketing strategy? Those habits have four shopping phases and are called See-Think-Do-Care (STDC) and were invented by Google Marketing evangelist Avinash Kaushik. Marketers all over the world use this technique to attract potential customers. Learn from it.

STDC is basically about building a relationship with your customers. If you’re doing it right, your customers will very soon become loyal customers, telling your story and selling your products to their friends and family.

**Phase 1: See**

At this point people haven't heard about you yet. In this phase you are talking to all your potential customers. If you’re selling a car, you have to talk to all the drivers. People can buy your product, but don't count on that in this phase. Right now you are basically trying to catch their attention. Run your profiles on social media, shoot a video, write a blog about your business, and don't forget about pay-per-click (PPC) and search engine optimization (SEO).

**Phase 2: Think**

Now customers are starting to think about actually buying a car, so you have to show them the reasons why they should buy a car from you. You have to show your experience, give them some advice on how to buy the right car for their needs, and show them how you can help them if they can’t decide. Write ‘How to’ blog posts, shoot a video comparing different cars, or run a PPC targeting people who are looking for advice.

**Phase 3: Do**

Customers are ready to buy a product, but they aren’t sure where. You have to show them that the easiest way to get what they want is at your business. Show them how easy it is, invent some “kick ass” solution, give them something special, and be better than your competitors. Good reviews will always help. Think about an affiliate program, show them your lucky customers and the benefits you have.

**Phase 4: Care**

Customers have already bought your product and you want them to buy more and spread the word to friends and family, or write a great review and come back. Offer them free customer care. Let them know how to take care of a product you sold them, be ready to solve any issue they might have. Show them they are the part of a ‘family’ and you actually care about their needs.

The most important part of the whole strategy is knowing your customer and target group better. If you know who they are, where they live, what they like, what they prefer, why they chose you etc., you will be able to target your communication better and also be ready to upgrade your portfolio to include other products.

### Content strategies

These days, content strategy is one of the most crucial parts of a marketing strategy. You are going to attract the customers with your experience, knowledge of your field, and show them just who you are. The market is full of people who want to succeed in your field and sometimes they have very similar products. The only differences will be your values and the personality you create for your business and company culture. You have to build a strong brand. Content strategy will help you build a relationship with your potential and current customers. And brand relationship is what sells nowadays.

Before building the content strategy, consider:

**Your target group**

Who are you actually talking to, what content do they like, what do they want to read, listen to, and watch?

**The problem you’re going to solve**

For example, are you going to help people lose weight and feel better or help moms manage their work/life balance? You have to find a problem which is real and your target group is trying to solve... Or something they didn’t know was a problem yet.

**How it’ll be unique**

What makes you better than your competitors or different products? If you don't have an answer for that, you will be one of many, and forgotten very soon. Don't waste your time and money. If you’re trying to invent something, know what’s already been invented. Be unique.

**Channels where your content will be published**

Always pick a channel where your audience is. Don't try to be somewhere and hope your audience will lead you there. Not yet. You can't afford to in the beginning. You have to follow your audience. Also, don't go where you are not able to produce the content. For example: don’t use YouTube if you can’t produce a quality video. Don't go to Instagram if you don't have pretty, eye-catching content. Don't run a blog if you hate writing.

How will you manage the content and publication - who will create the content? Who will publish it? How often? What kind of content are you going to produce? How will you know your content is good? How will you measure that? Who will answer your follower’s questions? Have everything set up before you start creating your content. Be prepared and you will not be surprised.

### Channels

**Pre-launch page**

Launch the web page as soon as possible. Even if you have an unfinished product, let people know that something is going on and explain to them what the startup is about. Don’t forget to collect emails and set up a PPC campaign. You can get dozens of users who will be eager to try your great service. Once you launch your product, you can write them and you can start working with them and expand product awareness among the community to quickly get your first paying customers.

**SEO**

SEO is a long-term strategy that you shouldn’t forget. If you do SEO well, you will get new clients regularly.

If you like to write or have employees who like to write, SEO can be for you. In this case, get your keyword analysis from an external agency and start writing valuable content on a blog. Remember, that SEO must by also part of your marketing strategy and it’s not a good idea to do it separately.

If you don’t have the people to do it, don't do SEO at this stage because it's expensive. Focus instead on technical SEO to make sure all pages are indexable by search engines like Google. You can start generating content and doing proper SEO in the scaling stage.

**PPC**

PPC advertising is a good way to get new clients quickly. Prepare to do it if you’re in a competitive environment, since one visitor can cost you more than \$10. Set up your campaign as soon as possible, before your launch your MVP, so you're not surprised. We recommend Google Adwords and PPC on Facebook. But keep in mind that these channels must be part of your marketing strategy. Proper PPC campaign setup is not easy and we recommend you hire an experienced professional.

**Video**

Video is the most popular, engaging, shareable, and easy-to-consume content. It helps people understand a topic far better than written content. It visualises the problem and solves it in a very short time. Books and other literature can provide more information, in greater depth, but take too long to consume. If a picture is worth a thousand words, video is worth a million. It’s 40x more likely to be shared on social media. On the other hand, you have to invest a lot of time and resources to produce something really valuable.

**Offline**

If you aren’t targeting a specific group of people, you can also try to focus on offline activities. Of course there are still people who watch TV, listen to the radio, use public transportation, or read newspapers. This kind of media has great impact - the numbers are really high - but the problem is, you can't exactly measure it. You can’t directly check how many sales (or different type of your goals) it brought and from which channel they came. You can do some additional activities to measure it, but there are extra costs. It is also much more expensive than online methods. You have to pay for the production of materials (a great TV spot can cost you a fortune) and the media space (which can be also higher than the production). The offline format is usually suggested for building brand awareness.

Also, you can use this one if you have a very specific target group. For example: if you are targeting a group of people who love to go to bars, you can definitely put out posters communicating your product there (it works the best on the toilet - the one place you have a moment of calm). If you provide, for example, a taxi service, you did the perfect targeting for catching people who are too drunk to drive.

**Events**

Your target group will gather at conferences, meetups, and exhibitions. If you manage to get to those events and present your idea, the community will definitely appreciate it and you’ll get many early adopters who can expand your product to the community. In addition, you will get a lot of ideas for improvement.

There are certainly some opinion leaders in the community whom it’s good to get in touch with and cooperate with, even if you promise them to test and use your product for free.

### Social Media

Use social media. Your target audience is there. You only have to find the right one. If you’re targeting millennials, you have to be on many forms of social media. If you are targeting a more specific group, you have to pick the right one. Don't forget, people are going to talk to you and others will see that.

On **Facebook** you can create your own business page, receive recommendations, feedback (which is very valuable), know your audience better, and target your communication. Facebook is not as friendly to business profiles as it is to personal profiles: you have to pay if you want your audience to see your posts. On the other hand, it provides perfect target options, so you can be sure your communication will find its audience.

**Twitter** is needed if you have something to say. It is a very fast medium, and if you don't post often, you can be forgotten very soon. You won’t be appearing in your followers’ and other people's feeds. There’s also an option to pay for distributing the content, but the targeting is not so good. You have fewer options than you have on Facebook and you will have a problem reaching the audience you want. Don't forget to use hashtags, so people can find you, while following the topic you are talking about.

**Linkedin** will help you with hiring, sharing your culture, vision, and B2B communication. It is quite expensive to promote the post or get a premium account, but still, you can do a lot for free. Give it a try and see how the audience likes your content.

**Instagram** is powerful for those who has something beautiful to share. If you are selling beauty products and services and targeting millennials, Instagram is the right platform for you. The audience there is pretty picky, so be prepare to dedicate extra time and focus on the content you are going to create. It has to be beautiful… or at least very interesting. Buy yourself special equipment for taking and editing photos.

There’s a lot more social media you can use, but check if your audience is there, and what content they’re used to consuming. If you have nothing to say, simply stay back. Don't forget, you have to be ready to chat with your current and potential customers. One chat can help you build your brand and also ruin everything.

Don't forget to educate yourself. Social media is fast-growing and everything around it changes quickly, so don't forget to follow the news about social media and stay updated.

### How to set KPI

How can you tell if your communication is working? It’s like trying to lose weight. First you stop eating chocolate and set yourself a goal to lose two lbs by the end of the month and you don't stop until you get there. Then you do some basic exercise and set a goal of four lbs. Then it’s 5 lbs and running to work. You see the progress and know what you’re capable of… and, importantly, you can see the results. It’s the same with setting up KPIs. You won’t see results and progress if you don’t set any goals. Try to set up reachable ones in the beginning, but enough to keep you motivated. The most common for social media are followers, engagement rate, and page rating.

### Analytics

How much weight have you lost? Buy a scale and check. What kind of scale does the internet offer for your communication? The most basic tool for measuring and getting information about people coming to your website is Google Analytics. GA works best with Google Tag Manager, which helps you see customers found your website (ppc, banners, social media, emailing…).

Every social media outlet has its own analytics which will help you to know/identify your audience and show you the content they are actually interested in. You can use the paid tools to compare all your social media channels and also compare them with your competitors. For example: Socialbakers.

There are also a few tools on the internet which help you centralize your communication, and share content among all your channels and measure the impact of your communication - a paid service, but definitely worth it. For example: Hubspot.com

### Who’s going to do it?

Your strategy is set, you picked the channels where you’ll attract your target audience, you decided which format you’re going to use. One question remains: who is going to execute all this? You can hire a marketing specialist, copywriter, graphic designer, content creator, media planner, web analyst, and others to help you to fill the strategy. Or you can hire an agency. Like everything else, agencies have a lot of pros and cons. First, agencies can be expensive. You’re actually paying not only for people to work on your project, but all their costs as well. On the other hand, they do have the most experience with communication. They are reliable, have thousands of campaigns under their belt, know every platform and audience, and have taken care of so many issues with different customers that they’re ready for almost anything. Because of this, they can save you lot of time, guard you against unforeseen difficulties, and prevent you from spending a lot of money on something that might not work. Or you can hire your own people, but this will also cost you a lot of time and money. You will meet dozens of candidates, more or less senior, with different needs, experiences, and expectations. We suggest you hire marketing people only for key positions in the very beginning. If you see your business is growing and you are able to produce hundreds of videos which bring you a lot of new customers, hire a video guy. However, we suggest you leave this “testing part” to an agency and see how it goes.
