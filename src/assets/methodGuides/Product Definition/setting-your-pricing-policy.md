---
type: Guide
category: Product definition
index: 4
title: Setting Your Pricing Policy
description: 'Your strategy for how, how much, and who you will charge depends on two main things: product type, and the nature of the market/competition.'
---

### Basic types of market and competition

- You compete with the traditional, big players as a newcomer. In this situation you will face fierce competition as you have to fight for the clients by lowering your price or having a significantly higher quality of service and product. Aggressive pricing and marketing will be needed and you can even expect heavy losses in the beginning, so you should have a strong capital buffer and a patient investor.

- You create a specific product, service, or a small niche business. Here it’s important to estimate precisely the number of potential clients and your ability to attract them. You will not face strong competition, so you’ll have higher control over the price, but you have to scale the company to the size and costs at which it is able to survive. So, you can afford higher margin, but should have more focus on cost control.

- A completely new opportunity, technology, or product, aka “Blue Ocean.” Targeting this kind of market is the most risky but also potentially the most rewarding. If the market potential is high, it will attract not only corporate players, but also lots of other startups. You will have to adapt quickly to the actions of the competition not only with pricing, but also in product development. The difficult thing is balancing pricing to be able to get enough clients, while at the same not dying because of overspending and losing favor with your investors.

### Product types which affect pricing and monetization strategy

**Digital products**

Clients simply pay for a digital service you provide. It’s common that some version of the product might be for free (trial period, freemium, etc.) to attract clients, or you offer different levels of service (free, basic, VIP etc). This needs to be tested in the real world to understand what clients are willing to pay for.

**Subscription model**

Clients pay a fixed fee and are allowed to use the platform freely for a period of time. This can be done again using service packages of different levels and prices to behaviorally steer the customer spending.

**Intermediary platforms**

Your product works by matching supply and demand for some product or service. In this case, the platform works on a fee that’s based on the number or value of the transactions. It is very crucial to understand which side of the transaction pays for it - buyer or seller. The cost impact can be the same, but behaviorally one side simply refuses to pay any fee.
