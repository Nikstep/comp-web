---
type: Guide
category: Product definition
index: 3
title: Building a team
description: When defining a dream team, you first need to look at the core team. Who are the people who founded the company? Who do you want to constitute the company later on? In the early stages, it’s very important who you pick; it will define your company. Put simply – A players hire A players, and B players hire C players. So be careful who you’re hiring for your team, even at the beginning when you might not have the most options. Consider what your company culture is already like at this point. How do they fit in? How will they contribute toward building the company culture you want to have?
---

### Important positions

At a minimum, you will always need to cover these professions:

- Product Management

- UX and Graphic Design

- Development

- Sales

- Marketing

- Customer support

Moreover, have a look at the team as a whole. Is it homogeneous or does it constitute people with different personalities, backgrounds, and knowledge? The risk of having a team of people who think alike is that they will probably not have the capability to think outside the box and reach new creative solutions.

Usually, in startups, no one does just one job. Are you picking the people who are able to jump on an issue even if it’s not in their job description?

### Four considerations when searching for candidates

Entrepreneur John Rampton lists the following:

1. They have experience in areas that other team members do not.

2. They can be vouched for—you know them or know someone who knows them.

3. They are able to start at a limited salary or for a stake in the startup.

4. They are fans of your product.

After you’ve answered all of these, you have a few options regarding the style of staff you'd like to have. Each has its pros and cons and will vary in different countries. The big question is whether to use employees or freelancers.

#### Employees

Hire employees if you are planning on keeping people in the company long-term or are forming the core of your team. Companies may feel more secure with a full-time committed employee because better relationships are forged and there's more guarantee the employee is involved in the company's own philosophy.

#### Freelancers

Get freelancers for jobs that are temporary in terms of time or skill needed for a particular project.Remote work has gotten increasingly popular in the last decade, and 34% of the American workforce is currently freelancing. Companies don’t have to provide benefits to freelancers, but issues can arise with deadlines and scheduling. Also, more people who become freelancers value the freedom of picking their project and people they want to work with. It will increasingly be the companies’ duty to find reasons and justifications for people to be bound to them in a traditional employer-employee manner.

The big differentiator will be interesting culture, cool office space, and the prospect to grow faster than they could by themselves.

In certain countries the difference is blurred or there is a tax benefit to one form of employment or the other. Please refer to your local tax and legal code for more information.

#### Outsourcing and recruitment agencies

As for outsourcing, it can be especially convenient to make use of talent outside your borders, but there is no guarantee you will get things done the way you want.

And when to use a recruitment agency? Ideally never. When you need a recruitment agency, it is already too late. In an ideal world, your company should be super interesting and everyone would want to work for you.

For smaller companies, the price of a recruiting agency can be prohibitive, therefore tapping your friends and family circle and your social media networks might be the best course of action.

Larger companies are accustomed to using recruitment agencies for staffing purposes as the plethora of skills required is hard to encompass in one HR department.

Recruitment agencies are a good match when you want to quickly staff up, like after a series A, or with rapid growth and enough cash flow to pay for the services.

However you find your people, after the candidate has passed your tests, you should do some form of post-hiring assessment. Creating a kind of structure for advancement from this criteria will also help motivate other team members in the future.
