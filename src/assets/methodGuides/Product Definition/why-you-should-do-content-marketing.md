---
type: Guide
category: Product definition
index: 7
title: Why You Should Do Content Marketing
description: 'Internet and offline media are full of ads. These can be video, radio spots, billboards, CLVs, or PPCs. It doesn’t matter. These are all just saying the same thing: We are the best. There is nothing better than us. Our product will save your life. Use us. BUY US! And they are everywhere. People have started to go blind to classic advertisements. They don’t trust what the ads say anymore. And they don’t even bother to see, read, or listen to anything you try to say. So how can you build your brand? How can you tell people you are here? It is simple: provide best-in-class service or do content marketing.'
---

### What is content marketing?

Let's keep the definition of content marketing simple. Content marketing can help you build a relationship with your potential customer and increase your sales chances by proving you are an expert and that you understand what you are doing.
Imagine you are going to buy a car. And imagine you are a woman who has no idea how to pick the car. You’ve only decided on the color. So what can you do? Ask a friend? Ask your parents? No! You are a strong independent woman, so you Google it.
Imagine you are a company that has just started selling a car. You would like to run your business in a different way. You want to be completely different then the company you bought your car from the last time. Those thieves! They sold you a car without brakes, they didn't know anything about servicing your car, and the guy who sold you the car also talked you into buying a polish which scratched the paint. You will never trust them again and you will never recommend their services to anyone.
You've decided to help people in similar situations, not by providing the best in class service, but by helping them avoid going through what you did. You will start your own blog where you will describe how to pick a good car seller. What to check before buying. What to ask the seller. What to be wary of when reading the contract. How to pick a good car. All this will show people that you are a good person. You are not a thief if you show them all the secrets! You know buying a car can be difficult, and you know people can get lost easily. You understand their needs and you won’t laugh at someone who has no idea how this all works. Oh! And you also sell cars?!
See? Do you get where am I going? It is simple, right? Show people you understand your field, you have experience, and you understand what they want. Be one of them. It doesn't mean you have to become a professional writer and start to visit creative writing courses… you can do basically anything you want to show you understand your target group.
Nike is a great example. They make sports equipment, like running shoes. So they started to support runners. They created Youtube channels with guides for [runners](https://www.youtube.com/watch?v=YFGw5pTcUl4).
They created a special application for measuring your runs, with a coach who helps you push your limits, achieve your goals, and lets you be the part of the running community. And the best part… everything is free, so anyone can join. Just listen to your target group and help them solve their issues. Be an expert!
