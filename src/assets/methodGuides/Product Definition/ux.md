---
type: Guide
category: Product definition
index: 1
title: 'UX – User experience'
description: UX is basically the most important process while developing a product. It will help you know your target audience and define your product. If done smartly, it can also help you avoid future mistakes and pitfalls.
---

### How to begin

When starting a digital company, or creating a product in general, you should begin by understanding its different aspects. Here is some of what you need to know:

- Why you’re doing it

- Who it’s for

- The desired result

- How to track it

- How to know it’s successful

- Who the future customers are

- How customers find their way to your product

For more information on defining your startup, see our “Idea Validation and Prototyping” article.

### What, why, for whom?

You should check available analytics, heatmaps, and customer behaviors. By analyzing current statuses and pain points you can then start to shape the basic architecture of what the product or brand should include. It’s important to create a screen flow (how things are connected) before making the design, to have a basic general overview of the whole project. Creating personas of model users and mapping their journeys through the product is also key.

From these previous steps you can start to draw the first sketches and wireframes.

Give shape to your idea, discuss the basic structure, layout, and contents with your team. Do many quick iterations to get the final structures and wireframes so the graphic design can begin.

### Design

When you start the graphic design phase, it shouldn’t be something random based on your taste. With the previous knowledge gathered, you should have a solid idea of what you’re doing and for who it’s for.

Style guides and mood boards are key to compiling a bank of references and inspirations. Creating clickable prototypes is very important so you can feel the product for real. You have to validate your results and collect real user data before and after the product is launched. Even after testing, reality will indicate changes and features that should be tweaked, so you should always iterate the product.
