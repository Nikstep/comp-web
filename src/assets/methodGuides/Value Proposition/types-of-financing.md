---
type: Guide
category: Value proposition
index: 6
title: Types of Financing
description: You’ve got some choices here. Depending on which market you’re entering or your own location on planet earth, you’re going to have different sets of options when looking for capital. The following covers these options and discusses when to use them.
---

### Investors

There are many good online resources for finding investors, such as angel.co, where you can find a cross section of the investment landscape. If you’re looking for something specific, here are a few categories:

- FFF/3F - Friends, Family and Fools

- Angel investors

- Seed investors

- Later stage investors

- Friends, Family, and Fools

These are the people you know - you should go to them in the beginning when you have little to show apart from yourself. The people who will invest are those who know you (except for Fools - who don’t have a better option to put their money in) and that’s what matters.

#### Angel investor

This is a person who invests in a business venture, providing capital for startup or expansion. Angel investors are typically individuals who have spare cash available and are looking for a higher rate of return than would be given by more traditional investments. An Angel usually looks for a return of 25x or more on his investment.

#### Seed investors

Seed capital is the initial capital used when starting a business, often coming from the founders’ personal assets, friends or family, for covering initial [operating expenses](https://www.investopedia.com/terms/o/operating_expense.asp) and attracting [venture capitalists](https://www.investopedia.com/terms/v/venturecapitalist.asp). This type of funding is often obtained in exchange for an [equity](https://www.investopedia.com/terms/e/equity.asp) stake in the enterprise, although with less formal contractual overhead than standard [equity financing](https://www.investopedia.com/terms/e/equityfinancing.asp).

#### Later stage investors

Capital provided after commercial manufacturing and sales but before any initial public offering. The product or service is in production and is commercially available. The company demonstrates significant revenue growth, but may or may not be showing a profit. It has usually been in business for more than three years

### When to find a loan

When you’re down on cash and need to survive. Also, when you have a business model that shows recurring revenue, signed contracts for the future, or you need to finance a development and marketing push, and are really certain you will succeed and don’t need to give up equity.

### Borrowing money from parents/family

Family monetary transactions can be tricky, because the reciprocity is usually unknown and may become problematic somewhere down the line. Also, if your parents aren’t Bill and Melinda, and the money they give you is the only money they have, be cautious of how you are using it.

### How to ICO

An Initial Coin Offering (ICO) is a relatively new, sometimes controversial option involving cryptocurrencies. Companies may raise funds by offering digital coins to investors. Read more [here](https://u.plus/startup-guide/scaling-stage/how-to-create-an-ico).

### How much money will you need?

As much as fulfills the business plan for the amount of time you need the money. For example, if you ask for \$1 million and there’s a roadmap attached to it, when fulfilled, your company has a higher value and you’re more trustworthy for future investors. Thus you will raise money for a higher valuation. If you ask for too much and you are successful, you gave up too much equity at a lower valuation.

### How big should the investor’s share be?

This should be governed by the valuation of your company at the moment of the investment.

### Who to contact if you want a US investor?

Ideally your national investment agency can connect you with the right people and give you free advice. If you don’t have this option, join online groups that revolve around that subject and ask for help.
