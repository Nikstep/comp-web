---
type: Guide
category: Value proposition
index: 7
title: How to Pitch
description: With a business plan laid out, and some idea of where you’ll seek financing, the next step is presenting your fledgling startup to the world. Here we lay out some great practical tips for pitching and the mindset you should have before going into meetings.
---

### How to prepare a pitch deck

You should check off the following:

- Product

- Market

- Business model

- Customers

- Team

- How much you are asking for

- What you’re going to spend it on/roadmap

When you have these, and know in-depth information about every category, you are OK. What’s important is your audience. A lot of startups come to the investors ambivalent about needing money. Even if that is the situation, you shouldn’t show it. Investors see your startup as an investment vehicle—disregarding every other motive, they are interested in giving you a \$1 and getting a multiple of that some time down the line. That’s the perspective from which they will be judging your company and you as the founder.

### Tips for pitching

#### Make sure you’re prepared

This sounds obvious enough, but it’s one of the most common mistakes. You might be a great natural speaker, but coming unprepared will turn you from great to mediocre. (Prepare even more if you’re going to speak in a language that isn’t your native one).

#### Speech and body language

You don’t have to be an award-winning actor, but speaking too quickly or too quietly can really put the crowd to sleep. Pay attention to your body language. For example: don’t gesture to the presentation wall with the wrong hand. You can end up turning your back to the audience and talking to the wall.

#### Speak elegantly and confidently

Avoid throwaway words— “basically”, “like”, “you know”, etc. Don’t ask the crowd for affirmation with the word “right?” at the end of your sentences, it weakens the power of your presentation. Don’t apologize, don’t promise to go through it quickly, don’t speak badly about yourself. Even if you’re not confident, try to project an aura of control.

#### Stay cool

Don’t panic if you lose your train of thought. The audience wants to see you succeed. If you lose track, just stop for a second, breathe in, breathe out and continue. You have more time than you imagine.

#### Tell us what YOU can do

Don’t remind everyone how big the market is—that’s a common refrain. You have the stage, so tell us more about your product and how exactly you want to disrupt the given market. Of course, a big part of this comes from fully researching your competition, which is essential for any stage of your startup journey.

#### Know your audience

You should have different approaches for investors with and without knowledge of your field; for startup people; for just presenting your idea without the need for investment; for hiring; and, for example, if it’s a US or Japanese crowd. In addition, tread lightly with regard to political and sexual jokes or swearing.

#### Show them the money

Discuss your revenue model. If you pitch an investor, their primary concern is how you’ll get them $16 if they give you $1.

#### Keep it humble

Don’t be offended by criticism that’s what it’s all about. Judges and experts are here to help you by providing helpful feedback based on their experience with startups. Nobody can force you to agree with their advice, but be open enough to listen.

#### Craft a narrative

Follow a narrative order. Begin with the name of your startup and your name, then proceed to the what, why, and how; your business and revenue model; why you; what you’re looking for; how to reach you (contact page). Make sure your slide design is clear and elegant, especially if your target customers care about visuals. The main purpose of your pitch is to generate interest for the next meeting.
