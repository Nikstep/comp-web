---
type: Guide
category: Value proposition
index: 2
title: Idea Validation and Prototyping
description: Validation is important to do right from the start, and you should keep doing it throughout the life of your company. Having a prototype is a great way to validate because it puts your ideas in the realm of the usable, making feedback easier for all sides.
---

### What is validation?

Before you build anything, you should do your homework. This may seem like a drag, but it will actually save you huge amounts of time and money down the road. In the bluntest terms: you need to make sure that your idea will interest people enough to bring you money. Maybe your idea came on like a bolt of lightning, and you feel the world should be exposed to it as soon as possible. Still, validate.

### Ask the right questions

Whether you have an idea based on years of personal experience, or a eureka moment about something entirely foreign to you, you need to understand the market and the problem you’re solving. Any startup can do exactly what it’s supposed to, the question is whether its function is connected to a strong need. If entrepreneur and investor Dave McClure is right, that you must “pitch the problem, not the solution,” then it truly behooves you to have a great problem on your hands.

### Get out more

After designing your business canvas (see previous article), you should “hit the streets” and talk to people. Don’t be afraid to share your idea. You should be talking about it all the time, asking for as much feedback as possible. Entrepreneur Steve Blank points out that each encounter might not mean all that much, but it’s the cumulative knowledge you gain that gives you the information you need to take your idea in the right direction. Listening is most important. At this stage, you have to reach the problem/solution fit. You need to know that most people are enthusiastic about your solution and willing to pay.

### Research methodology

Find out about your competition (see previous article). While you want to make sure your idea doesn’t already exist, it is possible to disrupt a market that already seems to be taken care of. Facebook came after Myspace. With potential competitors, consider the following: how they communicate, what tone of voice they use, name, colors, style of photos, prices, social media, EVERYTHING.

You can be blinded by your own passion and obsession without admitting that the idea is not that good. Know when to stop and shelve it or put it away. Set a point for when you’ll decide the world is not ready for it and move on. Having said that, we’ll now move to prototyping.

### Building a prototype for idea validation

You’ve done the research and now you’re ready to build something that users can actually interact with. Why should you make a prototype before you start selling your idea to investors? Because prototypes diminish the risk in creating a digital product.

#### The benefits of prototypes:

- Fail quickly and cheaply

  Prototypes give you the ability to iterate ideas to achieve your product/market fit. You can iterate as many times as you want, based on feedback from potential customers and investors, which helps to quickly improve your product.

- Learn from feedback

  Users of your product tell you stories of what they expect, which features they’d welcome, how they feel when using your prototype, what they understand and what they don't get. If you’re seeking potential investors, a prototype will show them you’re serious.

- Make your case visually

  Showing your users or investors what your idea looks like in reality rapidly increases your chances of success. To have something in your hand is easier for people to understand than merely imagining it. Guy Kawasaki, renowned author and VC, said he prefers a prototype to a really good pitch.

#### Process:

- Prepare content

  The goal of most websites is to get visitors to the content they seek as quickly as possible. Simple content accompanied by beautiful, intuitive design is the best way to accomplish that.
  The general attention span is about 3 seconds. Forget long paragraphs of text, no one’s going to read it. Make it short and to the point. But don't exclude our favorite thing… emotions!

- Sketching

  Write everything you know: content, user flow, pages you’ve had in your head. Then start wireframing sketches, page by page. You’ll be surprised how many things you forget: on-boarding, log in, mid-steps, etc. Each page. Then think about different cases and journeys of your users. Are they covered in your sketches?

- Wireframing

  The creation of wireframes. Putting each element in its right place.
  Here it’s going to be tricky without UX (user experience) knowledge, because just putting content on the page doesn’t make it efficient and user-friendly. There’s a reason why a UX designer made a particular button a certain width and put each element where it is.

- Design

  Transforming wireframes into visually appealing creations. Here come colors, shapes, photos, emotions, fonts, aesthetics, and playing around with elements

### Landing page

AKA “product page” is a fake page with value propositions. User’s iterations will be required. A PPC campaign is set up to track how many people have come to the site and how many have done a certain action. For example, if they click on a button to buy, they’ll be informed that the product is still in development and they can leave an email.
