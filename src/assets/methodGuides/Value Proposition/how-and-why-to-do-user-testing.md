---
type: Guide
category: Value proposition
index: 4
title: How and Why to Do User Testing
description: '<p>User testing is the necessary green light for every project to have a usable product and a satisfied client.</p><p>Nowadays there are a lot of companies that try to make apps to make life easier. The difference is that not all of these companies deliver good digital products that are actually useful for end users. This can be caused by several factors, but an important one is that skipping user testing may deliver a product that is not user-friendly.</p>'
---

### What is user testing?

User testing is actually done whenever people use a product, but here we’ll look at when a specific user goes through a task and a reviewer observes how they use it, what their difficulties are, what is missing, how easily they can navigate it, etc. Every feedback given is recorded and may be used for every step of development.

### How to do user testing

It is very important to have a scenario before starting with a description for the task. The following are the ways user testing can be applied.

### Moderated user testing

This is a planned meeting to monitor how the client goes through the task. The client may talk out loud during every step or just keep notes during the testing. There may be a lot of questions from both sides, which are good to keep as notes to prevent further mistakes. But this may not be the best way because it is time-consuming.

### Remote moderated user testing

This can be done by screen sharing or other external tools where you can follow and record how the client goes through the app. This is a good possibility because everything can be tested and you can see if additional features need to be added. Remember not to force the user in any direction, let them be free to navigate the site or app because observing their behavior is the most important part.

### Eye tracking

This is an advanced method of user testing. The user gets a sensor that knows where his eyes are focused on the site. By the time the user finishes, the reviewer will have a map of the site with a lot of details about the user’s actions. Analyzing this map can help understand if the user had trouble understanding certain sections of the site, and it’s also good for checking different behaviors. Only a few companies apply this method because it can be expensive for the client.

### Expert review

Done by experienced staff who evaluate the product. Users will go through the tasks and detailed notes are taken about some predefined conditions pertaining to the usability.

### Recommended methods

Often, our first step is to create personas (users with some personal details) from the UX team. Test cases should be created in advance to cover every possible feature to be tested. In the meeting, the client might be with some of his employees or even some random people who don’t know anything about the product. The average number is around to three to four individuals. The role of the facilitator in the meeting is very important. He will be keeping notes, recording comments, and maybe giving a hint if the user needs it, but not helping the user check the task. During this time it is necessary to observe how the user behaves. Keep in mind that every time you ask the client constructive questions in order to get detailed answers, it will lead to some helpful changes for the user. It is always a good idea to test every feature, but this requires a lot of time and a lot of money.
