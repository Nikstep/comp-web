---
type: Guide
category: Value proposition
index: 8
title: MVP Definition
description: This is the first time you will have something to show users.
---

### What is an MVP?

MVP stands for Minimum Viable Product. This is the smallest version of the product that still captures the essence of your company’s value to your customers. It also needs to be usable in its entirety. For example, if you’re building a mobility company to take people from place A to place B and you dreamed of building a car, the MVP would not be one or two wheels of your car. It would be a skateboard or a scooter for a very specific place-A to-place-B combination.

Said differently, MVP is a “mindset that says, think big for the long term but small for the short term.” You might have a million ideas from super-talented team members, but if you add features, really understand why you are adding them based on the needs of your users. However, you should also make sure you are catering to a good cross-section of customers, not just a select few early adopters. The key here is to always be validating the idea. Talking to users is a must.

Finding a balance point between what to include and what not to include in the MVP might be hard. But you have to keep an eye on the schedule—an MVP must be released [fast](https://u.plus/startup-guide/mvp-stage/scaling-your-product-don-t-hesitate-iterate) to mitigate possible losses if the idea turns out to be unprofitable. If you are in doubt whether to include a certain feature or not—do not include it. It is much easier to implement it later, in a running product, than releasing your MVP too late. Anyway, there is some inertia, and it will take time for more customers to start using your product. Thus, you’ll have some time to implement other features, or decline them according to analytic data from the early stages of running your MVP.

### Agile thinking

A more systematized method of doing the above is with the Agile software development manifesto. In short, success doesn’t depend on doing everything perfectly, all at once, but rather doing many fast iterations to fine-tune your product. Frequent releases of working products is the fastest way to learn what your company needs to succeed. See our separate [article](https://u.plus/startup-guide/mvp-stage/agile-process) on Agile development for more information.
