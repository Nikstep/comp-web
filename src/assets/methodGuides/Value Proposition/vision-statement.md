---
type: Guide
category: Value proposition
index: 3
title: Vision Statement
description: The most common mistake in defining the vision of your startup is that it isn’t reachable. Such a vision doesn’t motivate anyone—quite the opposite. Visions must be understandable, memorable, and have an impact. At the same time, your overall vision shouldn’t be innately tied to current trends or minute specifics. It’s a delicate balance, but the right ingredients will help you build your team.
---

### Vision statement or mission statement?

Simply put, a mission statement is based in the present. A vision statement should be for the long term. This statement of purpose should be used to attract the best employees who can identify with your idea and begin to breathe life into it. Personal connection with future employees and investors is very important. Meet them, go out, have a drink. Your vision statement isn’t a little wish written on a scrap of paper in your desk drawer, but something you live as your company begins to grow. But the vision statement doesn’t have to be too specific. A later article discusses this with OKRs.
