---
type: Guide
category: Value proposition
index: 5
title: Business Plan Creation
description: Just as any long journey needs some form of navigation, your startup needs its own guidelines. The business plan involves answering key questions that help define what you’re trying to do and how you’re going to do it.
---

### How to write a business plan

Keep it short and to the point—online templates for business plans can be intimidating with examples of really long documents and detailed analyses. We recommend using this template: [http://100startup.com/resources/business-plan.pdf](http://100startup.com/resources/business-plan.pdf). It is important for you to have all the detailed information handy, but it should not interfere with what you are trying to accomplish. Always include an executive summary for those with little time to browse through your whole plan.

Your business plan should be first and foremost your guide to building your company that others, potential investors, can understand as well. It will work as your roadmap going forward.

There are a couple of touchpoints you should definitely include:

- Value proposition

- Customers

- Roadmap of execution

- Team

- Financing needs

#### Value proposition

This is the definition of why your company is special and what value it brings to its customers.

#### Customer definition

It’s important to understand the type of business and target audience for the company’s products. Also, for specialized VCs, this is an easy signpost to understand if they bring any additional value to the company on top of the money.

#### Roadmap of execution

Will it take two years? 6 months? What are the necessary steps and critical path for the company to be successful? Lay it out as clearly as possible and identify potential issues and solutions.

#### Team

Who are your co-founders and what is the division of labor among them? What roles are currently missing? Who will you need to hire? In what timeframe?

#### Financing needs

This includes the amount of money you require, for what purpose, and when you’ll break even. If this is the bedrock of your business plan, you should include detailed calculations and justifications, which can be in an appendix.
