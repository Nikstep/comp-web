---
type: Guide
category: Value proposition
index: 1
title: Finding and Defining Ideas
description: So you have a startup idea? Nice! But you’ve got to be prepared for your idea to change several times. Maybe you’ll find that someone else has already realized your idea, or there’s a lack of interest in it. Let’s introduce you to some ways to prevent these issues and also determine how much money is necessary.
---

### Find a problem and try to solve it

Is there a problem out there that makes you angry or emotional? Obviously, you’re not going to fix the world’s problems by yourself. But you can contribute and make a difference, which is ultimately what’s most important for your own happiness and fulfillment. The problem you tackle could be related to your everyday life or previous experience, but it doesn't have to be only about you. It could be something your friends and family deal with, or the community around you or, as mentioned, world problems

### Incorporate your experience, strengths, and knowledge into your ideas

Maybe you once had an exciting idea and maybe you even told your bosses about it. Maybe there were other priorities, or the company was too big to change quickly, or wasn’t interested in adding new features. Don't throw away all those years of experience. Think about what you learned and what you saw that was being overlooked, or issues with how your company operated on the market.

### Has anyone beat me to it?

You’re drinking your morning coffee and all of a sudden you’re blessed with a great idea. First, you need to do a bit of research to see if similar ideas already exist. At U+ we use https://www.crunchbase.com/ where you can quickly find similar launches. Crunchbase is also a great tool for getting ideas, and we recommend spending a few hours studying the site.

Ok, so you’re sure that your idea hasn’t already been developed by someone else. Sweet! Now you can start writing a description.

### Define primary motivation or personal goals for inventing your idea

Understand what you want from your business. It could be:

- A hobby-based business, like making soap to sell on Etsy.

- A part-time lifestyle business that could become full time, like a wine investment club, or a full-time startup hoping for acquisition in a few years.

- A large, cash-flow positive business (e.g., B2B furniture import and delivery business).

- A path to industry credibility and networking over financial gain (e.g., a scriptwriting peer-training exchange for aspiring comedy writers).

### Business Model Canvas

If you’re sure that your idea hasn’t been developed by someone else, you can start writing a description of it. This step is important because you’re trying to figure out who your target group is and the value you can offer them.

A form that has proven to work at this stage is the Business Model Canvas. You can see a short introduction about how it works in this video [here](https://www.youtube.com/watch?v=QoAOzMTLP5s). There is also a great paid [tool](https://strategyzer.com/). However, you can simply use a [printed](https://www.google.cz/search?q=business+model+canvas&source=lnms&tbm=isch&sa=X&ved=0ahUKEwiHj-_LoLfZAhWpCsAKHendBpYQ_AUICigB&biw=1440&bih=826) canvas and put notes on it.

It’s important to note that the next phase is the validation of your canvas, and much will change, so don’t write anything permanently on it.

For us, the categories of “value propositions” and “customer segments” are the two most important items and need to be given a great deal of attention. Value proposition is the reason to buy your product. If this category is weak, it will mean the end of your startup.

The customer segment is the person who buys your product. Try to be as specific as possible. If you think your clients are all in the demographic of 30 to 40-year-olds, you’re probably wrong. You have to define your customers in detail, not only demographic data, but how they behave and what problem you’re solving for them. For example, the problem of sleeping at night.

Value Proposition Canvas

The value proposition canvas was created to better map value and target group. You can download a helpful free template here.

- Great value propositions

- Are embedded in a great business model.

- Focus on jobs, pains, and gains that are the most important for customers.

- Focus on unsatisfied jobs, unsolved pains, and gains not yet achieved.

- Do a great job at targeting a select few jobs, pains, and gains.

- Address emotional and social jobs beyond typical paid careers.

- Figure out how customers measure success and align with it.

- Focus on the jobs, pains, and gains people are willing to pay for.

- Differentiate from their competitors on how to alleviate these issues.

- Beat the competition in at least one area.

- Are special and difficult to copy.

Other fields on the business model canvas are not important yet, so you can skip them and go back to them after value validation and target groups.
