---
type: Guide
category: Further iterations
index: 7
title: Expanding Company Structure
description: Feels good to be in the scaling phase, doesn't it? This is where you can see your success becoming a reality and transforming into even more exciting opportunities. However, growth brings new challenges, so it's good to come prepared. What are some of the challenges you might face?
---

### Scaling shifts your team’s focus

Processes and day-to-day activities that worked so far might not work for you anymore. Things that could be done by a few people might now require automating because you can no longer dedicate so much time to customers/requests. You may have to re-design all your processes.

Testing becomes more crucial than ever before. More customers = more bugs. Growth will be slower than you’d want, but don’t let that get in the way of giving customers what they want. In addition, you need to start preparing for situations even before they occur. It’s not sufficient to just hotfix as you go. You should invest your time and money in things that will make scaling as painless as possible.

### Scaling is expensive

Your expenses don’t only increase due to hiring. Costs arise such as getting more server support or new tools that have now become necessary. Pricing and conversion optimization are extremely important to the survival of your startup. Automate where you can. Development, marketing, and customer support need a human touch, but automation systems can be used to help speed them up greatly. The are plenty of apps you can use in development, marketing etc. to reduce the workload that would otherwise have to be done manually.

### Scaling amplifies everything

The larger you get, the more problems you’ll have, so be prepared. The information flow needs to be re-assessed because the more people/divisions you have, the more difficult it becomes to share and act upon the information. Hiring people based on quality is the most important thing, even if it takes longer to find them. Conversely, don’t be afraid to fire when necessary. And make sure you have a great team of managers from the start.
