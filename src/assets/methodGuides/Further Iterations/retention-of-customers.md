---
type: Guide
category: Further iterations
index: 6
title: Retention of Customers
description: Keeping a customer is part of your marketing strategy. This is the Care phase. Forget about classic loyalty programs that are based on collecting points. The key to keeping a customer is the relationship between the two of you.
---

### Build a relationship

Don’t forget to send customers a notification when you add a new feature and also ask them to test it out. They will like being the first to have access. Also, create interesting content for them in monthly emails. You can also make a closed Facebook group where you can share your experiences and news. At the holidays, don’t forget to send greetings. Customer retention is about building a relationship.

If the only form of communication is an invoice in an automatic mail, the customer can stray to the competition even if you have a great product.

Ask any customer who has left to share their reasons. You may be surprised to hear what they have to say.

### Measure

You can also measure how your customers use your product and their level of engagement, for example, the number of logins or actions that users make in your app. You can tell in advance that something is happening and prevent customers from leaving.

It’s also good to measure churn rate. This metric tells you how fast you are losing customers. If the churn rate is greater than the acquisition rate, your client base is shrinking and you have to deal with it.
