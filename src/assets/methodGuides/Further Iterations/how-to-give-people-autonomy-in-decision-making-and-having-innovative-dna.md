---
type: Guide
category: Further iterations
index: 11
title: Culture with an Innovative DNA
description: '<p>Everything starts with picking the right people for the job. You want to hire people who can self-manage and who continuously surprise you with their results.</p><p>If you don’t have such people, chances are you picked incorrectly. This article details some management perspectives. (And how do you pick the right people for your company? First read the Startup Guide article on putting together a team)</p>'
---

### Freedom

Now let’s assume you have the right people. Do you like to be micromanaged? No. Nobody likes to be micromanaged. Don’t micromanage people. If you really picked the right people, and they continuously surprise you with their results and manage themselves in the face of ambiguity, just set checks and give them information to decide on for themselves.

If you both make the same decision from the same data, all is good. If not, then dig deeper and try to understand the differences in thinking. Every such interaction is an opportunity to improve a process to be more self-sufficient and consistent.

### What if it is not working out?

Are you constantly reaching different conclusions? Has there been a drop in motivation over time? Innovative DNA is very fragile and can be dumbed down by demotivation and choosing B players for your team. When this happens it is important to part ways with that person to keep the culture intact and bring in new blood.

### How to manage expectations with new hires?

When hiring for higher level positions, you can easily fall into the trap of them knowing your brand and being sold on your company. They slide through the interview process everybody is super happy and then reality hits. Things get messy; the last company they worked for had much better processes and this is not what they signed up for. Make sure that while choosing the right people for your quickly growing company, you are equally honest about the shortcomings as the fabulous wins—you will need to find people who are high level, but are also willing to share their knowledge with everybody else.
