---
type: Guide
category: Further iterations
index: 4
title: Recruitment Management and Role Definition
description: It’s obvious that role definitions are important to have figured out even before you start hiring, so the fact you’re reading this article shows you’re on the right track.
---

### Role description

A general role description should include: the purpose of the position, responsibilities and competencies, and, last but not least: key performance indicators or KPIs (how you measure if the employee is doing a good job).

But since startups are ever-growing organisms, it’s not always one person doing just what’s within their role definition, and often the role you had a year ago may no longer apply or has transformed a lot since then. Thus, it’s useful to sometimes re-evaluate positions in the company and see if they serve the same purpose as before.

### Role development

The most common role development within the first few years of a growing startup is that people who were there from the beginning of the company start to take on managerial roles. They maybe did a great job designing a product or setting up the architecture for development, but now they've started to manage not just the project but people as well. These are a few tips to better handle the challenges that most of new managers have to face:

- When leading people, you should figure out what you expect them to do. And don’t forget to align your expectations with them, otherwise they will not perform as you expect, or will not have a clear vision of why they should do the tasks. Don't forget to share your expectations and focus on communication in general - try to be as transparent as possible. Take a look at our OKRs article where you can learn about how to set proper expectations.

- You will have plenty of things to do and too little time to do them. One simple way to better manage your time is to use the [Pomodoro Technique](https://en.wikipedia.org/wiki/Pomodoro_Technique). Also, don't forget the [Pareto principle](https://en.wikipedia.org/wiki/Pareto_principle).

- The sudden shift in mindset from being a co-worker to a boss might be tricky to handle; some people struggle with it, and it tends to encompass two extremes. Being a friend but not being a enough of a boss, or acting from a position of power and forgetting the human side. It is good to keep in mind that you now hold some power, but you need to use it effectively.

- Hiring and firing people for the first time might be uncomfortable. Hiring is an important part of building out a productive team. You should consult with your HR about best practices and techniques. If you don't have a HR person, consider using the services of professional recruitment agencies or at least consult with them.

Firing people is never easy, but the first time might be especially tough. The best advice comes from meeting with your team and asking about other colleagues. If you see certain patterns emerge, help them improve, and then reassess the situation.

The most important thing to remember: if in doubt, don't be afraid to ask for help. If you don't have a mentor or someone in your environment that can help you, look into courses and training you could take or get coaching from a professional coach.
