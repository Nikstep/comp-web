---
type: Guide
category: Further iterations
index: 9
title: Why Should the Founder/CEO Be Active on Social Media?
description: Recently, we had to convince our CEO to write a blog and share his thoughts on social media. This is not an easy thing to get a CEO to do. They are usually super busy and if they do have time for social media, they often use private profiles showing pics of their kids or dog. So, why is it so important to use social for work purposes?
---

### What does the CEO represent?

The CEO is the face of the company. Nowadays, it is really important to show the people behind the company, so everyone can imagine “who” your company really is. The CEO should exhibit expertise, knowledge, wisdom, and experience. People want to know who they are giving their money to. And not only people—the media is interested in who they are going to talk to when preparing an interview or why, for example, they should invite him/her to a conference. How will they know this if your CEO is just a phantom figure in the back of the company?

### What to write about

It is not necessary to convince your CEO to write a daily report on what they’re doing. Nobody is actually interested in that. You have to find topics your CEO is good at and connect it with the mission/vision of your company. For example: if you work for an advertising agency, he can definitely write about trends, how to build your brand, and use your company as an example. He can definitely comment on the topics from your field and air his opinions of daily topics on social media. And it is always nice if he can describe the current situation in your company.
Maybe your company gets into a difficult situation—a personal statement from the CEO on social media can carry more weight than a super-formal press release since it can lend a personal touch and show your followers that your CEO actually really cares about the situation more than a press release (which is also important) can help your CEO statement published publicly. You CEO can help his company build a brand (which helps with sales), share the culture (which can help with hiring) and show the strengths of the company (which can help with PR). If your CEO doesn't talk or share his knowledge publicly, everyone might wonder if he really has anything to share or talk about.
It is not always necessary to let the CEO be the only one who contributes. It is always good to show more people from your company. Remember: as we’ve done with this guide, everyone who has something interesting to say, who can help your customers, and who can help build your brand, should be welcome to share their expertise.
