---
type: Guide
category: Further iterations
index: 1
title: Goal Definition and Growth Strategy
---

### Growth strategies

You have achieved product/market fit and are ready to scale! There are multiple strategies you can use:

- Organic growth

- VC-fueled growth

- Competition acquisition

### Organic growth

Continue growing at a pace that is sustainable given your revenues and costs. There’s nothing wrong with this - the benefit for you is having fewer stakeholders in your company and being able to retain big control over the direction and decision-making.

### Investment-fueled growth

You are most likely past your seed stage and are ready to raise a Series A to kickstart your growth. Please refer to YC’s checklist for all the documents, etc. that are usually required. Basically, you need to sufficiently demonstrate that you’ve reached product/market fit, have revenues coming in, have tested ways to acquire new customers, and have a financial model. As in previous chapters, this will be primarily a financial transaction, so the VCs need to clearly see that if they invest \$1m now, you will yield them a multiple of that further down the line.

### Competition acquisition

This is a strategy to consider if you’re in an oligopoly market where only a handful of companies can survive, so you can get a higher market share to render your business model profitable. The other possibility is talent acquisition for growth in a specific area that is hard to hire in.

### Goal definition

Regardless of what you chose in the previous chapter, it is important to measure it and know if you are meeting your goals. As in the very first stage, OKRs can be a helpful strategic tool to keep the company in check while offering personal goals for everybody.

At this stage you should be looking at your growth rate, runway (if you have VC money), MAUs (monthly active users)/MRR (monthly recurring revenue), customer lifetime value, and multiple cost-side metrics.

On the web/app side you should always understand what your conversion funnel looks like i.e., how many people come to your site, how many go to the trial, and how many ultimately convert to paying customers.

It is important to understand how these metrics influence each other and at what levels you reach profitability in various channels.

Your goal setting should be dependent on a combination of these values and focused on becoming profitable. You should also make sure to maintain focus on your long term vision of the company so you don’t stray from your end goal.

### Optimization

You need to understand what makes your users tick and what to do next to make them happier and pay more. In the majority of cases, it takes a lot of iterations to figure out what those things are and you need to rewrite major portions of your product and fast - otherwise you can expect to close your business.

You probably have a lot of technological and organizational debt - that’s okay. Identify all areas that require change and prioritize them according to your business needs and their impact. Now is the time to set a strong foundation that you can rely on in the future.

### Team

What roles should you have on your team and should they be internal or external? As a rule of thumb, all the roles that impact core value/USP in your company should be internal. Think back to your exit strategy and the reasons for acquisition/exit. Is it the technology? Then you need to have the core tech team in-house. Is it users? Then you need to have everybody who takes care of acquisition and retention in-house. Is it your talent? Then just do good work.

At this stage of growth, you will likely need to start investing in overhead roles like HR, in-house accounting, and more Q.A.

It is important to hire because of fit and drive and not only because of skill. You want to avoid hiring someone who you sell on the promise of your company when the current reality is far from that. You need people who are able to work with you to inch closer to a bright future and understand the challenges to come.
