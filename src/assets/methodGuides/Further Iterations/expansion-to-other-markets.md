---
type: Guide
category: Further iterations
index: 5
title: Expansion to Other Markets
description: So, you’re happy with your product? It helps people, they love it, and want more? That’s a great feeling. If it helps people in your country, maybe it can help those in other countries. Wouldn’t that be great? Let’s do it!
---

### Market analysis and market entry

Entering a new market is a costly endeavor. Before committing to one particular market you should conduct as many tests as possible in order to understand the viability.

Regardless of your type of product, the first thing you should do is to understand whether what you sell in one location can be sold without modifications in another location. Different markets will probably require different modifications, so you can make a list. If you have no modifications, perfect.

The next step is to understand the demand on the new market - if you have an online business you can create a localized version of your landing pages and drive traffic to them. These can be real or a so-called “fake door” website that seems to describe a complete product, but when people subscribe, you tell them something along the lines of “thank you, we’re launching in your location in three months.” That way you get the sense of real demand.

If you have a physical product or require someone to be present to make a sale, the best person to send is you - you understand the product best, and if expansion is really important, only you can make the necessary changes to the core of the company to be successful.

After you gauge interest and know the modifications required, you should have a pretty good idea which, if any, of the markets are a good start.

### Localization and customer support

Localization is important for products that are not used by multinationals or meant to be used in the local languages. That is kind of obvious, we know. What is usually not so obvious are the changes both in the UI and UX of the product and the operations of the company.

The typical example for the Latin alphabet is translation into German, which can have very long words that buttons and UI are usually not equipped for, unless your design was meant to accommodate them from the start (which, if it’s the case, should also make your UI fine for any other language.) Expanding to right-to-left writing countries like those speaking Arabic or Hebrew? Get ready for your whole UI to be mirror-flipped.

And of course if you have a website localized to Mandarin, people will write Mandarin on your customer support, so operationally you need to be able to have people speaking all the languages you have your interface localized to.
