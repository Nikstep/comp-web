---
type: Guide
category: Further iterations
index: 8
title: Scaling Your Product — Don't Hesitate...Iterate
description: '<p>Whoever said &ldquo;good things come to those who wait&rdquo; no doubt missed the bus a lot, and is probably reading this piece thinking &ldquo;just a few more changes, and then we&#39re ready to ship.&rdquo;</p><p>The world we live in today does not allow for us to wait and make things perfect (sorry, grandfather), especially in the tech space. By the time you have “perfected” your product, the industry as a whole has already started to migrate towards something else, and the users and their short attention spans are long gone.</p>'
---

### Fast market, fast products

With things changing so fast, it goes without saying that getting your product to market quickly, within a reasonable MLQ (minimum level of quality), depends on how quickly you get feedback on your product – giving you the ability to make changes at the same speed as the market expects them.
Now, I understand that this movement is very much focused on products – mainly software related. But I see this strategy as an entire element to your business model, strategy, and overall culture. While I don’t sign on to the phrase “fail fast,” I do however believe in constant change and improvement through education. It is my belief (though it’s brashly worded) that “F*ck it… Ship it” is addressing this very point.

### Roll it out

It’s called continuous delivery…not perpetual development. With continuous delivery you produce updated versions of your product in short sprints and send them out into the world. This is opposed to the methods of certain corporations that seek a kind of glacial perfection before finally releasing their “new” product.
Large corporations such as banks may be hampered by bureaucracies that prevent timelier releases, but in our world of ever-increasing speed and little patience, where corporate dinosaurs are being disrupted by hot new products (think TransferWise vs. whatever your bank is), the startup-style approach is becoming more relevant than ever before.
For iteration heroics, take the example of the messaging platform Slack. Here’s a company that wasn’t afraid of an identity crisis and was willing to morph into what users wanted – because it found out what users wanted (heck, its original version was for a videogame). Later in Slack’s development, the team realized that their application was functioning differently with larger groups. They went back to the drawing board and began another process of testing with user groups of various sizes before rolling the product out to a wider user base.

### Who are you?

While this “movement” grows traction, you as a business owner have to ask yourself if this development method is the image you want for your products and company. Are you the one that delivers a beautiful and “perfect” product when you think it’s ready? Or are you someone that delivers a product for the market, by the market - where you give your users the ability to test-drive and shape your products and company based on their needs, which is, after all, the most empirical method for fine-tuning your product. **Iterating is not ragged imperfection, spewed out haphazardly, but a path to higher perfection.**
Lastly, you have to ask yourself if your market is ready and understanding enough to test “imperfect” products, in the hopes that the end product, while never finished, will essentially be built by them through constant iteration.
**Humans are built through constant iteration, why shouldn’t products be?**
