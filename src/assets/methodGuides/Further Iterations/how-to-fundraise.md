---
type: Guide
category: Further iterations
index: 2
title: How to Fundraise
---

### Funding sources

Before getting external financing, every company should think of whether they are not able to at least partially fund themselves by optimizing their financial structure. The first natural source of company financing is profits retained in the company. This is of course hard to reach for a startup trying to scale, but you can reduce your capital needs by optimizing your operations and cash cycle. Many industries and products have the possibility to get paid from the customers up front, before actually providing the service or product (e.g., subscription businesses, insurance, credit card payments before product delivery, etc.). If you operate in B2B business and your clients are companies and institutions, it is in many cases possible to negotiate advance payments as well. On the other side, your suppliers are in some cases able to accept delayed payment conditions. All this is able to generate some surplus cash which then at least reduces your need for external capital.

Strategic partners are another way to finance yourself. Your large suppliers or clients can be interested in long-term cooperation and mutual vesting, especially if you provide each other with added value beyond the financial benefits (high quality product, access to specific market and clients, special know-how, etc.). These kind of partners might be willing to give you favorable payment conditions (as described in the paragraph above), special prices or discounts in exchange for long-term agreements, or equity stake. Also, it might be better to have as a shareholder somebody who is not motivated only by getting yield on his investment.

When it comes to external investors, the easier it is to get financing, the stricter the conditions will be and the higher the price. If you’re in a more advanced scaling stage, your options are now much wider. There are still Venture Capital funds and investors searching for round A/B/C startup equity investments, but their time horizon is usually a couple years and they’re searching primarily for large capital gains over this period. VCs, instead, are willing to accept a lot of risks. Compared to VCs, private equities are focused on somewhat less risky investments into usually smaller or non-traded companies. In the current era, they are also very often willing to fund startups in advanced phases (showing revenue growth, maybe still in loss, but under control). The upside of private equities is their time horizon is longer and they tend to develop their investments long-term and more actively.

Finally, compared to the above, banks and other financial institutions are increasingly searching for startup investments. These large institutional investors have in many cases dedicated VC and PE units and funds or they are even partnering with startup accelerators, innovation labs, and similar entities. It might be hard to obtain financing from them as they are more risk-averse, and due diligence and negotiations processes might be long and demanding, but their yield expectations are usually lower than those of others.

Funding can have different forms. Apart from basic equity funding, other forms of capital are also common in the scaling phase. If your startup is already out of the losses and shows decent financial conditions, you might be able to get to an ordinary loan. There are also a wide variety of investments between loan and equity which in fact very often match the needs of both the startup and the investor.

The first is a convertible loan, which is a loan that can be under some circumstances converted (also partially) into equity stake in the company. The circumstances might be, for example, fulfillment of the business plan or reaching a target valuation. The point of this is that the investor has an upside potential if the startup is doing well, but the downside risks are limited if the company isn’t reaching its goals, and the value of its equity would thus decrease. Startups, on the other hand, will get much a better interest rate on the loan or less strict conditions.

The second example of a similar investment is a loan with an embedded option. The investor in this case provides the standard loan, but has a right to acquire some pre-defined equity share of the company in the future for a pre-set price. If the startup succeeds, market value of this equity share will go above this initially pre-set price and the investor is able to get this upside. The startup will also get a better interest rate or less strict loan conditions.

### How to evaluate your company

Two main kinds of business valuation are used for companies in later phases. The first derive the company value from its expected, future ability to generate profits and for its owner (models of discounted cash flows, free cash flows, dividends, etc.). It is important that these models are based on your business plan and current financial performance. So, especially if you are still in losses, these models cannot be used or the valuation might be understated, although your potential is high. It might also be difficult to align on the business plan with your investor and agree on the valuation.

The second valuation approach is based on comparing the company with the market, its peers, and past market transactions. It usually derives company value from the set of multipliers obtained from researching recent transactions (such as past financing and valuation of similar startups). Multipliers such as average market price per unit of sales or price per client are used to determine the value of your startup. This valuation can also be used for companies still in the red, and also quite often reflects the market conditions. It should be noted that this valuation usually requires some specific adjustments for the risks of the company.

### How big a share should you give up?

There are two ways to answer this question.

First: as little as possible. Valuation is the price an investor is paying for your company, so you should always try to negotiate the highest possible valuation and give away for one dollar the least possible amount of equity.

Second: find a balance and an optimal financial structure. Selling equity is the most expensive way to finance a company while debt is in fact a cheaper way to finance it. On the other hand, you have to balance the risks for you and your startup, the types and expectations of your investors, and their willingness to accept risks. Due to these factors, the optimal financial structure of every company and startup is very specific.
