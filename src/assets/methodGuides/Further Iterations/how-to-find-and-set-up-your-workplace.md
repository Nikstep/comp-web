---
type: Guide
category: Further iterations
index: 12
title: How to Find and Set Up Your Workplace
description: '<p>So you&#39ve grown, and your business is doing well, but you don’t have an office? Perhaps you’re starting to feel the need for more space? This is the good kind of a problem you want to have. So, what should you do?</p><p>The environment in which you work has an immense influence on how you feel and perform. It is not just the psychology of colors, but the design you choose, whether you work in an open space or a closed office, etc. It shapes your everyday thinking and mood to a large extent. Picking correctly will pay off many times in your increased productivity. Here are a few tips on how to go about creating an office space that helps you achieve what matters most—the success of your startup.</p>'
---

### Individual offices, coworking spaces, and open offices

How will a coworking space influence the culture of your company and your business? There are various options when picking a space such as shared, coworking, subleased, or direct. Consider whether you are looking for a space with other like-minded startups or if you're already in a stage where your employees would benefit more from their own environment. While in the earlier stages, try out more shared office spaces and temporary arrangements because they have greater flexibility than full leases.

As a rule of thumb, solo founders and small (less than eight) teams should choose a coworking space to have the mental benefit of being surrounded by other hard-working people from other companies, which will outweigh the potential discomfort of being around unfamiliar people. Coworking spaces also provide basic services such as meeting rooms, printers, coffee, desks, and chairs. Those are investments you don’t need to make by yourself at the beginning of your company.

Moving to an individual office space will give you more privacy, but getting good real estate is hard, and suddenly all of the services you have taken for granted have to be provided by you, which further increases your costs. That being said, coworking spaces do not scale well to larger teams in terms of per-person cost. That’s why it makes sense to move your team out of a coworking space once it is larger than say ten people (given the economics of your area and pricing of the space). At this point the concern is how open your want your office to be.

Open workspaces have become rather standard in the corporate sphere; their advantages for the employer are obvious: pack as many people as possible into the smallest area possible to reduce cost. But are open workspaces also good for employees? Not always. It highly depends on the type of the team and the work people do.

In an open workspace, it is harder to concentrate. With so many people, even if they are respectful, it is hard to keep noise level low. A good thing is that everybody is within short reach and can communicate quickly. On the other hand, this may make it common practice to approach people whenever you have a question, which could disrupt their workflow.

A possible solution for both problems (noise and communication) is to have open space and smaller offices available together. People who need to communicate a lot can use the open space and those who need to concentrate on standalone discrete tasks can use the quiet office rooms.

### Consider your culture

One of the most noticeable ways to reinforce the values and vision of your company is through the physical space and design. Aesthetics, not merely functionality, is important here. Communicating company culture through physical surroundings can be done quite simply and cheaply with well-chosen decorations to give a lighthearted, fun feeling for employees. Something simple like naming conference rooms can achieve this effect as well. Existing employees will feel comfortable and new or prospective employees will be attracted to the space.

Your office should have a modern feel. A comfortable desk and chair is without a doubt a must. But there are other things that can take your workspace from boring to cool.

People are playful creatures— so equip the office with some gadgets that they can play with, because nobody can focus on coding for eight hours straight without a break. For example, a 3D printer might be highly appreciated by some creative developers, while others might prefer to spend some time playing FIFA on Playstation or Xbox. Others will want to rest for a while on a comfortable sofa or fat boy bag after working on a hard task or sitting on a chair for a long time. Also, a little in-office fridge packed with drinks like Arizona, Monster, etc. will make the office a better place. These type of furnishings and amenities will contribute to a modern, casual, Silicon-Valley style office culture.

### Do the math

This is part of your planning too, not entirely different from any other concern about the initial stages of your startup, so be sure not to underestimate how much time it will take to find the right space for you, since you will be at the mercy of fluctuations in the property market. Think about your future growth and how much space you’ll need when considering terms of leases, etc. The faster you grow, the more this can become an issue. You should time things so about two-thirds to three-quarters of the way through your lease you are reaching the occupancy limit. You don’t want to find yourself trapped in a long-term, maybe five-year lease. It may benefit you to find a real estate agent who can alert you to the most flexible properties. Knowing your budget helps narrow the search. Just remember to always keep your company culture in mind.
