---
type: Guide
category: Further iterations
index: 13
title: Investor Relations
description: You have investors on your side, which is good. These are probably the most important relationships in your career. Here is some advice about how not to ruin things and keep long-term business relationships.
---

### Why it’s important to communicate

You’ve won over the investors and you now have the money to chase your dream. Congrats! Now comes the hard part. The investor bought it because of a roadmap of growth you both believed in. While you’re involved in your startup every day, it’s important for the other party to understand what’s happening in the company and if everything is on track.
The rule of thumb is: the more you communicate your progress and results, the fewer questions and disruptions will come from the investor side in you regular business.

### Roles

Unless specified otherwise in contracts, you are the CEO of the company, so you make executive decisions including the money from investors. You need to abide by the rules of the engagement, but you should be in the driver’s seat.
The investor sees your company as a monetary vehicle and you and the team as a way to capitalize on the investment. They should not be directly interfering with your running of the company, unless there are some unforeseen circumstances that warrant the interference.

### Reporting

Agree on a standard for reporting and stick to it. Some investors will send you a template of their own that helps them look at all their portfolio companies in the same way. Include all the metrics, their past values, projections, and how you’re meeting the target.
