---
type: Guide
category: Further iterations
index: 10
title: Setting Up a Company Culture - Four Basics
description: Establishing a company culture is a complex and long-term undertaking with many different factors. We boiled it down to the four most basic things you should do or think about when creating one.
---

### Reflect

Who are you are as a team? What is your startup’s mission? What is the vision? (see earlier article on creating a Vision Statement). Why did you start doing what you’re doing in the first place? What is the ultimate goal?

Once you realize what your company’s vision, you need to make sure everyone in your company shares that vision and understands how their contributions fit into the whole picture—then you can start to be successful.

You may want to consider whether you have a common story. Of course your company has its own history, but forming an easy-to-understand narrative that can be simply explained, especially to new employees, really helps focus your company on its future.

### Create a set of core values

The core values are almost like the rules or even laws a company operates by. A society can’t function without laws and neither can a company. When establishing your core values, make sure they are ones which can stand the test of time as your company’s philosophy.

Establishing a set of core values will help you in at least in three areas:

- In the decision-making process

- In educating clients and potential customers about what the company is about and clarifying the identity of the company

- In recruiting and retention

### Communicate and live it

While the founder and senior members might have an idealized picture of the company culture, it won’t automatically instill itself into the minds of employees. It needs to be actively reinforced through communication, and be present in multiple dimensions, including the very aesthetics of the office space.

While the company culture should be something that everyone more or less feels part of and comfortable with, there will be more senior members who are kinds of “brand ambassadors” for your philosophy.

Another view claims that 80% of your company is determined by the founder. Whatever the founder’s proclivities are, the company will tend to that direction, e.g. if the founder is aggressive, data-driven, design-oriented, etc.

### Reassess

Once you’ve gone through each of these steps, don't think your job is done. It’s necessary to continually return to these points and reassess, especially when you’re growing and more people start to join the company. After all, the people you choose and the choices you make all contribute to the company culture. Make sure people understand not only the culture, but why your company exists in the first place. It all goes back to the first stages of conceiving of your company. If you can answer these fundamental questions, at any time, any day, then you’re still on the right track.
