---
type: Research
title: Insurtech 01–2020
description: Is insurance boring or not? Digital trends that are waking up the sleeping giant
coverImage: ./media/cover1.jpg
date: '2020-04-07'
---

## Insurance Industry Today

So how boring is insurance? Well, let’s kick this off with a few figures. According to Allianz[^1], just in 2018, global insurance premiums reached EUR 3655 billion or 5.4% of global output. Whichever way you cut it, that’s a lot of money. North America, Western Europe and Asia (excl Japan) together account for 85% of global premiums.

So let’s break down the figures a little more. North America accounted for EUR 1002 billion with the runner up being Western Europe with 1201 billion, and Asia (excl Japan) snatching the bronze medal with 817 billion (of which, interestingly, China accounted for 48%).

Not so boring now when you start looking at the money involved, huh? So the question is… How do you wake up the sleeping giant that for years has been set in its ways? Well, let’s look at the challenges the industry faces first…. and these are not small.

- It’s complicated—products are hard to understand and legacy systems stifle innovation.
- It’s conservative—focused on the long-term with customer experience a secondary concern.
- It’s conflicted—companies recognize the need to change but are not able to do so.

The last point in particular is a killer. Inability to change (legacy systems, high cost base, and traditional culture) means insurers losing out to more flexible rivals. One-shot digital transformation won’t cut it - regulations are updated, technology advances and society changes.

### So, what is prodding the sleeping giant into action?

Insurtech - basically using technology innovations to squeeze out savings from the current insurance industry model. To give you an idea of the investment into insurtech, Business Insider[^2] stated that in 2019 insurance investment hit USD 6.37 billion globally with Wefox, Lemonade, Hippo, Next Insurance and Bright Health in particular fuelling insurtech funding.

One of the main areas that insurtech has focused on is digital strategies for traditional insurers - not just to cut costs and improve efficiency but also for increased customer satisfaction, with 61% of customers preferring to check their applications online according to Thomas Brady from Board of innovation.

Transferring[^3] from paper based systems to online is not easy. One of the biggest challenges is developing the technology infrastructure they need and dealing with legacy software and huge IT systems. Digitization, whereby digital processes are tacked onto existing ways of working, is probably not going to be enough. Digitalization, involving the complete transformation of business models, is also going to be required.

So, that is one huge obstacle that the insurance industry faces, but they also have another monumental challenge - how to reach their younger customer base.

This is not easy peasy lemon squeezy. There is no magic wand that is gonna transform the pumpkin into a gleaming carriage.

The younger generation don’t really want insurance and in many cases do not understand why they should have it. In addition, many insurance companies do not own their customers - their network of insurance agents do. Insurance companies face the problem of getting information about their customers combined with the fact that their systems don’t support the process.

In terms of innovating in the insurance space there are broadly two approaches:

- Bundling with other services so that it does not seem like insurance
- Pay-as-you go models with easy onboarding and flexible plans

To wake up the sleeping giant, we need to take it to the ‘’Insurance Digital Transformation Fun Fair’’ and its various attractions. The fun fair contains innovative digital startups that are tackling different aspects of the insurance market.

On our tour, we are going to look at innovations for freelancers, how to die happy, life insurance and wills, how to manage your insurance, rewards for staying healthy, virtual health and how corporates are changing the market.

#### [Sherpa – life insurance for freelancers](https://meetsherpa.com/)

![Sherpa – life insurance for freelancers](./media/sherpa.jpg)

This platform targets entrepreneurs, contractors, freelancers and self-employed with its zero commission, flat membership fee. Onboarding is done through their intuitive communication tool ‘’Brain’’ and consists of 13 questions for a personalized solution.

#### [Dead Happy – life insurance](https://deadhappy.com/)

![Dead Happy – life insurance](./media/deadhappy.jpg)

A novel platform targeting the younger population aimed at making insurance faster, easier and fairer. There are 4 easy steps to get approved, a 10 year lifeline guarantee, and a focus on customers’ health now. It’s ‘’Make a dead wish’’ function clearly and simply tells you what is covered when you pass away.

#### [Fabric – life insurance and wills for families](https://meetfabric.com/)

![Fabric – life insurance and wills for families](./media/fabric.jpg)

The platform is designed for busy parents with 3 easy steps to apply for life insurance, and personalized plans. You can also create your will for free, appoint a guardian and beneficiary for your kids and access to legal help.

#### [Ladder – life insurance](https://www.ladderlife.com/)

![Ladder – life insurance](./media/ladder.jpg)

Ladders offers customers the opportunity of real time underwriting online, the opportunity to change coverage or cancel anytime, simple design with intuitive steps, and an insurance calculator. As an additional bonus, they give you a referral reward of up to USD 1000 for sharing with friends.

#### [Clark – insurance manager](https://www.clark.de/)

![Clark – insurance manager](./media/clark.jpg)

A mobile app that allows you to manage all your insurance contracts in one place. It evaluates existing contracts and helpful tips on how to save money on insurance as well as giving your better offers from across the insurance market.

#### [Yu Life – life insurance rewarding employees for living well](https://yulife.com/)

![Yu Life – life insurance rewarding employees for living well](./media/yulife.jpg)

The Wellbeing app requires you to be invited by your employer and involves gamification to support interaction. You earn benefits for healthy living (walking, mindfulness sessions, etc) while also creating a social impact.

#### [The Care Voice – operating system for insurers](http://www.thecarevoice.com/)

![The Care Voice – operating system for insurers](./media/carevoice.jpg)

This is an SaaS solution aimed at digitizing health and insurance services. The app covers the whole medical journey - virtual nurse - appointment - online pharmacy - online payment - online content for staying healthy - activity tracking. There is also an AI-voice based virtual assistant to process claims. A digital partner for insurers.

#### [PZU Polska – corporate web portal for insurance](https://moje.pzu.pl/cas/login)

![PZU Polska – corporate web portal for insurance](./media/pzu.jpg)

This portal provides a good example of corporate innovation to increase online sales. It is designed for registered members of PZU, where all insurance services are listed through a clean and simple UX design and easy to fill in forms.

#### [Nippon Life – financial institution](https://www.nissay.co.jp/english/)

![Nippon Life – financial institution](./media/nippon.jpg)

Nippon Life is an established provider with an innovative approach to sales. They have developed the Taskall tablet for their sales teams based on AI and previous sales approaches. The tablet analyses successful sales made by Nippon life and identifies new customers from extensive prospect lists.

So, having taken the sleeping giant for a spin round the fun park, isn’t it time you decided which of the attractions you want to have a go on?

### Notes

[^1]: [https://www.eulerhermes.com/en_global/economic-research/insights/Global-insurance-markets-at-a-crossroads.html](https://www.eulerhermes.com/en_global/economic-research/insights/Global-insurance-markets-at-a-crossroads.html)
[^2]: [https://www.businessinsider.com/2019-insurtech-funding-reached-new-highs-2020-2](https://www.businessinsider.com/2019-insurtech-funding-reached-new-highs-2020-2)
[^3]: [https://www.boardofinnovation.com/blog/trends-shaping-the-future-of-the-insurance-industry/](https://www.boardofinnovation.com/blog/trends-shaping-the-future-of-the-insurance-industry/)
