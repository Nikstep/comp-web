---
type: Research
title: Fintech 01-2020
description: We are going to take a sampling of digital innovations in the Fintech market
coverImage: ./media/fintech_cover.jpg
date: '2020-04-08'
---

What is FinTech exactly? According to Ivestopedia[^1], Financial technology (FinTech) is used to describe new tech that seeks to improve and automate the delivery and use of financial services.

At its inception, FinTech was initially applied to the backend technology of established financial institutions. However, since then it has come on leaps and bounds with a shift to more consumer-orientated services. Fintech can now be seen extending its octopussy hands into education, retail banking, fundraising and nonprofit, and investment management to name but a few.

### What has driven the explosion in FinTech?

Pure and simple - the internet revolution and the mobile internet/smartphone revolution. The application of technology to backend systems has now meant its presence being heavily felt in personal and commercial finance.

So, let’s have a look at some of the ways FinTech is used in 2020…..[^2]

- Crowdfunding platforms - brought to you by FinTech
- Cryptocurrency - think bitcoin and litecoin
- Blockchain - BlockVerify helping to reduce fraud
- Mobile payment - Apple Pay is paving the way
- Robo advising and stock trading apps - Charles Schwab getting in on the act
- Budgeting apps - like Intuit - designed to keep you out of the red
- Fintech stocks - Paypal is your friend

and the list goes on….

### How big is the FinTech market?

Big. Really big. Just to give you a flavour, total investment activity in Fintech rose from USD 18.9 billion in 2013 to USD111.8 billion in 2018[^3]. Delving into the figures, we find some interesting trends:

- Large companies are ramping up their investment into FinTech firms. Financial services institutions need to improve their cost effectiveness, service portfolios and customer experience. Technology and innovation plays a key role in realising such advancements, and smaller, agiler companies are better able to blend the two into a proposition that adds value. In 2018, corporate venturing deals grew to 357, with total deal value at \$23.1 billion.
- M&A is becoming a key driver of growth, with the number of international deals reaching \$54 billion in 2018. Et voila. We now see the evolution of FinTech with financial services companies seeking to extend their geographical reach.

Let’s tie all this together, shall we?

We are going to take a sampling of digital innovations in the Fintech market including banking innovations, lending platforms, competitors to credit cards, trading platforms, investment apps and security apps.

#### [Peo Pay - digital banking from Bank Pekao](https://www.pekao.com.pl/private-banking/bankowosc-elektroniczna/peopay.html)

![Peo Pay - digital banking from Bank Pekao](./media/peopay.jpg)

The application is feature rich including opening an account remotely using a ‘’selfie’’, full banking functionality, a virtual credit card, getting a consumer loan in 30 seconds using Click Cash, a virtual credit card, and intelligent multi-currency exchange.

#### [Kabbage - direct funding for small businesses in conjunction with Softbank](https://www.kabbage.com)

![Kabbage - direct funding for small businesses in conjunction with Softbank](./media/kabbage.jpg)

This is a funding platform that enables small businesses to get loans up to USD 100,000. It is based on a number of data factors, including business volume, time in business, transaction volume, social media activity, and the seller’s credit score. The simple online application means you can get a decision in minutes.

#### [Affirm - monthly payments for eCommerce backed by investment funds](https://www.affirm.com)

![Affirm - monthly payments for eCommerce backed by investment funds](./media/affirm.jpg)

You can choose from thousands of sites and stores, select Affirm at checkout, and then enter a few pieces of information for a real‑time decision. You then select the payment schedule you like best and confirm your loan. It enables you to buy quickly without form filling or credit card authentication.

#### [Robinhood - no commission trading app funded by VCs, investment funds, private investors](https://robinhood.com/us/en/)

![Robinhood - no commission trading app funded by VCs, investment funds, private investors](./media/robinhood.jpg)

This smartphone app allows you to invest in US publicly traded companies, exchange-traded funds and crypto currencies in real time with no commission. You can also customize your portfolio to mitigate risk.

#### [Wealthfront - automated investment platform backed by investment funds](https://www.wealthfront.com)

![Wealthfront - automated investment platform backed by investment funds](./media/wealthfront.jpg)

Wealthfront allows you to be a passive investor on autopilot thanks to its ‘’roboadvisors’’, cutting out the need for traditional advisors to communicate advice about stocks and mutual funds. The software executes time-tested investment strategies, automatically looking for opportunities to improve your portfolio’s performance.

#### [Friends24 - mobile payment application with Ceska Sporitelna bank](https://www.csas.cz/en/mobile-apps/friends-24)

![Friends24 - mobile payment application with Ceska Sporitelna bank](./media/friends24.jpg)

This is a Czech startup which allows you to send money to anyone without knowing their bank account details. You can use any contact details for the recipient – email, telephone or any messenger contact on social networks for payments up to CZK 10,000.

### Notes

[^1]: [https://www.investopedia.com/terms/f/fintech.asp](https://www.investopedia.com/terms/f/fintech.asp)
[^2]: [https://www.thestreet.com/technology/what-is-fintech-14885154](https://www.thestreet.com/technology/what-is-fintech-14885154)
[^3]: [https://www.consultancy.eu/news/2390/global-fintech-investment-more-than-doubled-to-112-billion](https://www.consultancy.eu/news/2390/global-fintech-investment-more-than-doubled-to-112-billion)
