export const getGridColumn = (index: number, numberOfColumns: number) =>
  (index % numberOfColumns) + 1

export const getGridRow = (index: number, numberOfColumns: number) =>
  Math.ceil((index + 1) / numberOfColumns)
