import { CaseStudy } from '../types/CaseStudy'

export const randomPicker = (data: Array<CaseStudy>, limit: number) => {
  if (!data || data.length === 0) {
    return []
  }

  if (data.length <= 2) {
    return data
  }

  const array: Array<CaseStudy> = []

  for (let i = 0; i < limit; i++) {
    const index = Math.floor(Math.random() * data.length)

    array.push(data[index])

    data.splice(index, 1)
  }

  return array
}
