import { useTriggerTransition } from 'gatsby-plugin-transition-link'
import { PAGE_TRANSITION_DURATION } from '../constants'

export const usePageTransition = (link: string) => {
  const triggerTransition = useTriggerTransition({
    to: link,
    exit: { length: PAGE_TRANSITION_DURATION },
    entry: { delay: PAGE_TRANSITION_DURATION },
  })

  return triggerTransition
}
