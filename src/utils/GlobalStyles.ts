import { createGlobalStyle } from 'styled-components'
import { FONT_SIZE, FONT_WEIGHT, COLORS, SCREEN } from '../constants'

export const GlobalStyles = createGlobalStyle`
  * {
    margin: 0;
    padding: 0;
    border: 0;
    box-sizing: border-box;

    ::selection { 
      color: ${COLORS.WHITE};  
      background-color: ${COLORS.BLUE};
    }
  }

  html {
		font-size: 62.5%;
	}

  body {
    color: ${COLORS.BLACK};
    font-family: 'Theinhardt', sans-serif;
    font-size: ${FONT_SIZE.BASE};
    overflow-x: hidden;
  }

  a {
    color: inherit;
    text-decoration: none;
    cursor: pointer;
  }

  button {
    background-color: transparent;
    font-size: inherit;
    cursor: pointer;

    :focus {
      outline: none;
    }

    :disabled {
      cursor: default;
    }
  }

  h1 {
    color: ${COLORS.OFF_BLACK};
    font-size: 12vw;
    line-height: 1.1;

    ${SCREEN.ABOVE_MOBILE} {
      font-size: 8rem;
    }
  }

  h2 {
    color: ${COLORS.OFF_BLACK};
    font-size: 4rem;
    line-height: 1.1;

    ${SCREEN.ABOVE_MOBILE} {
      font-size: 8rem;
    }
  }

  h3 {
    font-size: 3.6rem;
    line-height: 5rem;
  }

  h4 {
    font-size: 2.4rem;
    line-height: 2.5rem;
  }
  
  h1, h2, h3, h4, strong {
    font-weight: ${FONT_WEIGHT.MEDIUM};
  }

  p {
    line-height: 1.7;
  }

  b {
    font-weight: ${FONT_WEIGHT.MEDIUM};
  }

  section {
    padding-top: 15rem;

    ${SCREEN.ABOVE_TABLET} {
      padding-top: 17rem;
    }
  }
`
