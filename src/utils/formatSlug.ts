export const formatSlug = (slug: string) => {
  const reg = /(\s|\W)/gm

  return slug.replace(reg, '-').toLowerCase()
}
