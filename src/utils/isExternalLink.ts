export const isExternalLink = (link: string) =>
  link.includes('www') || link.includes('http')
