export const scrollToElement = (
  element: HTMLElement | null | undefined,
  offset: number | undefined = 0
) => {
  if (element) {
    const scrollDistance =
      element.getBoundingClientRect().top + window.pageYOffset - offset

    window.scrollTo({
      top: scrollDistance,
      left: 0,
      behavior: 'smooth',
    })
  }
}
