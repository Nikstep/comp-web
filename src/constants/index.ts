import { keyframes } from 'styled-components'

export const COLORS = {
  BLACK: '#030303',
  OFF_BLACK: '#202020',
  GREY: '#505050',
  LIGHT_GREY: '#696969',
  WHITE: '#fff',
  BLUE: '#0000FF',
  YELLOW: '#FFD600',
  LIGHT_YELLOW: 'rgba(255, 213, 25, 0.8)',
  RED: 'rgba(255, 37, 25, 0.8)',
  GREEN: 'rgba(13, 255, 104, 0.8)',
}

export const FONT_SIZE = {
  EXTRA_SMALL: '1.4rem',
  SMALL: '1.5rem',
  BASE: '1.6rem',
  MEDIUM: '1.8rem',
  LARGE: '2.4rem',
  EXTRA_LARGE: '3.6rem',
}

export const FONT_WEIGHT = {
  BASE: '400',
  MEDIUM: '500',
  BOLD: '700',
}

export const HEADER_HEIGHT = 8

export const OFFSET_POSITION_TOP = {
  MOBILE: 2,
  DESKTOP: 3,
}

export const OFFSET_POSITION_LEFT = {
  MOBILE: 2,
  DESKTOP: 3,
}

export const PADDING = {
  MOBILE: 2.5,
  DESKTOP: 6,
}

export const MAX_CONTENT_WIDTH = 180

export const BREAKPOINT = {
  SMALL_MOBILE: 360,
  MOBILE: 426,
  TABLET: 851,
  LAPTOP: 1125,
  MAX_CONTENT_WIDTH: MAX_CONTENT_WIDTH * 10,
}

export const SCREEN = {
  ABOVE_MOBILE: `@media (min-width: ${BREAKPOINT.MOBILE}px)`,
  ABOVE_TABLET: `@media (min-width: ${BREAKPOINT.TABLET}px)`,
  ABOVE_LAPTOP: `@media (min-width: ${BREAKPOINT.LAPTOP}px)`,
  ABOVE_MAX_CONTENT_WIDTH: `@media (min-width: ${BREAKPOINT.MAX_CONTENT_WIDTH}px)`,
}

export const Z_INDEX = {
  BACKGROUND: -1,
  ABOVE: 2,
  HEADER: 1,
}

export const LINK_UNDERLINE = {
  DEFAULT: `::after {
    content: '';
    position: absolute;
    bottom: 0;
    left: 0;
    width: 100%;
    height: 1rem;
    border-bottom: 2px solid blue;
    opacity: 1;
    transform: translateY(0);
    transition: opacity 0.2s ease-in, transform 0.2s ease-in;
  }`,
  HOVER: `::after {
    transform: translateY(0.3rem);
  }
`,
}

const IE_QUERY =
  'screen and (-ms-high-contrast: active), (-ms-high-contrast: none)'

export const IE = {
  DEFAULT: `@media ${IE_QUERY}`,
  ABOVE_MOBILE: `@media (min-width: ${BREAKPOINT.MOBILE}px) and ${IE_QUERY}`,
  ABOVE_TABLET: `@media (min-width: ${BREAKPOINT.TABLET}px) and ${IE_QUERY}`,
  ABOVE_LAPTOP: `@media (min-width: ${BREAKPOINT.LAPTOP}px) and ${IE_QUERY}`,
  ABOVE_MAX_CONTENT_WIDTH: `@media (min-width: ${BREAKPOINT.MAX_CONTENT_WIDTH}px) and ${IE_QUERY}`,
}

export const MEDIA_IE_HOVER = `${IE.DEFAULT}, (hover: hover)`

export const PAGE_TRANSITION_DURATION = 0.25
export const TRANSLATE_DISTANCE = '20px'

export const ANIMATIONS = {
  FADE_IN: keyframes`
    0% {
      opacity: 0;
    }
    100% {
      opacity: 1;
    }
  `,

  FADE_UP: (distance: string) => keyframes`
    0% {
      opacity: 0;
      transform: translateY(${distance});
    }

    100% {
      opacity: 1;
      transform: translateY(0);
    }
  `,
}

export const API_URL = 'https://strapi.u.plus'
