import { COLORS } from './index'
export type TechLogo = {
  name: string
  title: string
}
export interface Stage {
  stage?: number
  header: string
  body: string[] | string
  offsetColor?: string
  isAside?: boolean
  positioning?: 'left' | 'right'
  icon?: string
  arrow?: 'left' | 'right'
  mobileArrow?: boolean
  techIcons?: TechLogo[]
}

export const stages: Array<Stage> = [
  {
    header: 'Value Proposition',
    body:
      'Initial idea validation and market testing where we look at the applicability of a specific idea to the particular market and target demographics.',
    isAside: true,
    positioning: 'right',
    icon: 'pyramid1',
  },
  {
    stage: 1,
    header: 'Validation',
    body: ['Target users', 'Product positioning', 'High level user stories'],
    offsetColor: COLORS.BLUE,
    arrow: 'right',
  },
  {
    header: 'Product Definition',
    body: [
      'When we find a market-verified value proposition, we move to define the product and ways of taking it to the market.',
      'This usually means building prototypes, smoke tests and understanding launch channels and features.',
    ],
    isAside: true,
    positioning: 'left',
    icon: 'pyramid2',
  },
  {
    stage: 2,
    header: 'Product Specification',
    body: ['Product definition', 'MVP Specification', 'MVP Scoping'],
    offsetColor: COLORS.LIGHT_YELLOW,
    arrow: 'left',
    mobileArrow: true,
  },
  {
    stage: 3,
    header: 'Take to Market Strategy',
    body: [
      'Channel testing',
      'Pricing sensitivity',
      'Brand and Communications',
    ],
    offsetColor: COLORS.LIGHT_YELLOW,
    arrow: 'right',
  },
  {
    header: 'MVP Launch',
    body: [
      'Developers and designers engage in building out an MVP while we  move to launch with early customers.',
    ],
    isAside: true,
    positioning: 'right',
    icon: 'pyramid3',
    techIcons: [
      {
        name: 'aws',
        title: 'Cloud',
      },
      {
        name: 'react',
        title: 'React.js',
      },
      {
        name: 'nodejs',
        title: 'Node.js',
      },
    ],
  },
  {
    stage: 4,
    header: 'Product Build',
    body:
      'Developers and designers are engaged in building the MVP of the product',
    offsetColor: COLORS.RED,
    arrow: 'left',
    mobileArrow: true,
  },
  {
    stage: 5,
    header: 'Launch',
    body: [
      'Gathering early customer feedback',
      'Build out the post launch roadmap',
      'Optimizing the customer onboarding process ',
    ],
    offsetColor: COLORS.RED,
    arrow: 'right',
  },
  {
    header: 'Further Iterations',
    body: [
      'When the MVP launch goes well, we move onto scaling up the product based on market feedback',
      'This stage typically includes supporting internal operational processes and product modifications based on early customer feedback.',
    ],
    isAside: true,
    positioning: 'left',
    icon: 'pyramid4',
  },
  {
    stage: 6,
    header: 'Scaling',
    body: [
      'Build out internal team',
      'Enhance operations processes',
      'Expand core functionalities',
    ],
    offsetColor: COLORS.GREEN,
  },
]
