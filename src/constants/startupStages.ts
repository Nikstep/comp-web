interface MethodGuideStage {
  name: string
  description: string
}

export const METHOD_STAGES: Array<MethodGuideStage> = [
  {
    name: 'Value proposition',
    description:
      'Initial idea validation and market testing where we look at the applicability of a specific idea to the particular market and target demographics.',
  },
  {
    name: 'Product definition',
    description:
      'When we find a market-verified value proposition, we move to define the product and ways of taking it to the market. This usually means building prototypes, smoke tests and understanding launch channels and features.',
  },
  {
    name: 'MVP Launch',
    description:
      'Developers and designers are engaged in building out an MVP while we simultaneously move to launch with early customers.',
  },
  {
    name: 'Further iterations',
    description:
      'When the MVP launch goes well, we move onto scaling up the product based on market feedback. This stage typically includes supporting internal operational processes and product modifications based on early customer feedback.',
  },
]
