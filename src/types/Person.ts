export interface Person {
  id: string
  name: string
  position: string
  photo: {
    publicURL: string
  }
  linkedin: string
  shortDescription?: string
  priority: number
}

export interface People {
  allStrapiPerson: {
    nodes: Array<Person>
  }
}
