export interface Tag {
  text: string
  id: number
}
