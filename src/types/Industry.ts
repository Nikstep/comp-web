export interface Industry {
  strapiId: number
  title: string
}

export interface Industries {
  allStrapiIndustry: {
    nodes: Industry[]
  }
}

export interface IndustryCount {
  totalCount: number
  fieldValue: number
}

export interface IndustryCounts {
  allRestApiCaseStudies: {
    group: IndustryCount[]
  }
}
