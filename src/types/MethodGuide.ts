export interface MethodGuide {
  html: string
  frontmatter: {
    index: number
    title: string
    category: string
    description: string
  }
}
