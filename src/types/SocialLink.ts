export interface SocialLink {
  id: string
  url: string
  urlLabel: string
}
