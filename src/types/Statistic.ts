export interface Statistic {
  number: string
  text: string[]
}
