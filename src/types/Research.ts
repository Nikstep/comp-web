export interface Research {
  id: string
  frontmatter: {
    title: string
    description: string
    date: string
    coverImage?: {
      childImageSharp: {
        fixed: {
          src: string
        }
      }
    }
  }
  link: string
}

export interface Researches {
  allMarkdownRemark: {
    nodes: Array<Research>
  }
}
