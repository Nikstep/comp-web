export interface Author {
  name: string
  created_at: string
  id: number
}
