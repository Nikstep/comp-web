import { BodyElement } from './BodyElement'

export interface Item {
  list_item: string
  id: number
}

export interface CaseStudy {
  id: number
  name: string
  client: string
  shortDescription: string
  description: string
  cover?: { publicURL?: string }
  coverPublicUrl?: string
  logo?: { publicURL?: string }
  logoPublicUrl?: string
  cardImage: { publicURL?: string }
  cardImagePublicUrl: string
  outcomes: Array<Item>
  caseStudyBody: Array<BodyElement>
  industry: { id: number; title: string }
  isFeatured: boolean
  approach: string
  mission: string
  strapiId: string
}

export interface CaseStudies {
  allRestApiCaseStudies: {
    nodes: CaseStudy[]
  }
  allStrapiCaseStudy: {
    nodes: CaseStudy[]
  }
}
