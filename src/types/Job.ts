export interface Job {
  Position: string
  Location: 'PRG' | 'NYC' | 'SF'
  Description: string
  link: string
}

export interface Jobs {
  allStrapiJob: {
    nodes: Array<Job>
  }
}
