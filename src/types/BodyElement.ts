export interface BodyElement {
  _xcomponent: string
  author?: string
  text?: string
  id: number
  textAlignment?: string
  alignment?: string
  caption?: string
  media?: {
    url: string
  }
  isBanner?: boolean
}
