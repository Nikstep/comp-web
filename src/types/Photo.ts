export interface Photo {
  publicURL: string
  id: string
}
