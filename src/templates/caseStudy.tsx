import React, { useRef, useMemo } from 'react'
import styled from 'styled-components'
import { CaseStudyCard } from '../components/work/CaseStudyCard'
import { CardsContainer } from '../components/work/CaseStudySection'
import { CaseStudyHighlights } from '../components/caseStudy/CaseStudyHighlights'
import {
  CaseStudyMetaDataCol,
  MetaDataTitle,
} from '../components/caseStudy/CaseStudyMetaDataCol'
import { Body } from '../components/caseStudy/ContentBody'
import { Layout } from '../components/Layout'
import SEO from '../components/Seo'
import { FONT_SIZE, SCREEN, COLORS } from '../constants'
import { CaseStudy } from '../types/CaseStudy'
import { randomPicker } from '../utils/randomPicker'
import { useIsInView } from '../utils/useIsInView'
import { Heading } from '../components/Heading'
import { AnimatedPageProps } from '../pages'
import { FloatingText } from '../components/FloatingText'
import { scrollToElement } from '../utils/scrollToElement'
import { AnimatedLink } from '../components/AnimatedLink'
import { FullScreenCover } from '../components/FullScreenCover'

const Article = styled.article`
  padding-top: 15rem;

  ${SCREEN.ABOVE_TABLET} {
    padding-top: 17rem;
  }
`

const StyledHeading = styled(Heading)`
  max-width: 100rem;
  margin-bottom: 3rem;

  ${SCREEN.ABOVE_MOBILE} {
    margin-bottom: 5rem;
  }
`

const CaseMeta = styled.div`
  display: grid;
  grid-template-columns: 1fr 0.7fr;
  grid-gap: 1.5rem 1rem;
  margin-bottom: 2rem;

  ${SCREEN.ABOVE_MOBILE} {
    display: flex;
    align-items: flex-start;
  }

  ${SCREEN.ABOVE_TABLET} {
    flex-direction: row;
    align-items: center;
    max-height: 5rem;
  }
`

const CaseLogo = styled.div<{ url?: string }>`
  width: 14rem;
  height: 5rem;
  background: ${({ url }) => (url ? `url(${url})` : COLORS.BLUE)};
  background-repeat: no-repeat;
  background-size: contain;
  background-position-y: center;

  ${SCREEN.ABOVE_MOBILE} {
    margin-right: 8rem;
  }
`

const DescriptionTitle = styled(MetaDataTitle)`
  margin: 5rem auto 1.5rem;
  max-width: 50rem;
  color: ${COLORS.LIGHT_GREY};
  text-align: center;

  ${SCREEN.ABOVE_MOBILE} {
    margin-top: 8rem;
  }
`

const Description = styled.p`
  margin: 0 auto 5rem;
  max-width: 50rem;
  font-size: ${FONT_SIZE.LARGE};
  line-height: 3.2rem;
  text-align: center;

  ${SCREEN.ABOVE_MOBILE} {
    margin-bottom: 8rem;
  }
`

interface CaseStudyContext extends CaseStudy {
  caseStudies: Array<CaseStudy>
  endpointId: string
}

const CaseStudyPage: React.FC<AnimatedPageProps<unknown, CaseStudyContext>> = ({
  pageContext: {
    approach,
    caseStudyBody,
    caseStudies,
    client,
    description,
    endpointId,
    industry,
    mission,
    name,
    shortDescription,
    outcomes,
    coverPublicUrl,
    logoPublicUrl,
  },
}) => {
  const containerRef = useRef<HTMLDivElement>(null)
  const articleRef = useRef<HTMLDivElement>(null)
  const isInView = useIsInView(containerRef, 200)

  const filteredCS = caseStudies.filter(
    (caseStudy: CaseStudy) =>
      caseStudy.isFeatured && caseStudy.strapiId !== endpointId
  )

  const caseStudiesToShow = useMemo(() => randomPicker(filteredCS, 2), [])

  return (
    <Layout>
      <SEO
        title={`${name} - ${industry.title} | Our Work`}
        description={description}
      />

      <Article>
        <StyledHeading
          subheading={<AnimatedLink to="/work">Our work</AnimatedLink>}
          isMain
        >
          {shortDescription}
        </StyledHeading>

        <CaseMeta>
          {logoPublicUrl && <CaseLogo url={logoPublicUrl} />}
          {industry && (
            <CaseStudyMetaDataCol title="Industry">
              {industry.title}
            </CaseStudyMetaDataCol>
          )}
          {name && (
            <CaseStudyMetaDataCol title="Product name">
              {name}
            </CaseStudyMetaDataCol>
          )}
          {client && (
            <CaseStudyMetaDataCol title="Client">{client}</CaseStudyMetaDataCol>
          )}
        </CaseMeta>

        <FloatingText onClick={() => scrollToElement(articleRef.current)}>
          Scroll down
        </FloatingText>

        {coverPublicUrl && <FullScreenCover url={coverPublicUrl} />}

        <DescriptionTitle ref={articleRef}>A quick look</DescriptionTitle>
        <Description>{description}</Description>

        {caseStudyBody.length > 0 && <Body nodes={caseStudyBody} />}

        <CaseStudyHighlights
          approach={approach}
          mission={mission}
          outcomes={outcomes}
        />
      </Article>

      {caseStudiesToShow.length > 0 && (
        <section>
          <StyledHeading textAlign="center">More of our work</StyledHeading>
          <CardsContainer isInView={isInView} ref={containerRef}>
            {caseStudiesToShow.map(
              (
                { name, cardImage, logo, shortDescription, description, id },
                index
              ) => (
                <CaseStudyCard
                  key={id}
                  index={index}
                  name={name}
                  coverPublicUrl={cardImage?.publicURL}
                  logoPublicUrl={logo?.publicURL}
                  shortDescription={shortDescription}
                  description={description}
                  isCaseDetailPage
                />
              )
            )}
          </CardsContainer>
        </section>
      )}
    </Layout>
  )
}

export default CaseStudyPage
