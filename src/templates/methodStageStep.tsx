import React from 'react'
import styled from 'styled-components'
import SEO from '../components/Seo'
import { Layout } from '../components/Layout'
import { SCREEN } from '../constants'
import { MethodGuide } from '../types/MethodGuide'
import { MethodArticle } from '../components/methodStage/MethodArticle'
import { Heading } from '../components/Heading'
import { AnimatedPageProps } from '../pages'
import { AnimatedLink } from '../components/AnimatedLink'
import { SectionDescription } from '../components/SectionDescription'
import { FloatingText } from '../components/FloatingText'
import { formatSlug } from '../utils/formatSlug'

const ArticlesContainer = styled.div`
  ${SCREEN.ABOVE_LAPTOP} {
    width: 83rem;
    margin: 0 auto;
  }
`

type MethodStageStepContext = MethodGuide

const MethodStageStepPage: React.FC<AnimatedPageProps<
  unknown,
  MethodStageStepContext
>> = ({ pageContext: { frontmatter, html } }) => (
  <Layout>
    <SEO
      title={`${frontmatter.title} | ${frontmatter.category} | U+Method`}
      description={frontmatter.description}
    />

    <section>
      <Heading
        isMain
        subheading={
          <AnimatedLink to={`/method/${formatSlug(frontmatter.category)}`}>
            {frontmatter.category}
          </AnimatedLink>
        }
      >
        {frontmatter.title}
      </Heading>

      <SectionDescription maxWidth={60} isMain>
        {frontmatter.description}
      </SectionDescription>

      <FloatingText>Read on</FloatingText>

      <ArticlesContainer>
        <MethodArticle html={html} frontmatter={frontmatter} isTitleHidden />
      </ArticlesContainer>
    </section>
  </Layout>
)

export default MethodStageStepPage
