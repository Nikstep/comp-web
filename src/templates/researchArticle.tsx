import React, { useRef, useState, useEffect, useMemo } from 'react'
import styled from 'styled-components'
import SEO from '../components/Seo'
import { Layout } from '../components/Layout'
import { COLORS, FONT_SIZE, BREAKPOINT } from '../constants'
import { throttle } from 'throttle-debounce'
import { Heading } from '../components/Heading'
import { AnimatedPageProps } from '../pages'
import { AnimatedLink } from '../components/AnimatedLink'
import { SectionDescription } from '../components/SectionDescription'
import { FloatingText } from '../components/FloatingText'
import { FixedObject } from 'gatsby-image'
import { FullScreenCover } from '../components/FullScreenCover'
import { ArticlesContainer } from '../components/ArticlesContainer'
import { ResearchTableOfContents } from '../components/researchArticle/ResearchTableOfContents'
import { Article } from '../components/Article'

const StyledSectionDescription = styled(SectionDescription)`
  margin-bottom: 2rem;
`

const StyledArticle = styled(Article)`
  figcaption {
    margin-top: 1rem;
    color: ${COLORS.GREY};
    font-size: ${FONT_SIZE.MEDIUM};
    text-align: center;
  }

  sup a {
    text-decoration: none;
    color: blue;
  }

  .footnotes {
    p,
    ol {
      font-size: ${FONT_SIZE.MEDIUM};
    }
  }
`

interface ResearchArticleContext {
  frontmatter: {
    title: string
    description: string
    coverImage: {
      childImageSharp: {
        fixed: FixedObject
      }
    }
  }
  html: string
}

const ResearchArticlePage: React.FC<AnimatedPageProps<
  unknown,
  ResearchArticleContext
>> = ({
  pageContext: {
    frontmatter: { title, description, coverImage },
    html,
  },
}) => {
  const [currentTitle, setCurrentTitle] = useState<string>()

  const containerRef = useRef<HTMLElement>(null)

  const getTitleElements = () => {
    const elements = []

    if (containerRef.current) {
      for (const el of containerRef.current.children) {
        if (
          el.nodeName === 'H2' ||
          el.nodeName === 'H3' ||
          el.nodeName === 'H4'
        ) {
          elements.push(el as HTMLElement)
        }
      }
    }

    return elements
  }

  const titleElements = useMemo(getTitleElements, [containerRef.current])

  const handleScrollSpy = throttle(400, () => {
    if (titleElements) {
      let bestMatch: { titleText: string; delta: number } | undefined

      for (const titleEl of titleElements) {
        const titleText = titleEl.innerText

        const delta = Math.abs(window.pageYOffset - titleEl.offsetTop)

        if (!bestMatch) {
          bestMatch = { titleText, delta }
        } else if (delta < bestMatch?.delta) {
          bestMatch = { titleText, delta }
        }
      }

      setCurrentTitle(bestMatch?.titleText)
    }
  })

  useEffect(() => {
    if (window.innerWidth >= BREAKPOINT.LAPTOP) {
      window.addEventListener('scroll', handleScrollSpy)

      return () => window.removeEventListener('scroll', handleScrollSpy)
    }
  }, [titleElements])

  return (
    <Layout>
      <SEO title={`${title} | Research`} description={description} />

      <section>
        <Heading
          isMain
          subheading={<AnimatedLink to="/research">Research</AnimatedLink>}
        >
          {title}
        </Heading>
        <StyledSectionDescription maxWidth={60} isMain>
          {description}
        </StyledSectionDescription>
        <FullScreenCover url={coverImage.childImageSharp.fixed.src} />
        <FloatingText>Read on</FloatingText>

        <ArticlesContainer>
          <ResearchTableOfContents
            titleElements={titleElements}
            currentTitle={currentTitle}
            containerRef={containerRef}
          />

          <StyledArticle
            ref={containerRef}
            dangerouslySetInnerHTML={{
              __html: html,
            }}
          />
        </ArticlesContainer>
      </section>
    </Layout>
  )
}

export default ResearchArticlePage
