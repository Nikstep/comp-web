import React from 'react'
import styled from 'styled-components'
import { TransitionState } from 'gatsby-plugin-transition-link'
import { CaseStudy } from '../../types/CaseStudy'
import { formatSlug } from '../../utils/formatSlug'
import {
  COLORS,
  FONT_WEIGHT,
  SCREEN,
  ANIMATIONS,
  FONT_SIZE,
  LINK_UNDERLINE,
  MEDIA_IE_HOVER,
  Z_INDEX,
  OFFSET_POSITION_LEFT,
  OFFSET_POSITION_TOP,
  TRANSLATE_DISTANCE,
} from '../../constants'
import { AnimatedLink } from '../AnimatedLink'
import { usePageTransition } from '../../utils/usePageTransition'
import { useStaticQuery, graphql } from 'gatsby'

const Cover = styled.div<{ url?: string }>`
  position: relative;
  width: 100%;
  height: 40rem;
  margin-bottom: 2rem;
  background: ${({ url }) => `url(${url})`};
  background-repeat: no-repeat;
  background-position: center;
  background-size: cover;

  ::before {
    content: '';
    position: absolute;
    left: -${OFFSET_POSITION_LEFT.MOBILE - 0.4}rem;
    top: -${OFFSET_POSITION_TOP.MOBILE - 0.4}rem;
    display: block;
    border: 4px solid ${COLORS.OFF_BLACK};
    width: 100%;
    height: 100%;
    transition: transform 0.4s cubic-bezier(0.24, -0.3, 0.6, 1.52),
      border-color 0.4s ease-in;
    z-index: ${Z_INDEX.BACKGROUND};

    ${SCREEN.ABOVE_MOBILE} {
      left: -${OFFSET_POSITION_LEFT.DESKTOP - 0.4}rem;
      top: -${OFFSET_POSITION_LEFT.DESKTOP - 0.4}rem;
    }
  }

  ::after {
    content: '';
    position: absolute;
    width: 100%;
    height: 100%;
    transition: background 0.2s ease-in;
  }

  ${SCREEN.ABOVE_TABLET} {
    height: 44rem;
  }
`

const ShortDescription = styled.h3`
  line-height: 4rem;
  ${SCREEN.ABOVE_MOBILE} {
    line-height: 5rem;
  }
`

const SeeMore = styled.span`
  position: relative;
  padding: 0.5rem;
  margin-left: -0.5rem;
  font-weight: ${FONT_WEIGHT.MEDIUM};
  transition: border-bottom-width 0.2s ease-in, color 0.1s ease-in;

  ${LINK_UNDERLINE.DEFAULT};
`

const Container = styled.div<{
  index: number
  isCaseDetailPage?: boolean
  isReadyToRender: boolean
}>`
  display: ${({ isReadyToRender }) => !isReadyToRender && 'none'};
  width: 100%;
  transform: translateY(${TRANSLATE_DISTANCE});
  animation: ${ANIMATIONS.FADE_UP(TRANSLATE_DISTANCE)} 1s both;
  animation-delay: ${({ index }) => index * 200}ms;
  cursor: pointer;

  :not(:last-child) {
    margin-bottom: 7rem;
  }

  ${MEDIA_IE_HOVER} {
    h3 {
      transition: color 0.1s ease-in;
    }

    :hover {
      h3 {
        color: ${COLORS.BLUE};
      }

      ${SeeMore} {
        color: ${COLORS.BLUE};
        ${LINK_UNDERLINE.HOVER};
      }

      ${Cover} {
        ::after {
          background: #0000ff80;
        }

        ::before {
          border-color: ${COLORS.BLUE};
          transform: translate(-10px, -10px);
        }
      }
    }
  }

  ${SCREEN.ABOVE_MOBILE} {
    width: 28rem;

    :not(:last-child) {
      margin-bottom: 0;
    }

    :nth-child(2n) {
      margin-top: ${({ isCaseDetailPage }) => (isCaseDetailPage ? 0 : '10rem')};
    }

    :nth-child(2n + 1) {
      margin-bottom: ${({ isCaseDetailPage }) =>
        isCaseDetailPage ? 0 : '10rem'};
    }

    :last-child {
      margin-bottom: 0;
    }
  }

  ${SCREEN.ABOVE_TABLET} {
    width: 40rem;
  }

  ${SCREEN.ABOVE_MAX_CONTENT_WIDTH} {
    :not(:nth-child(3n + 2)) {
      margin: 0 0 10rem;
    }

    :nth-child(3n + 2) {
      margin-top: ${({ isCaseDetailPage }) => (isCaseDetailPage ? 0 : '10rem')};
    }
  }
`

const Logo = styled.div<{ url: string }>`
  width: 14rem;
  height: 4rem;
  margin-bottom: 2rem;
  background: ${({ url }) => `url(${url})`};
  background-repeat: no-repeat;
  background-size: contain;
`

const Description = styled.p`
  margin: 2rem 0;
  font-size: ${FONT_SIZE.SMALL};
`

interface CaseStudyCardProps
  extends Pick<
    CaseStudy,
    | 'name'
    | 'cover'
    | 'coverPublicUrl'
    | 'logoPublicUrl'
    | 'logo'
    | 'shortDescription'
    | 'description'
  > {
  index: number
  isCaseDetailPage?: boolean
}

export const CaseStudyCard: React.FC<CaseStudyCardProps> = ({
  index,
  name,
  coverPublicUrl,
  logoPublicUrl,
  shortDescription,
  description,
  isCaseDetailPage,
}) => {
  const triggerTransition = usePageTransition(`/work/${formatSlug(name)}`)
  const { file } = useStaticQuery(graphql`
    {
      file(relativePath: { eq: "card_default.jpg" }) {
        id
        childImageSharp {
          fluid {
            src
          }
        }
      }
    }
  `)

  return (
    <TransitionState>
      {({ transitionStatus }: { transitionStatus: string }) => (
        <Container
          index={index}
          onClick={triggerTransition}
          isCaseDetailPage={isCaseDetailPage}
          isReadyToRender={
            transitionStatus === 'entered' || transitionStatus === 'exiting'
          }
        >
          <Cover
            url={
              coverPublicUrl
                ? `${coverPublicUrl}`
                : file.childImageSharp.fluid.src
            }
          />
          {logoPublicUrl && <Logo url={logoPublicUrl} />}
          <ShortDescription>{shortDescription}</ShortDescription>
          <Description>{description}</Description>
          <AnimatedLink to={'/work/' + formatSlug(name)}>
            <SeeMore>See more</SeeMore>
          </AnimatedLink>
        </Container>
      )}
    </TransitionState>
  )
}
