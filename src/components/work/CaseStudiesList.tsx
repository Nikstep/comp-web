import React, { useState, useEffect, useRef } from 'react'
import styled from 'styled-components'
import { COLORS, FONT_SIZE, FONT_WEIGHT, SCREEN } from '../../constants'
import { CaseStudy } from '../../types/CaseStudy'
import { formatSlug } from '../../utils/formatSlug'
import { HorizontalButton } from '../HorizontalButton'
import { AnimatedLink } from '../AnimatedLink'

const Container = styled.div`
  margin-bottom: 10rem;
`

const ListContainer = styled.div<{
  isOpened: boolean
  height: number
  isListToShort: boolean
}>`
  position: relative;
  height: ${({ isOpened, height }) =>
    isOpened ? `${height / 10}rem` : '25rem'};
  margin-bottom: ${({ isListToShort }) => (isListToShort ? '10rem' : 0)};
  overflow-y: hidden;
  transition: height 1.5s cubic-bezier(0.26, -0.15, 0.1, 1.13);

  ${SCREEN.ABOVE_MOBILE} {
    height: ${({ isOpened, height }) =>
      isOpened ? `${height / 10}rem` : '15rem'};
    transition-duration: 1s;
  }
`

const Columns = styled.div`
  display: flex;
  justify-content: space-between;
  flex-wrap: wrap;
  padding-bottom: 5rem;
`

const Column = styled.div`
  flex-basis: 100%;

  ${SCREEN.ABOVE_MOBILE} {
    flex-basis: 50%;
  }

  ${SCREEN.ABOVE_LAPTOP} {
    flex-basis: 25%;
  }
`

// eslint-disable-next-line
const ListElement = styled(({ isFeatured, ...props }) => (
  <AnimatedLink {...props} />
))<{
  isFeatured?: boolean
}>`
  display: flex;
  margin-bottom: 1rem;
  font-size: ${FONT_SIZE.EXTRA_SMALL};
  font-weight: ${({ isFeatured }) =>
    isFeatured ? FONT_WEIGHT.MEDIUM : FONT_WEIGHT.BASE};
`

const Industry = styled.span`
  flex: 1 1 30%;
  width: 10rem;
  margin-right: 2rem;
  color: ${COLORS.BLUE};
  text-align: right;
`

const Title = styled.span`
  flex: 1 1 70%;

  :hover {
    color: ${COLORS.BLUE};
  }
`

const Cover = styled.div<{ isCollapsed: boolean }>`
  position: absolute;
  bottom: 0;
  width: 100%;
  height: 10rem;
  background: linear-gradient(
    to top,
    ${COLORS.WHITE} 15%,
    rgba(255, 255, 255, 0.001)
  );
  transition: bottom 0.7s ease-in;

  ${SCREEN.ABOVE_MOBILE} {
    transition: bottom 0.3s ease-in;
  }
`

const StyledHorizontalButton = styled(HorizontalButton)<{
  isListOpen: boolean
}>`
  width: 100%;
  transition: margin-top 0.7s ease-in;

  ${SCREEN.ABOVE_MOBILE} {
    transition: margin-top 0.3s ease-in;
  }
`

const compare = (a: CaseStudy, b: CaseStudy) => {
  if (a.industry.title > b.industry.title) {
    return 1
  }
  if (a.industry.title < b.industry.title) {
    return -1
  }
  return 0
}

interface CaseStudiesListProps {
  list: CaseStudy[]
  selectedIndustry?: number
}

export const CaseStudiesList: React.FC<CaseStudiesListProps> = ({ list }) => {
  const [isListOpened, toggleList] = useState(false)
  const [containerHeight, setContainerHeight] = useState(0)
  const ref = useRef<HTMLDivElement>(null)

  useEffect(() => {
    const containerEl = ref.current

    if (containerEl) {
      setContainerHeight(containerEl.getBoundingClientRect().height)
    }
  }, [list])

  const isListToShort = list.length <= 16

  const formattedList = list
    .sort(compare)
    .reduce((acc: Array<Array<CaseStudy>>, current, index) => {
      const itemsPerColumn = Math.ceil(list.length / 4)

      const currentIndex = Math.floor(index / itemsPerColumn)

      if (!acc[currentIndex]) {
        acc[currentIndex] = []
      }

      acc[currentIndex].push(current)

      return acc
    }, [])

  const handleButtonClick = () => {
    toggleList(!isListOpened)
  }

  return (
    <Container>
      <ListContainer
        isOpened={isListOpened}
        height={containerHeight}
        isListToShort={isListToShort}
      >
        <Columns ref={ref}>
          {formattedList.map(column => (
            <Column key={column[0].id}>
              {column.map(caseStudy => (
                <ListElement
                  to={`/work/${formatSlug(caseStudy.name)}`}
                  key={caseStudy.id}
                  isFeatured={caseStudy.isFeatured}
                >
                  <Industry>{caseStudy.industry.title}</Industry>
                  <Title>{caseStudy.shortDescription}</Title>
                </ListElement>
              ))}
            </Column>
          ))}
        </Columns>

        {!isListToShort && <Cover isCollapsed={isListOpened} />}
      </ListContainer>

      {!isListToShort && (
        <StyledHorizontalButton
          alignment="center"
          icon={!isListOpened ? 'list' : 'upArrow'}
          isListOpen={isListOpened}
          onClick={handleButtonClick}
          ariaLabel={`${!isListOpened ? 'See' : 'Hide'} the whole list`}
        >
          {`${!isListOpened ? 'See' : 'Hide'} the whole list`}
        </StyledHorizontalButton>
      )}
    </Container>
  )
}
