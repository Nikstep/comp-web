import React, { useState, forwardRef, useEffect, useRef } from 'react'
import styled from 'styled-components'
import { graphql, useStaticQuery } from 'gatsby'
import { Heading } from '../Heading'
import { SCREEN, FONT_WEIGHT, COLORS, MEDIA_IE_HOVER } from '../../constants'
import { CaseStudy, CaseStudies } from '../../types/CaseStudy'
import { Button } from '../Button'
import { CaseStudyCard } from './CaseStudyCard'
import { CaseStudyFilters } from './CaseStudyFilters'
import { CaseStudiesList } from './CaseStudiesList'
import { useIsInView } from '../../utils/useIsInView'
import { HorizontalButton } from '../HorizontalButton'
import { usePageTransition } from '../../utils/usePageTransition'

const StyledHeading = styled(Heading)<{ isCaseStudyPage?: boolean }>`
  margin-bottom: ${({ isCaseStudyPage }) =>
    isCaseStudyPage ? '4rem' : '10rem'};

  ${SCREEN.ABOVE_LAPTOP} {
    margin-bottom: ${({ isCaseStudyPage }) => isCaseStudyPage && '5rem'};
  }
`

export const CardsContainer = styled.div<{ isInView: boolean }>`
  display: flex;
  flex-wrap: wrap;
  justify-content: space-between;
  max-width: 100%;
  min-height: 60rem;

  > div {
    display: ${({ isInView }) => !isInView && 'none'};
  }

  ${SCREEN.ABOVE_MOBILE} {
    padding: 0 1rem 0 3rem;
  }

  ${SCREEN.ABOVE_TABLET} {
    padding: 0;
  }

  ${SCREEN.ABOVE_LAPTOP} {
    width: 90rem;
    margin: 0 auto 0;
  }

  ${SCREEN.ABOVE_MAX_CONTENT_WIDTH} {
    justify-content: space-around;
    width: 140rem;
  }
`

const SeeMoreContainer = styled.div`
  display: flex;
  align-items: center;
  width: 15rem;
  margin: 5rem auto 0;
  cursor: pointer;

  ${MEDIA_IE_HOVER} {
    :hover {
      color: ${COLORS.BLUE};

      button {
        transform: scale(1.1) translateZ(0px);
      }
    }
  }
`

const SeeMore = styled.span`
  margin-left: 2rem;
  font-weight: ${FONT_WEIGHT.MEDIUM};
`
const LineBreak = styled.br`
  display: none;

  ${SCREEN.ABOVE_TABLET} {
    display: block;
  }
`

const DEFAULT_LIMIT = 6

interface CaseStudySectionProps {
  isCaseStudyPage?: boolean
}

export const CaseStudySection = forwardRef<HTMLElement, CaseStudySectionProps>(
  ({ isCaseStudyPage }, ref) => {
    const [selectedIndustry, setSelectedIndustry] = useState<number>()
    const [limit, setLimit] = useState(DEFAULT_LIMIT)

    const cardRef = useRef<HTMLDivElement>(null)
    const isInView = useIsInView(cardRef, 50)
    const triggerTransition = usePageTransition('/work')

    const data = useStaticQuery<CaseStudies>(graphql`
      query CaseStudiesQuery {
        allStrapiCaseStudy(sort: { fields: isFeatured, order: DESC }) {
          nodes {
            strapiId
            logo {
              publicURL
            }
            cardImage {
              publicURL
            }
            cover {
              publicURL
            }
            industry {
              id
              title
            }
            isFeatured
            client
            id
            shortDescription
            description
            name
          }
        }
        sitePage {
          path
        }
      }
    `)

    useEffect(() => {
      setLimit(DEFAULT_LIMIT)
    }, [selectedIndustry])

    const filter = (caseStudy: CaseStudy) => {
      if (selectedIndustry) {
        return caseStudy.industry.id === selectedIndustry
      }

      return true
    }

    const filteredCaseStudies = data.allStrapiCaseStudy.nodes.filter(filter)

    return (
      <section ref={ref}>
        <StyledHeading
          isMain={isCaseStudyPage}
          isCaseStudyPage={isCaseStudyPage}
        >
          Over $1 billion in new value <LineBreak /> across 70+ products built
        </StyledHeading>

        {isCaseStudyPage && (
          <CaseStudyFilters
            selectedIndustry={selectedIndustry}
            setSelectedIndustry={setSelectedIndustry}
          />
        )}

        {isCaseStudyPage && (
          <CaseStudiesList
            list={filteredCaseStudies}
            selectedIndustry={selectedIndustry}
          />
        )}

        <CardsContainer isInView={isInView} ref={cardRef}>
          {filteredCaseStudies
            .map(
              (
                { id, name, cardImage, logo, shortDescription, description },
                index
              ) => (
                <CaseStudyCard
                  key={selectedIndustry ? id + selectedIndustry : id}
                  index={index % DEFAULT_LIMIT}
                  name={name}
                  coverPublicUrl={cardImage?.publicURL}
                  logoPublicUrl={logo?.publicURL}
                  shortDescription={shortDescription}
                  description={description}
                />
              )
            )
            .slice(0, limit)}
        </CardsContainer>

        {!isCaseStudyPage && (
          <HorizontalButton onClick={triggerTransition} ariaLabel="Work page">
            All case studies
          </HorizontalButton>
        )}

        {isCaseStudyPage && filteredCaseStudies.length > limit && (
          <SeeMoreContainer
            onClick={() => setLimit(current => current + DEFAULT_LIMIT)}
          >
            <Button ariaLabel="See more case studies" />
            <SeeMore>See more</SeeMore>
          </SeeMoreContainer>
        )}
      </section>
    )
  }
)
