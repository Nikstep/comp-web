import React from 'react'
import { useStaticQuery, graphql } from 'gatsby'
import styled from 'styled-components'
import { IndustryCounts, Industries } from '../../types/Industry'
import {
  FONT_SIZE,
  COLORS,
  SCREEN,
  MEDIA_IE_HOVER,
  FONT_WEIGHT,
  PADDING,
} from '../../constants'

const Container = styled.div`
  display: flex;
  align-items: flex-start;
  margin: 0 -${PADDING.MOBILE}rem 6rem;
  overflow-x: scroll;

  ${SCREEN.ABOVE_TABLET} {
    margin: 0 0 4rem;
    overflow-x: visible;
  }
`

const Title = styled.div<{ isActive: boolean }>`
  position: relative;
  padding: 0 0.5rem 0.3rem;
  margin-left: -0.5rem;
  white-space: pre;
  font-size: ${FONT_SIZE.EXTRA_SMALL};
  color: ${COLORS.LIGHT_GREY};

  ::after {
    content: '';
    position: absolute;
    left: 0;
    bottom: 0;
    width: 100%;
    height: 0;
    border-top: 2px solid blue;
    transition: transform 0.2s;
    transform: ${({ isActive }) => (isActive ? 'scale(1, 1)' : 'scale(0, 0)')};
  }

  ${SCREEN.ABOVE_TABLET} {
    text-align: initial;
  }
`

const Count = styled.div`
  font-size: ${FONT_SIZE.EXTRA_LARGE};
  font-weight: ${FONT_WEIGHT.MEDIUM};
`

const FilterContainer = styled.div<{ isActive: boolean }>`
  display: flex;
  flex-direction: column;
  width: 9rem;
  min-width: 6rem;
  text-align: left;
  cursor: pointer;

  ${Count},
  ${Title} {
    color: ${({ isActive }) => isActive && COLORS.BLUE};
  }

  :first-child {
    margin-left: ${PADDING.MOBILE}rem;
  }

  :not(:last-child) {
    margin-right: 2rem;
  }

  ${SCREEN.ABOVE_TABLET} {
    align-items: flex-start;

    :first-child {
      margin-left: 0;
    }
  }

  ${MEDIA_IE_HOVER} {
    :hover {
      ${Count},
      ${Title} {
        color: ${COLORS.BLUE};
      }

      ${Title} {
        ::after {
          transform: scale(1, 1);
        }
      }
    }
  }
`
interface IndustryGroup {
  count: number
  title: string
  strapiId: number
}

const compare = (a: IndustryGroup, b: IndustryGroup) => {
  if (a.count < b.count) {
    return 1
  }
  if (a.count > b.count) {
    return -1
  }
  return 0
}

interface CaseStudyFiltersProps {
  selectedIndustry: number | undefined
  setSelectedIndustry: React.Dispatch<React.SetStateAction<number | undefined>>
}

export const CaseStudyFilters: React.FC<CaseStudyFiltersProps> = ({
  selectedIndustry,
  setSelectedIndustry,
}) => {
  const {
    allStrapiIndustry: industries,
    allRestApiCaseStudies: industryCounts,
  } = useStaticQuery<IndustryCounts & Industries>(graphql`
    query IndustriesQuery {
      allRestApiCaseStudies {
        group(field: industry___id) {
          totalCount
          fieldValue
        }
      }
      allStrapiIndustry {
        nodes {
          title
          strapiId
        }
      }
    }
  `)

  const industryGroups: Array<IndustryGroup> = industries.nodes
    .reduce((acc: Array<IndustryGroup>, { title, strapiId }) => {
      const count = industryCounts.group.find(
        ({ fieldValue }) => fieldValue == strapiId
      )?.totalCount
      const newTitle = title.replace(' ', '\n')

      if (count && count > 1) {
        return [{ count, title: newTitle, strapiId }, ...acc]
      }

      return acc
    }, [])
    .sort(compare)

  return (
    <Container>
      <FilterContainer
        isActive={!selectedIndustry}
        onClick={() => setSelectedIndustry(undefined)}
      >
        <Count>All</Count>
        <Title isActive={!selectedIndustry}>Projects</Title>
      </FilterContainer>

      {industryGroups.map(({ count, title, strapiId }) => {
        if (count) {
          return (
            <FilterContainer
              key={strapiId}
              isActive={selectedIndustry === strapiId}
              onClick={() => setSelectedIndustry(strapiId)}
            >
              <Count>{count}</Count>
              <Title isActive={selectedIndustry === strapiId}>{title}</Title>
            </FilterContainer>
          )
        }
      })}
    </Container>
  )
}
