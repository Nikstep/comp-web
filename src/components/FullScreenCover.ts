import styled from 'styled-components'
import { COLORS, PADDING, SCREEN } from '../constants'

export const FullScreenCover = styled.div<{ url?: string }>`
  height: 35rem;
  margin-right: -${PADDING.MOBILE}rem;
  background: ${({ url }) => (url ? `url(${url})` : COLORS.BLUE)};
  background-repeat: no-repeat;
  background-position: center;
  background-size: cover;

  ${SCREEN.ABOVE_MOBILE} {
    height: 55rem;
    margin-right: -${PADDING.MOBILE}rem;
  }

  ${SCREEN.ABOVE_TABLET} {
    margin-right: -${PADDING.DESKTOP}rem;
  }
`
