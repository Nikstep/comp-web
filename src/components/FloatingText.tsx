import React from 'react'
import styled from 'styled-components'
import { PADDING, COLORS, SCREEN } from '../constants'

const FloatingTextContainer = styled.div`
  position: absolute;
  top: 20rem;
  right: ${PADDING.MOBILE}rem;
  display: none;
  flex-direction: column;
  align-items: center;
  cursor: default;
  transform: rotate(180deg);

  span {
    writing-mode: tb;
    -ms-writing-mode: tb-lr;
  }

  ${SCREEN.ABOVE_MOBILE} {
    display: flex;
  }

  ${SCREEN.ABOVE_TABLET} {
    right: ${PADDING.DESKTOP}rem;
  }
`

const Line = styled.div`
  width: 0.1rem;
  height: 6.6rem;
  margin-top: 1rem;
  background: ${COLORS.BLACK};
`

interface FloatingTextProps {
  onClick?: () => void
}

export const FloatingText: React.FC<FloatingTextProps> = ({
  children,
  onClick,
}) => (
  <FloatingTextContainer onClick={onClick}>
    <span>{children}</span>
    <Line />
  </FloatingTextContainer>
)
