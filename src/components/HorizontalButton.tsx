import React from 'react'
import styled from 'styled-components'
import { Button, ButtonProps } from './Button'
import { COLORS, FONT_WEIGHT, FONT_SIZE, MEDIA_IE_HOVER } from '../constants'

const ButtonContainer = styled.div<{
  alignment?: 'flex-start' | 'center' | 'flex-end'
}>`
  display: flex;
  justify-content: ${({ alignment }) => alignment};
  align-items: center;
  margin-top: 5rem;
  font-weight: ${FONT_WEIGHT.MEDIUM};
  font-size: ${FONT_SIZE.SMALL};
  cursor: pointer;

  ${MEDIA_IE_HOVER} {
    :hover {
      button {
        transform: scale(1.1) translateZ(0px);
      }
    }
  }
`

const Line = styled.div`
  width: 6.6rem;
  height: 0.1rem;
  margin: 0 3rem 0;
  background: ${COLORS.BLACK};
`

const Children = styled.span`
  margin-right: 2rem;
`

interface HorizontalButtonProps extends ButtonProps {
  ariaLabel: string
  alignment?: 'flex-start' | 'center' | 'flex-end'
  className?: string
}

export const HorizontalButton: React.FC<HorizontalButtonProps> = ({
  alignment,
  children,
  link,
  onClick,
  icon,
  ariaLabel,
  className,
}) => (
  <ButtonContainer
    alignment={alignment ? alignment : 'flex-end'}
    onClick={onClick}
    className={className}
  >
    <Line />
    <Children>{children}</Children>
    <Button link={link} icon={icon} ariaLabel={ariaLabel} />
  </ButtonContainer>
)
