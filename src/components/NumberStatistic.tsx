import React from 'react'
import styled from 'styled-components'
import { COLORS, Z_INDEX, FONT_WEIGHT } from '../constants'
import { Statistic } from '../types/Statistic'

const Amount = styled.div`
  display: flex;
  align-items: flex-end;
  line-height: 2.3rem;
  font-size: 3.6rem;
  font-weight: ${FONT_WEIGHT.BOLD};
`

const NumberWithCircle = styled.span`
  display: flex;
  align-items: flex-end;
  position: relative;

  ::before {
    content: '';
    position: absolute;
    left: -50%;
    bottom: 0;
    width: 4.5rem;
    height: 4.5rem;
    z-index: ${Z_INDEX.BACKGROUND};
    border-radius: 50%;
    background: ${COLORS.YELLOW};
  }
`

const Description = styled.div`
  margin-top: 1.5rem;
  font-size: 1.4rem;
  color: ${COLORS.LIGHT_GREY};
`

interface NumberStatisticProps extends Statistic {
  className?: string
}

export const NumberStatistic: React.FC<NumberStatisticProps> = ({
  className,
  number,
  text,
}) => {
  const firstCharacter = number.substring(0, 1)
  const remainingCharacters = number.substring(1, number.length)

  return (
    <div className={className}>
      <Amount>
        <NumberWithCircle>{firstCharacter}</NumberWithCircle>
        <span>{remainingCharacters}</span>
      </Amount>
      <Description>
        {text.map(value => (
          <div key={value}>{value}</div>
        ))}
      </Description>
    </div>
  )
}
