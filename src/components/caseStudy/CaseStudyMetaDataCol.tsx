import React from 'react'
import styled from 'styled-components'
import { COLORS, FONT_SIZE, FONT_WEIGHT, SCREEN } from '../../constants'

const StyledCaseMetaDataCol = styled.div`
  ${SCREEN.ABOVE_MOBILE} {
    :not(:last-child) {
      margin-right: 8rem;
    }
  }
`
export const MetaDataTitle = styled.p`
  margin-bottom: 0.5rem;
  color: ${COLORS.LIGHT_GREY};
  font-size: ${FONT_SIZE.SMALL};
`
const MetaData = styled.p`
  font-size: ${FONT_SIZE.BASE};
  font-weight: ${FONT_WEIGHT.MEDIUM};
  line-height: 2.6rem;
`

interface CaseMetaDataColProps {
  title: string
  children: string
}

export const CaseStudyMetaDataCol: React.FC<CaseMetaDataColProps> = ({
  title,
  children,
}) => (
  <StyledCaseMetaDataCol>
    <MetaDataTitle>{title}</MetaDataTitle>
    <MetaData>{children}</MetaData>
  </StyledCaseMetaDataCol>
)
