import React from 'react'
import styled from 'styled-components'
import {
  COLORS,
  FONT_WEIGHT,
  FONT_SIZE,
  SCREEN,
  Z_INDEX,
  OFFSET_POSITION_TOP,
  OFFSET_POSITION_LEFT,
} from '../../constants'
import { UnorderedList } from '../UnorderedList'
import { Item } from '../../types/CaseStudy'

const Container = styled.div`
  display: flex;
  flex-direction: column;
  margin-top: 5rem;
  font-size: ${FONT_SIZE.MEDIUM};
  line-height: 2.5rem;

  ${SCREEN.ABOVE_TABLET} {
    flex-direction: row;
    margin: 5rem 0 8rem;
    font-size: ${FONT_SIZE.LARGE};
    line-height: 3.2rem;
  }
`
const Column = styled.div`
  flex: 0 1 45%;

  ${SCREEN.ABOVE_MOBILE} {
    padding-right: 8rem;

    :nth-child(2) {
      padding-right: 3rem;
    }
  }

  ${SCREEN.ABOVE_TABLET} {
    :nth-child(2) {
      flex: 0 1 55%;
      padding-right: 0;
      padding-left: 6rem;
    }
  }
`
const Heading = styled.h3<{ isListHeading?: boolean }>`
  margin-bottom: ${({ isListHeading }) => (isListHeading ? '2rem' : '0.5rem')};
  padding-left: ${({ isListHeading }) => (isListHeading ? '2.5rem' : 0)};
  font-size: 3rem;
  font-weight: ${FONT_WEIGHT.MEDIUM};
  line-height: 4rem;
  letter-spacing: 0.05rem;

  ${SCREEN.ABOVE_MOBILE} {
    margin-bottom: ${({ isListHeading }) => (isListHeading ? '3rem' : '1rem')};
    padding-left: ${({ isListHeading }) => (isListHeading ? '6rem' : 0)};
  }
`

const CardContainer = styled.div`
  position: relative;
  padding: 4rem 3rem;
  background: ${COLORS.WHITE};
  border: 3px solid ${COLORS.BLACK};
  margin: 3rem 0;

  ::after {
    content: '';
    position: absolute;
    top: -${OFFSET_POSITION_TOP.MOBILE}rem;
    left: -${OFFSET_POSITION_LEFT.MOBILE}rem;
    width: 100%;
    height: 100%;
    z-index: ${Z_INDEX.BACKGROUND};
    background: ${COLORS.BLUE};
    transition: transform 0.2s ease-in;
  }

  ${SCREEN.ABOVE_MOBILE} {
    padding: 5rem 6rem;
    margin: 6rem 0 0 4rem;

    ::after {
      top: -${OFFSET_POSITION_TOP.DESKTOP}rem;
      left: -${OFFSET_POSITION_LEFT.DESKTOP}rem;
    }
  }

  ${SCREEN.ABOVE_TABLET} {
    margin: 0;
  }
`

const Paragraph = styled.p`
  margin-bottom: 3rem;
  font-size: ${FONT_SIZE.MEDIUM};
  line-height: 3.2rem;
  max-width: 50rem;

  ${SCREEN.ABOVE_TABLET} {
    margin-bottom: 5.5rem;

    :last-child {
      margin-bottom: 0;
    }
  }
`
const StyledUnorderedList = styled(UnorderedList)`
  letter-spacing: 0.06rem;
  font-size: ${FONT_SIZE.MEDIUM};

  ${SCREEN.ABOVE_TABLET} {
    font-size: ${FONT_SIZE.LARGE};
    font-size: min(2.4rem, 2.2vw);
  }

  li {
    padding-bottom: 1.5rem;

    :before {
      ${SCREEN.ABOVE_MOBILE} {
        content: ' ';
        margin-top: 1.8rem;
        margin-right: 4rem;
        margin-left: -4rem;
        width: 4.2rem;
        border-top: 2px solid ${COLORS.BLACK};
      }
    }
  }
`

interface CaseHighlightsProps {
  approach: string
  mission: string
  outcomes: Array<Item>
}

export const CaseStudyHighlights: React.FC<CaseHighlightsProps> = ({
  mission,
  approach,
  outcomes,
}) => (
  <>
    {(mission || approach || outcomes.length > 0) && (
      <Container>
        <Column>
          {mission && (
            <>
              <Heading>Our Mission</Heading>
              <Paragraph>{mission}</Paragraph>
            </>
          )}
          {approach && (
            <>
              <Heading>Our Approach</Heading>
              <Paragraph>{approach}</Paragraph>
            </>
          )}
        </Column>
        <Column>
          {outcomes.length > 0 && (
            <CardContainer>
              <Heading isListHeading>Outcomes</Heading>
              <StyledUnorderedList list={outcomes} />
            </CardContainer>
          )}
        </Column>
      </Container>
    )}
  </>
)
