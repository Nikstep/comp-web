import React from 'react'
import styled from 'styled-components'
import { BodyElement } from '../../types/BodyElement'
import { API_URL, COLORS, FONT_SIZE, SCREEN } from '../../constants'

const Container = styled.div`
  max-width: 96rem;
  margin: 0 auto 8rem;

  ${SCREEN.ABOVE_TABLET} {
    margin-bottom: 12rem;
  }

  ${SCREEN.ABOVE_LAPTOP} {
    margin-bottom: 16rem;
  }

  h2 {
    font-size: ${FONT_SIZE.EXTRA_LARGE};
    line-height: 5rem;
  }

  h3 {
    font-size: ${FONT_SIZE.LARGE};
    line-height: 2.5rem;
  }
`

const Figure = styled.figure`
  margin: 0 auto 4rem;
`

const Media = styled.img`
  max-width: 100%;
  margin-bottom: 1rem;
`

const Caption = styled.figcaption`
  width: 100%;
  text-align: center;
  font-size: ${FONT_SIZE.EXTRA_SMALL};
  color: ${COLORS.LIGHT_GREY};
`

const Paragraph = styled.p<{ textAlignment?: string }>`
  margin-bottom: 1.75rem;
  text-align: ${({ textAlignment }) =>
    textAlignment ? textAlignment : 'left'};
`

interface ContentBodyProps {
  nodes: Array<BodyElement>
}

export const Body: React.FC<ContentBodyProps> = ({ nodes }) => (
  <Container>
    {nodes.map((el: BodyElement) => {
      const key = el._xcomponent + '-' + el.id
      switch (el._xcomponent.split('.')[1]) {
        case 'h2': {
          return <h2 key={key}>{el.text}</h2>
        }
        case 'h3': {
          return <h3 key={key}>{el.text}</h3>
        }
        case 'h4': {
          return <h4 key={key}>{el.text}</h4>
        }
        case 'quote': {
          return (
            <p key={key}>
              <small>{el.author}: </small>
              <q>{el.text}</q>
            </p>
          )
        }
        case 'media': {
          return (
            <Figure>
              <Media key={key} src={`${API_URL}${el.media?.url}`} alt={key} />
              {el.caption && <Caption>{el.caption}</Caption>}
            </Figure>
          )
        }
        case 'paragraph': {
          return (
            <Paragraph key={key} textAlignment={el.textAlignment}>
              {el.text}
            </Paragraph>
          )
        }
      }
    })}
  </Container>
)
