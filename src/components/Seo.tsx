import React from 'react'
import Helmet from 'react-helmet'
import { useStaticQuery, graphql } from 'gatsby'
interface SEOProps {
  title: string
  description?: string
}

const SEO: React.FC<SEOProps> = ({ description, title }) => {
  const { site, file } = useStaticQuery(
    graphql`
      {
        site {
          siteMetadata {
            title
            description
            author
            siteUrl
          }
        }
        file(relativePath: { eq: "banner.png" }) {
          publicURL
        }
      }
    `
  )
  const {
    title: defaultTitle,
    description: defaultDescription,
    author,
    siteUrl,
  } = site.siteMetadata
  const metaDescription = description || defaultDescription

  return (
    <Helmet
      htmlAttributes={{
        lang: 'en',
      }}
      title={title || defaultTitle}
      titleTemplate={`%s | ${defaultTitle}`}
      meta={[
        {
          name: 'description',
          content: metaDescription,
        },
        {
          property: 'og:title',
          content: title || defaultTitle,
        },
        {
          property: 'og:description',
          content: metaDescription,
        },
        {
          property: 'og:image',
          content: siteUrl + file.publicURL,
        },
        {
          property: 'og:url',
          content: siteUrl,
        },
        {
          property: 'og:type',
          content: 'website',
        },
        {
          name: 'twitter:card',
          content: 'summary_large_image',
        },
        {
          name: 'twitter:creator',
          content: author,
        },
        {
          name: 'twitter:site',
          content: author,
        },
        {
          name: 'twitter:title',
          content: title || defaultTitle,
        },
        {
          name: 'twitter:description',
          content: metaDescription,
        },
        {
          name: 'twitter:image',
          content: siteUrl + file.publicURL,
        },
        {
          name: 'google-site-verification',
          content: 'MQPFh_a0SYrXmPoU7NIQiAda2ZhT_LzLstUIwrYh-hA',
        },
      ]}
    />
  )
}

export default SEO
