import React from 'react'
import styled from 'styled-components'
import {
  COLORS,
  MEDIA_IE_HOVER,
  FONT_SIZE,
  FONT_WEIGHT,
  SCREEN,
} from '../constants'

type TextAlign = 'left' | 'right' | 'center'

type Spacing = 'normal' | 'wide'

const Container = styled.div<{ textAlign: TextAlign }>`
  text-align: ${({ textAlign }) => textAlign};
  margin-bottom: 2.5rem;
`

const Subheading = styled.p<{ textAlign: TextAlign }>`
  color: ${COLORS.LIGHT_GREY};
  font-size: ${FONT_SIZE.SMALL};
  font-weight: ${FONT_WEIGHT.MEDIUM};
  letter-spacing: 0.05rem;

  a {
    ${MEDIA_IE_HOVER} {
      :hover {
        text-decoration: underline;
      }
    }
  }
`

const StyledHeading = styled.h2<{ spacing: Spacing }>`
  word-spacing: ${({ spacing }) => spacing === 'wide' && '100vw'};

  ${SCREEN.ABOVE_LAPTOP} {
    margin-left: -0.5rem;
  }
`

interface HeadingProps {
  subheading?: React.ReactNode
  isMain?: boolean
  textAlign?: TextAlign
  spacing?: Spacing
  className?: string
}

export const Heading: React.FC<HeadingProps> = ({
  subheading,
  isMain,
  textAlign = 'left',
  children,
  spacing = 'normal',
  className,
}) => (
  <Container textAlign={textAlign} className={className}>
    {subheading && <Subheading textAlign={textAlign}>{subheading}</Subheading>}
    <StyledHeading spacing={spacing} as={isMain ? 'h1' : 'h2'}>
      {children}
    </StyledHeading>
  </Container>
)
