import React from 'react'
import styled from 'styled-components'
import { Heading } from '../Heading'
import { SectionDescription } from '../SectionDescription'
import {
  LINK_UNDERLINE,
  FONT_SIZE,
  FONT_WEIGHT,
  MEDIA_IE_HOVER,
  COLORS,
  SCREEN,
} from '../../constants'
import { MAIN_EMAIL } from '../../constants/contacts'

const StyledDescription = styled(SectionDescription)`
  ${SCREEN.ABOVE_LAPTOP} {
    margin-bottom: 4rem;
  }
`

const MailContainer = styled.div`
  display: flex;
  justify-content: center;
`

const Mail = styled.a`
  position: relative;
  padding: 0 0.5rem 0.5rem;
  margin: 0 auto;
  text-align: center;
  font-size: ${FONT_SIZE.MEDIUM};
  font-weight: ${FONT_WEIGHT.MEDIUM};
  transition: color 0.1s ease-in;

  ${LINK_UNDERLINE.DEFAULT};

  ${MEDIA_IE_HOVER} {
    :hover {
      color: ${COLORS.BLUE};
      ${LINK_UNDERLINE.HOVER};
    }
  }
`

export const WorkshopSection = () => (
  <section>
    <Heading subheading="Teach the teacher" textAlign="center">
      Method workshops
    </Heading>
    <StyledDescription>
      The Method workshops walk you through the product development process to
      see how you can implement the thinking inside your organization with your
      stakeholders.
    </StyledDescription>
    <MailContainer>
      <Mail
        href={`mailto:${MAIN_EMAIL}?subject=U+Method workshop&body=Hi U+, I would like apply your method internally...`}
      >
        Request a workshop
      </Mail>
    </MailContainer>
  </section>
)
