import React from 'react'
import styled from 'styled-components'
import { useStaticQuery, graphql } from 'gatsby'
import { NumberStatistic } from '../NumberStatistic'
import { Statistic } from '../../types/Statistic'
import { SCREEN } from '../../constants'
import { METHOD_STAGES } from '../../constants/startupStages'
import { Heading } from '../Heading'
import { SectionDescription } from '../SectionDescription'
import { StageCard } from './StageCard'
import { MethodGuide } from '../../types/MethodGuide'

const StyledDescription = styled(SectionDescription)`
  margin-bottom: 5rem;

  ${SCREEN.ABOVE_TABLET} {
    margin-bottom: 10rem;
  }
`

const TopSection = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  max-width: 110rem;

  ${SCREEN.ABOVE_TABLET} {
    flex-direction: row;
  }
`

const Statistics = styled.div`
  display: flex;
  margin: 0 0 12rem 2rem;

  ${SCREEN.ABOVE_MOBILE} {
    margin-left: 4rem;
  }

  ${SCREEN.ABOVE_LAPTOP} {
    margin: 0;
  }
`

const NumberContainer = styled.div`
  margin-top: 3rem;

  :not(:last-child) {
    margin-right: 7.5rem;
  }
`

const StagesContainer = styled.div`
  ${SCREEN.ABOVE_MOBILE} {
    display: grid;
    grid-template-columns: 1fr 1fr;
    grid-column-gap: 8rem;
    margin: 5rem 0 0 5rem;
  }

  ${SCREEN.ABOVE_TABLET} {
    display: -ms-grid;
    grid-template-columns: 1fr 1fr;
    -ms-grid-columns: 1fr 1fr;
    margin: 12rem 0 0 5rem;
  }
`

const statisticData: Array<Statistic> = [
  {
    number: '150+',
    text: ['Pages'],
  },
  {
    number: '40+',
    text: ['Articles'],
  },
]

export interface MethodGroup {
  nodes: Array<MethodGuide>
  fieldValue: string
}

export interface GroupedStages {
  allMarkdownRemark: {
    group: Array<MethodGroup>
  }
}

export const StageSection: React.FC = () => {
  const { allMarkdownRemark } = useStaticQuery<GroupedStages>(
    graphql`
      query {
        allMarkdownRemark(sort: { fields: frontmatter___index }) {
          group(field: frontmatter___category) {
            nodes {
              frontmatter {
                index
                title
              }
            }
            fieldValue
          }
        }
      }
    `
  )

  return (
    <section>
      <Heading isMain>Method guides</Heading>

      <TopSection>
        <StyledDescription maxWidth={60} isMain>
          The U+Method is a step-by-step product development methodology that
          focuses on front–loading the risky parts of product development before
          starting large build–outs.
        </StyledDescription>
        <Statistics>
          {statisticData.map(({ number, text }) => (
            <NumberContainer key={number}>
              <NumberStatistic number={number} text={text} />
            </NumberContainer>
          ))}
        </Statistics>
      </TopSection>

      <StagesContainer>
        {METHOD_STAGES.map(({ name, description }, index) => {
          const stageGroup = allMarkdownRemark.group.find(
            ({ fieldValue }) => fieldValue === name
          )

          return (
            <StageCard
              key={name}
              index={index}
              name={name}
              description={description}
              stageGroupNodes={stageGroup?.nodes}
            />
          )
        })}
      </StagesContainer>
    </section>
  )
}
