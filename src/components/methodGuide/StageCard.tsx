import React from 'react'
import styled from 'styled-components'
import Stage1 from '../../assets/images/pyramid1.svg'
import Stage2 from '../../assets/images/pyramid2.svg'
import Stage3 from '../../assets/images/pyramid3.svg'
import Stage4 from '../../assets/images/pyramid4.svg'
import { getGridColumn, getGridRow } from '../../utils/gridHelpers'
import {
  FONT_WEIGHT,
  SCREEN,
  OFFSET_POSITION_TOP,
  OFFSET_POSITION_LEFT,
  COLORS,
  FONT_SIZE,
  Z_INDEX,
  MEDIA_IE_HOVER,
} from '../../constants'
import { formatSlug } from '../../utils/formatSlug'
import { AnimatedLink } from '../AnimatedLink'
import { MethodGuide } from '../../types/MethodGuide'
import { usePageTransition } from '../../utils/usePageTransition'

const CardContainer = styled.div<{ column: number; row: number }>`
  position: relative;
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: stretch;
  width: 100%;
  max-width: 47rem;
  margin-bottom: 13rem;
  padding: 3rem;
  border: 3px solid ${COLORS.BLACK};
  background: ${COLORS.WHITE};
  cursor: pointer;

  :last-child {
    margin-bottom: 0;
  }

  svg {
    height: 8.5rem;
    width: 8.5rem;
  }

  ::before {
    content: '';
    position: absolute;
    top: -${OFFSET_POSITION_TOP.MOBILE}rem;
    left: -${OFFSET_POSITION_LEFT.MOBILE}rem;
    width: 100%;
    height: 100%;
    z-index: ${Z_INDEX.BACKGROUND};
    background: ${COLORS.YELLOW};
    transition: transform 0.2s ease-in;
  }

  ${SCREEN.ABOVE_MOBILE} {
    -ms-grid-column: ${({ column }) => column};
    -ms-grid-row: ${({ row }) => row};
    margin-bottom: 17rem;

    :nth-last-child(-n + 2) {
      margin-bottom: 0;
    }

    ::before {
      top: -${OFFSET_POSITION_TOP.DESKTOP}rem;
      left: -${OFFSET_POSITION_LEFT.DESKTOP}rem;
    }
  }

  ${MEDIA_IE_HOVER} {
    :hover {
      ::before {
        transform: translate(-10px, -10px);
      }
    }
  }
`

const Number = styled.span`
  position: absolute;
  top: -8.5rem;
  right: 2.5rem;
  font-weight: ${FONT_WEIGHT.MEDIUM};
  font-size: 13rem;

  ${SCREEN.ABOVE_MOBILE} {
    top: -12.5rem;
    font-size: 20rem;
  }
`

const Heading = styled.div`
  font-size: ${FONT_SIZE.LARGE};
  font-weight: ${FONT_WEIGHT.MEDIUM};
`

const Description = styled.p`
  margin: 0.5rem 0 2.7rem;
  letter-spacing: 0.05rem;
  font-size: ${FONT_SIZE.EXTRA_SMALL};
`

const TOC = styled.ol`
  padding-left: 1.8rem;
  font-size: ${FONT_SIZE.MEDIUM};
  font-weight: ${FONT_WEIGHT.MEDIUM};
  color: ${COLORS.BLUE};

  > li {
    margin-bottom: 0.5rem;
  }
`

const Chapter = styled(AnimatedLink)`
  position: relative;

  ::after {
    content: '';
    position: absolute;
    left: 0;
    bottom: 0;
    width: 100%;
    border-top: 2px solid blue;
    transform: scale(0, 0);
    transition: transform 0.2s;
  }

  ${MEDIA_IE_HOVER} {
    :hover {
      ::after {
        transform: scale(1, 1);
      }
    }
  }
`

const Dots = styled.div`
  font-size: ${FONT_SIZE.MEDIUM};
  font-weight: ${FONT_WEIGHT.MEDIUM};
`

const renderPyramid = (number: number) => {
  const pyramidName = `pyramid${number}`

  switch (pyramidName) {
    case 'pyramid1':
      return <Stage1 />
    case 'pyramid2':
      return <Stage2 />
    case 'pyramid3':
      return <Stage3 />
    case 'pyramid4':
    default:
      return <Stage4 />
  }
}

interface StageCardProps {
  name: string
  description: string
  index: number
  stageGroupNodes?: Array<MethodGuide>
}

export const StageCard: React.FC<StageCardProps> = ({
  index,
  name,
  description,
  stageGroupNodes,
}) => {
  const triggerTransition = usePageTransition(`/method/${formatSlug(name)}`)

  const isTOCLong = stageGroupNodes && stageGroupNodes.length > 7
  const stageGroupPreview = isTOCLong
    ? stageGroupNodes?.slice(0, 6)
    : stageGroupNodes

  return (
    <CardContainer
      column={getGridColumn(index, 2)}
      row={getGridRow(index, 2)}
      onClick={triggerTransition}
    >
      {renderPyramid(index + 1)}
      <Heading>{name}</Heading>
      <Description>{description}</Description>
      <Number>{index + 1}</Number>
      <TOC>
        {stageGroupPreview?.map(({ frontmatter: { title, index } }) => (
          <li key={title}>
            <Chapter
              to={`/method/${formatSlug(name)}`}
              state={{ index }}
              onClick={e => e.stopPropagation()}
            >
              {`${title}`}
            </Chapter>
          </li>
        ))}
      </TOC>
      {isTOCLong && <Dots>...</Dots>}
    </CardContainer>
  )
}
