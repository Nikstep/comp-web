import React from 'react'
import styled from 'styled-components'
import {
  COLORS,
  FONT_SIZE,
  FONT_WEIGHT,
  LINK_UNDERLINE,
  MEDIA_IE_HOVER,
  Z_INDEX,
  SCREEN,
  OFFSET_POSITION_LEFT,
  OFFSET_POSITION_TOP,
} from '../../constants'
import { isExternalLink } from '../../utils/isExternalLink'
import { AnimatedLink } from '../AnimatedLink'

const Heading = styled.div`
  font-size: ${FONT_SIZE.LARGE};
  font-weight: ${FONT_WEIGHT.MEDIUM};
  transition: color 0.1s ease-in;
`

const LinkContainer = styled.div`
  position: relative;
  padding: 0 0.5rem 0.5rem;
  margin-left: -0.5rem;
  text-align: center;
  font-size: ${FONT_SIZE.MEDIUM};
  font-weight: ${FONT_WEIGHT.MEDIUM};
  transition: color 0.1s ease-in;

  ${LINK_UNDERLINE.DEFAULT};
`

const Container = styled.div<{
  maxWidth?: number
  offsetColor: string
  isStretched?: boolean
}>`
  position: relative;
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  justify-content: stretch;
  width: 100%;
  max-width: ${({ maxWidth }) => (maxWidth ? `${maxWidth}rem` : '100%')};
  height: ${({ isStretched }) => (isStretched ? '100%' : 'auto')};
  padding: 3rem;
  border: 3px solid ${COLORS.BLACK};
  background: ${COLORS.WHITE};
  cursor: pointer;

  ::after {
    content: '';
    position: absolute;
    top: -${OFFSET_POSITION_TOP.MOBILE}rem;
    left: -${OFFSET_POSITION_LEFT.MOBILE}rem;
    width: 100%;
    height: 100%;
    z-index: ${Z_INDEX.BACKGROUND};
    background: ${({ offsetColor }) => offsetColor};
    transition: transform 0.2s ease-in;
  }

  ${SCREEN.ABOVE_MOBILE} {
    ::after {
      top: -${OFFSET_POSITION_TOP.DESKTOP}rem;
      left: -${OFFSET_POSITION_LEFT.DESKTOP}rem;
    }
  }

  ${MEDIA_IE_HOVER} {
    :hover {
      ::after {
        transform: translate(-10px, -10px);
      }

      ${Heading} {
        color: ${COLORS.BLUE};
        ${LINK_UNDERLINE.HOVER};
      }

      ${LinkContainer} {
        color: ${COLORS.BLUE};
        ${LINK_UNDERLINE.HOVER}
      }
    }
  }
`

const Tag = styled.div`
  margin-bottom: 0.5rem;
  color: ${COLORS.LIGHT_GREY};
  font-size: 1.2rem;
`

const Description = styled.p<{ isDescriptionNarrow: boolean }>`
  flex-grow: 1;
  margin: 0.5rem 0 2.7rem;
  letter-spacing: 0.05rem;
  font-size: ${FONT_SIZE.EXTRA_SMALL};

  ${SCREEN.ABOVE_MOBILE} {
    max-width: ${({ isDescriptionNarrow }) =>
      isDescriptionNarrow ? '75%' : '100%'};
  }
`

export interface CoreRectangleProps {
  heading: string
  description: string
  link: string
  linkText: string
  maxWidth?: number
  isStretched?: boolean
  tag?: string
}

interface RectangleProps extends CoreRectangleProps {
  color: string
  isDescriptionNarrow: boolean
  onClick?: () => void
}

export const Rectangle: React.FC<RectangleProps> = ({
  children,
  heading,
  description,
  link,
  linkText,
  color,
  isStretched,
  maxWidth,
  tag,
  isDescriptionNarrow,
  onClick,
}) => (
  <Container
    maxWidth={maxWidth}
    isStretched={isStretched}
    offsetColor={color}
    onClick={onClick}
  >
    {children}
    {tag && <Tag>{tag}</Tag>}
    <Heading>{heading}</Heading>
    <Description isDescriptionNarrow={isDescriptionNarrow}>
      {description}
    </Description>
    {isExternalLink(link) ? (
      <LinkContainer>{linkText}</LinkContainer>
    ) : (
      <LinkContainer>
        <AnimatedLink to={link}>{linkText}</AnimatedLink>
      </LinkContainer>
    )}
  </Container>
)
