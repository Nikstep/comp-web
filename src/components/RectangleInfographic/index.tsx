import React from 'react'

import { CoreRectangleProps, Rectangle } from './Rectangle'
import { COLORS } from '../../constants'
import { usePageTransition } from '../../utils/usePageTransition'
import { isExternalLink } from '../../utils/isExternalLink'

export interface RectangleInfographicProps extends CoreRectangleProps {
  color?: string
  isDescriptionNarrow?: boolean
}

export const RectangleInfographic: React.FC<RectangleInfographicProps> = ({
  children,
  heading,
  description,
  linkText,
  link,
  color = COLORS.YELLOW,
  isStretched,
  maxWidth,
  tag,
  isDescriptionNarrow = true,
}) => {
  const triggerTransition = usePageTransition(link)

  if (isExternalLink(link)) {
    return (
      <a href={link} rel="noreferrer noopener">
        <Rectangle
          heading={heading}
          description={description}
          link={link}
          linkText={linkText}
          color={color}
          isStretched={isStretched}
          maxWidth={maxWidth}
          tag={tag}
          isDescriptionNarrow={isDescriptionNarrow}
        >
          {children}
        </Rectangle>
      </a>
    )
  }

  return (
    <Rectangle
      heading={heading}
      description={description}
      link={link}
      linkText={linkText}
      color={color}
      isStretched={isStretched}
      maxWidth={maxWidth}
      tag={tag}
      isDescriptionNarrow={isDescriptionNarrow}
      onClick={triggerTransition}
    >
      {children}
    </Rectangle>
  )
}
