import React from 'react'
import styled from 'styled-components'
import Plus from '../assets/images/plus.svg'
import DownArrow from '../assets/images/down-arrow.svg'
import LeftArrow from '../assets/images/left-arrow.svg'
import UpArrow from '../assets/images/up-arrow.svg'
import List from '../assets/images/list.svg'
import { COLORS, MEDIA_IE_HOVER } from '../constants'
import { AnimatedLink } from './AnimatedLink'
import { isExternalLink } from '../utils/isExternalLink'

const StyledButton = styled.button`
  display: flex;
  justify-content: center;
  align-items: center;
  flex-shrink: 0;
  width: 5.4rem;
  height: 5.4rem;
  border-radius: 50%;
  background: ${COLORS.BLUE};
  transition: transform 0.1s ease-in;

  ${MEDIA_IE_HOVER} {
    :hover {
      transform: scale(1.1) translateZ(0px);
    }
  }
`

const renderIcon = (icon: string) => {
  switch (icon) {
    case 'upArrow':
      return <UpArrow />
    case 'downArrow':
      return <DownArrow />
    case 'leftArrow':
      return <LeftArrow />
    case 'list':
      return <List />
    case 'plus':
    default:
      return <Plus />
  }
}

export interface ButtonProps {
  icon?: 'plus' | 'upArrow' | 'downArrow' | 'leftArrow' | 'list'
  link?: string
  onClick?: () => void
  ariaLabel?: string
}

export const Button: React.FC<ButtonProps> = ({
  link,
  icon = 'plus',
  onClick,
  ariaLabel,
}) => {
  if (link) {
    if (isExternalLink(link)) {
      return (
        <a href={link} rel="noreferrer noopener" aria-label={ariaLabel}>
          <StyledButton aria-label={ariaLabel}>{renderIcon(icon)}</StyledButton>
        </a>
      )
    }

    return (
      <AnimatedLink to={link} aria-label={ariaLabel}>
        <StyledButton aria-label={ariaLabel}>{renderIcon(icon)}</StyledButton>
      </AnimatedLink>
    )
  }

  return (
    <StyledButton onClick={onClick} aria-label={ariaLabel}>
      {renderIcon(icon)}
    </StyledButton>
  )
}
