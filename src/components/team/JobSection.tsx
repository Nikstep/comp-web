import React, { forwardRef } from 'react'
import styled from 'styled-components'
import { useStaticQuery, graphql } from 'gatsby'
import { Heading } from '../Heading'
import { RectangleInfographic } from '../RectangleInfographic'
import { COLORS, SCREEN, IE } from '../../constants'
import { getGridColumn, getGridRow } from '../../utils/gridHelpers'
import { Jobs } from '../../types/Job'

const Container = styled.div`
  display: grid;
  display: -ms-grid;
  grid-gap: 5rem;
  margin-top: 6rem;

  ${SCREEN.ABOVE_MOBILE} {
    grid-template-columns: repeat(2, 1fr);
    -ms-grid-columns: 1fr 1fr;
    margin-top: 10rem;
  }

  ${SCREEN.ABOVE_TABLET} {
    grid-gap: 8rem;
  }

  ${SCREEN.ABOVE_LAPTOP} {
    grid-template-columns: repeat(3, 1fr);
    -ms-grid-columns: 1fr 1fr 1fr;
  }
`

const CardWrapper = styled.div<{
  column: number
  row: number
  tabletColumn: number
  tabletRow: number
}>`
  ${IE.DEFAULT} {
    margin: 0 0 5rem 5rem;
  }

  ${IE.ABOVE_TABLET} {
    margin: 0 0 8rem 8rem;
    -ms-grid-column: ${({ tabletColumn }) => tabletColumn};
    -ms-grid-row: ${({ tabletRow }) => tabletRow};
  }

  ${IE.ABOVE_LAPTOP} {
    -ms-grid-column: ${({ column }) => column};
    -ms-grid-row: ${({ row }) => row};
  }
`

export const JobSection = forwardRef<HTMLElement>((_props, ref) => {
  const { allStrapiJob } = useStaticQuery<Jobs>(graphql`
    query {
      allStrapiJob {
        nodes {
          Position
          Location
          Description
          link
        }
      }
    }
  `)

  return (
    <section ref={ref}>
      <Heading textAlign="center">Become one of us</Heading>
      <Container>
        {allStrapiJob.nodes.map(
          ({ Position, Description, Location, link }, index) => (
            <CardWrapper
              key={Position}
              tabletColumn={getGridColumn(index, 2)}
              column={getGridColumn(index, 3)}
              tabletRow={getGridRow(index, 2)}
              row={getGridRow(index, 3)}
            >
              <RectangleInfographic
                heading={Position}
                description={Description}
                link={link}
                isDescriptionNarrow={false}
                linkText="Apply now"
                color={COLORS.LIGHT_YELLOW}
                tag={Location}
                isStretched
              />
            </CardWrapper>
          )
        )}
      </Container>
    </section>
  )
})
