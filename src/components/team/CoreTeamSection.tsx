import React from 'react'
import styled from 'styled-components'
import { useStaticQuery, graphql } from 'gatsby'
import { Heading } from '../Heading'
import { VerticalButton } from '../VerticalButton'
import { FONT_SIZE, FONT_WEIGHT, COLORS, SCREEN } from '../../constants'
import { People } from '../../types/Person'
import { SectionDescription } from '../SectionDescription'

const FlexContainer = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: flex-start;
`

const TeamContainer = styled.div`
  display: flex;
  flex-wrap: wrap;
  justify-content: space-between;

  ${SCREEN.ABOVE_MOBILE} {
    justify-content: flex-start;
  }

  ${SCREEN.ABOVE_TABLET} {
    margin-top: 4rem;
  }
`

const TeamMemberContainer = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  align-items: center;
  width: 50%;
  margin-top: 3rem;
  max-width: 14rem;
  text-align: center;

  ${SCREEN.ABOVE_MOBILE} {
    width: auto;
    margin: 5rem 2rem 0;
    flex: 0 0 17.5%;

    :nth-child(-n + 2) {
      margin-top: 5rem;
    }
  }

  ${SCREEN.ABOVE_TABLET} {
    margin-top: 8rem;

    :nth-child(-n + 2) {
      margin-top: 8rem;
    }
  }
`

const PhotoContainer = styled.div`
  height: 17rem;
  width: 100%;
  margin-bottom: 2rem;
`

const Photo = styled.img`
  height: 100%;
`

const Name = styled.div`
  font-size: ${FONT_SIZE.LARGE};
  font-weight: ${FONT_WEIGHT.MEDIUM};
  line-height: 2.3rem;
  width: 100%;
  word-spacing: 100vw;
`

const Position = styled.div`
  margin: 0.5rem 0 0.3rem 0;
  font-size: ${FONT_SIZE.SMALL};
  font-weight: ${FONT_WEIGHT.MEDIUM};
  color: ${COLORS.LIGHT_GREY};
`

const LinkedinLink = styled.a`
  font-size: ${FONT_SIZE.EXTRA_SMALL};
  color: ${COLORS.BLUE};
`

interface CoreTeamProps {
  scrollToNextSection: () => void
}

export const CoreTeamSection: React.FC<CoreTeamProps> = ({
  scrollToNextSection,
}) => {
  const { allStrapiPerson } = useStaticQuery<People>(
    graphql`
      query {
        allStrapiPerson(sort: { fields: priority }) {
          nodes {
            id
            name
            position
            linkedin
            photo {
              publicURL
            }
          }
        }
      }
    `
  )

  const { nodes } = allStrapiPerson

  return (
    <section>
      <FlexContainer>
        <div>
          <Heading isMain>Core Team</Heading>
          <SectionDescription maxWidth={70} isMain>
            We are former CEOs, Product Managers, Backend Developers, DevOps
            Engineers focused on helping new products and companies grow. Join a
            great international team and work with amazing companies operating
            in the US and EU.
          </SectionDescription>
        </div>
        <VerticalButton icon="downArrow" onClick={scrollToNextSection}>
          Join the team
        </VerticalButton>
      </FlexContainer>
      <TeamContainer>
        {nodes.map(({ name, position, photo, linkedin }) => (
          <TeamMemberContainer key={name}>
            <PhotoContainer>
              <Photo src={`${photo.publicURL}`} alt={`${name} photo`} />
            </PhotoContainer>
            <Name>{name}</Name>
            <Position>{position}</Position>
            <LinkedinLink href={linkedin}>LinkedIn</LinkedinLink>
          </TeamMemberContainer>
        ))}
      </TeamContainer>
    </section>
  )
}
