import React from 'react'
import styled from 'styled-components'
import TeamSetupDesktop from '../../assets/images/team-setup-desktop.svg'
import TeamSetupMobile from '../../assets/images/team-setup-mobile.svg'
import Logo from '../../assets/images/logo.svg'
import { Heading } from '../Heading'
import { SectionDescription } from '../SectionDescription'
import { SCREEN, FONT_WEIGHT, COLORS } from '../../constants'

const Container = styled.section``

const SchemaContainer = styled.div`
  position: relative;

  > svg {
    width: 100%;

    :nth-child(2) {
      display: none;
    }
  }

  ${SCREEN.ABOVE_MOBILE} {
    margin: -25rem 0;

    > svg {
      position: relative;
      left: 5rem;
      display: block;
      width: 40rem;
      margin: 0 auto;

      :first-child {
        display: none;
      }

      :nth-child(2) {
        display: block;
      }
    }
  }

  ${SCREEN.ABOVE_LAPTOP} {
    margin: -10rem 0;

    > svg {
      left: 8rem;
      width: 60rem;
    }
  }
`

const TextContainer = styled.div`
  position: absolute;
  top: 16rem;
  display: none;
  justify-content: space-between;
  width: 100%;
  padding: 0 calc((100% - 120rem) / 2);
  font-size: 1.4rem;

  > div {
    flex: 50%;
  }

  svg {
    width: 4rem;
    height: 3rem;
    margin-bottom: 3rem;
  }

  > :last-child {
    text-align: right;
  }

  ::before {
    content: '';
    position: absolute;
    top: 5rem;
    width: 100%;
    max-width: 120rem;
    border-top: 1px dashed ${COLORS.GREY};
  }

  ${SCREEN.ABOVE_TABLET} {
    display: flex;
  }

  ${SCREEN.ABOVE_LAPTOP} {
    top: 22rem;
    font-size: initial;
  }
`

const ClientHeading = styled.p`
  margin-bottom: 3rem;
  font-size: 3rem;
  font-weight: ${FONT_WEIGHT.MEDIUM};
  line-height: 3rem;
`

export const TeamSetupSection: React.FC = () => (
  <Container>
    <Heading subheading="How we work together" textAlign="center">
      Team Setup
    </Heading>
    <SectionDescription>
      Team of international venture builders who have built, launched and
      mentored products in Fintech, Insurtech, Telco, Automotive, E-health and
      Energy sectors.
    </SectionDescription>

    <SchemaContainer>
      <TeamSetupMobile />
      <TeamSetupDesktop />

      <TextContainer>
        <div>
          <Logo />
          <p>Product Definition</p>
          <p>Market analysis</p>
          <p>Value proposition validation</p>
          <p>Business model creation</p>
          <p>Product roadmap creation</p>
          <p>Backlog management</p>
          <p>Product roadmap consulting</p>
          <p>Technical solution creation</p>
          <p>Technical arch. and system design</p>
          <p>Technical Project Management</p>
          <p>User Experience and user testing</p>
          <p>User Acceptance Testing</p>
        </div>

        <div>
          <ClientHeading>Client</ClientHeading>
          <p>Sponsor/Investor communication</p>
          <p>Legal/Compliance viability</p>
          <p>Day–to–Day business operations</p>
          <p>Marketing, Sales, Customer Care</p>
        </div>
      </TextContainer>
    </SchemaContainer>
  </Container>
)
