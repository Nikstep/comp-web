import React from 'react'
import styled from 'styled-components'

const List = styled.ul`
  list-style: none;
`

const ListItem = styled.li`
  display: flex;
  padding: 0 0 0.5rem 2rem;

  ::before {
    flex-shrink: 0;
    content: '−';
    margin: 0 1.25rem 0 -2rem;
  }
`

interface Item {
  list_item: string
  id: number
}

interface UnorderedListProps {
  list: Array<string | Item>
  className?: string
}

export const UnorderedList: React.FC<UnorderedListProps> = ({
  list,
  className,
}) => (
  <List className={className}>
    {list.map((item, index: number) => {
      if (typeof item === 'object') {
        return (
          <ListItem key={item.id}>
            <span>{item.list_item}</span>
          </ListItem>
        )
      }

      return <ListItem key={index}>{item}</ListItem>
    })}
  </List>
)
