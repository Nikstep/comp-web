import styled from 'styled-components'
import { FONT_SIZE, COLORS, SCREEN } from '../constants'

export const Article = styled.article`
  padding-top: 7rem;

  :not(:last-child) {
    margin-bottom: 10rem;

    ::after {
      content: '';
      position: relative;
      top: 8.5rem;
      display: block;
      width: 16rem;
      height: 0.1rem;
      margin: 0 auto;
      background: ${COLORS.BLUE};
    }
  }

  h2 {
    margin-bottom: 2rem;
    font-size: ${FONT_SIZE.EXTRA_LARGE};
    line-height: 5rem;
  }

  h3 {
    margin: 5rem 0 2rem;
    font-size: 3rem;
    line-height: 1.3;
  }

  h4 {
    margin: 3rem 0 1rem;
    line-height: 1.5;
  }

  ul,
  ol {
    margin: 1rem 0 2.5rem;
    padding-left: 2.5rem;
    font-size: ${FONT_SIZE.MEDIUM};

    li > ul {
      margin: 0;
    }

    li > p {
      margin-bottom: 0;
      padding-bottom: 0.7rem;
    }

    > li {
      padding-bottom: 0.75rem;
      padding-left: 0.75rem;
      line-height: 1.375;

      :last-child {
        padding-bottom: 0;
      }
    }
  }

  a {
    text-decoration: underline;
    word-break: break-all;
  }

  p {
    font-size: ${FONT_SIZE.MEDIUM};
    margin-bottom: 2.5rem;
  }

  div {
    font-size: ${FONT_SIZE.MEDIUM};
    line-height: 1.7;
  }

  ${SCREEN.ABOVE_MOBILE} {
    p,
    div,
    ul,
    ol {
      font-size: 2.1rem;
    }
  }
`
