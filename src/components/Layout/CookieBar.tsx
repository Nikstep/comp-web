import React, { useEffect, useState } from 'react'
import { createPortal } from 'react-dom'
import styled from 'styled-components'
import Cookies from 'js-cookie'
import {
  COLORS,
  Z_INDEX,
  FONT_SIZE,
  FONT_WEIGHT,
  MEDIA_IE_HOVER,
  MAX_CONTENT_WIDTH,
  PADDING,
  SCREEN,
} from '../../constants'

const Container = styled.div<{ isVisible: boolean }>`
  position: fixed;
  bottom: 0;
  left: 0;
  display: ${({ isVisible }) => (isVisible ? 'block' : 'none')};
  width: 100%;
  height: 20rem;
  padding: 1.5rem 0;
  background: ${COLORS.WHITE};
  border-top: 1.5px solid ${COLORS.BLACK};
  font-size: ${FONT_SIZE.MEDIUM};
  z-index: ${Z_INDEX.ABOVE};

  ${SCREEN.ABOVE_MOBILE} {
    height: 8rem;
  }
`

const ContentContainer = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: space-around;
  width: 100%;
  height: 100%;
  padding: 0 ${PADDING.MOBILE}rem;
  max-width: ${MAX_CONTENT_WIDTH}rem;
  margin: 0 auto;

  ${SCREEN.ABOVE_MOBILE} {
    flex-direction: row;
    justify-content: center;
  }

  ${SCREEN.ABOVE_TABLET} {
    padding: 0 ${PADDING.DESKTOP}rem;
  }
`

const Text = styled.p`
  text-align: center;

  ${SCREEN.ABOVE_MOBILE} {
    margin-right: 3rem;
    text-align: left;
  }
`

const ButtonContainer = styled.div`
  display: flex;
`

const RectangleButton = styled.button<{ isInverted?: boolean }>`
  padding: 1.2rem 2rem;
  margin: 0 1rem;
  text-transform: uppercase;
  font-size: ${FONT_SIZE.SMALL};
  font-weight: ${FONT_WEIGHT.MEDIUM};
  color: ${({ isInverted }) => (isInverted ? COLORS.BLUE : COLORS.WHITE)};
  background: ${({ isInverted }) => (isInverted ? COLORS.WHITE : COLORS.BLUE)};
  border: 1.5px solid
    ${({ isInverted }) => (isInverted ? COLORS.BLACK : COLORS.BLUE)};
  transition: background 0.1s ease-in;

  ${MEDIA_IE_HOVER} {
    :hover {
      background: ${({ isInverted }) =>
        isInverted ? '#edecec' : `${COLORS.BLUE}99`};
    }
  }
`

interface WindowWithGA extends Window {
  gaOptout?: () => void
  disableStr?: string
}

export const CookieBar = () => {
  const [isVisible, setIsVisible] = useState(false)
  const COOKIE_NAME = 'consentCookie'

  useEffect(() => {
    const consentCookie = Cookies.get(COOKIE_NAME)
    const { disableStr } = window as WindowWithGA
    const gaDisableCookie = disableStr ? Cookies.get(disableStr) : false

    if (!consentCookie) {
      setIsVisible(true)
    }

    if (consentCookie === 'false' && !gaDisableCookie) {
      const { gaOptout } = window as WindowWithGA

      gaOptout && gaOptout()
    }
  }, [])

  const setCookie = (isAccepted: 'true' | 'false') => {
    Cookies.set(COOKIE_NAME, isAccepted, { expires: 365 })
    setIsVisible(false)

    if (isAccepted === 'false') {
      const { gaOptout } = window as WindowWithGA

      gaOptout && gaOptout()
    }
  }

  const cookieBarElement = (
    <Container isVisible={isVisible}>
      <ContentContainer>
        <Text>
          We use cookies to track the website usage and user preferences.
        </Text>
        <ButtonContainer>
          <RectangleButton onClick={() => setCookie('true')}>
            Okay
          </RectangleButton>
          <RectangleButton onClick={() => setCookie('false')} isInverted>
            Disable
          </RectangleButton>
        </ButtonContainer>
      </ContentContainer>
    </Container>
  )

  const portal =
    typeof document !== `undefined` && document.getElementById('cookieBar')

  if (portal) {
    return createPortal(cookieBarElement, portal)
  }

  return null
}
