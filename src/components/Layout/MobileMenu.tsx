import React from 'react'
import styled from 'styled-components'
import CloseIcon from '../../assets/images/close.svg'
import Logo from '../../assets/images/logo.svg'
import {
  COLORS,
  PADDING,
  FONT_WEIGHT,
  FONT_SIZE,
  SCREEN,
  Z_INDEX,
} from '../../constants'
import { AnimatedLink } from '../AnimatedLink'
import { ROUTES } from './Header'
import { MAIN_EMAIL } from '../../constants/contacts'
import { createPortal } from 'react-dom'

const Container = styled.div<{ isMenuOpen: boolean }>`
  position: fixed;
  top: 0;
  left: 0;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  width: 100vw;
  height: ${({ isMenuOpen }) => (isMenuOpen ? '100vh' : '0')};
  padding: 0 ${PADDING.MOBILE}rem;
  background: ${COLORS.WHITE};
  transition: height 0.5s ease-in-out;
  transition-delay: 0.2s;
  overflow: hidden;
  z-index: ${Z_INDEX.ABOVE};

  > :not(:first-child) {
    opacity: ${({ isMenuOpen }) => (isMenuOpen ? 1 : 0)};
    transition: opacity 0.3s ease-out;
    transition-delay: ${({ isMenuOpen }) => (isMenuOpen ? '0.7s' : '0')};
  }

  ${SCREEN.ABOVE_MOBILE} {
    display: none;
  }
`

const TopContainer = styled.div`
  display: flex;
  justify-content: flex-end;
  align-items: center;
  height: 8rem;
  min-height: 8rem;
`

const LogoLink = styled(AnimatedLink)<{ isScrollUp: boolean }>`
  position: fixed;
  top: 2.25rem;
  left: ${PADDING.MOBILE}rem;
  width: 5.5rem;
  height: 3.5rem;
  transform: ${({ isScrollUp }) =>
    !isScrollUp ? 'translateY(-8rem)' : 'translateY(0)'};
  transition: transform 300ms cubic-bezier(0.45, 0.05, 0.55, 0.95);
  z-index: 3;

  ${SCREEN.ABOVE_MOBILE} {
    display: none;
  }
`

const CloseIconContainer = styled.div<{ isMenuOpen: boolean }>`
  height: 3.5rem;
  opacity: ${({ isMenuOpen }) => (isMenuOpen ? 1 : 0)};
  transition: ${({ isMenuOpen }) =>
    isMenuOpen ? 'opacity 0.5s linear 0.3s' : 'opacity 0.3s linear 0.1s'};
`

const Navigation = styled.nav`
  text-align: center;
`

const StyledLink = styled(AnimatedLink)`
  position: relative;
  display: block;
  padding: 1rem;
  font-size: ${FONT_SIZE.EXTRA_LARGE};
  font-weight: ${FONT_WEIGHT.MEDIUM};
`

const Info = styled.div`
  font-size: ${FONT_SIZE.MEDIUM};
`

const Address = styled.div`
  margin-bottom: 3rem;
`

const Contacts = styled.div`
  display: flex;
  justify-content: space-between;
  margin-bottom: ${PADDING.MOBILE}rem;
  font-size: ${FONT_SIZE.MEDIUM};
  color: ${COLORS.BLUE};
`

interface MobileMenuProps {
  isMenuOpen: boolean
  isScrollUp: boolean
  closeMenu: () => void
}

export const MobileMenu: React.FC<MobileMenuProps> = ({
  isMenuOpen,
  closeMenu,
  isScrollUp,
}) => {
  const elements = (
    <>
      <LogoLink to="/" aria-label="Homepage" isScrollUp={isScrollUp}>
        <Logo />
      </LogoLink>
      <Container isMenuOpen={isMenuOpen}>
        <TopContainer>
          <CloseIconContainer onClick={closeMenu} isMenuOpen={isMenuOpen}>
            <CloseIcon />
          </CloseIconContainer>
        </TopContainer>

        <Navigation>
          {ROUTES.map(({ path, name }) => (
            <StyledLink key={name} to={path}>
              {name}
            </StyledLink>
          ))}
        </Navigation>

        <Info>
          <Address>
            44 Tehama St.
            <br />
            San Francisco, CA 94105
          </Address>

          <Contacts>
            <a href={`mailto:${MAIN_EMAIL}`}>{MAIN_EMAIL}</a>
            <a href="tel:+1-347-200-7553">+1 (347) 200-7553</a>
          </Contacts>
        </Info>
      </Container>
    </>
  )

  const portalEl =
    typeof document !== `undefined` && document.getElementById('menu')

  if (portalEl) {
    return createPortal(elements, portalEl)
  }

  return null
}
