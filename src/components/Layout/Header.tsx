import React, { useEffect, useRef, useState } from 'react'
import styled, { css } from 'styled-components'
import { throttle } from 'throttle-debounce'
import { useLocation } from '@reach/router'
import {
  COLORS,
  FONT_SIZE,
  FONT_WEIGHT,
  SCREEN,
  MAX_CONTENT_WIDTH,
  MEDIA_IE_HOVER,
  Z_INDEX,
  HEADER_HEIGHT,
  PADDING,
} from '../../constants'
import Logo from '../../assets/images/logo.svg'
import { MAIN_EMAIL } from '../../constants/contacts'
import { AnimatedLink } from '../AnimatedLink'
import { MobileMenu } from './MobileMenu'
import MenuIcon from '../../assets/images/menu.svg'
import {
  enableBodyScroll,
  disableBodyScroll,
  clearAllBodyScrollLocks,
} from 'body-scroll-lock'

const Container = styled.header<{ isScrollUp: boolean }>`
  position: fixed;
  left: 50%;
  display: flex;
  justify-content: space-between;
  align-items: center;
  width: 100%;
  max-width: ${MAX_CONTENT_WIDTH}rem;
  height: ${HEADER_HEIGHT}rem;
  margin: 0 auto;
  padding: 0 ${PADDING.MOBILE}rem;
  background: ${COLORS.WHITE};
  transform: ${({ isScrollUp }) =>
    !isScrollUp ? 'translate(-50%, -8rem)' : 'translateX(-50%)'};
  transition: transform 300ms cubic-bezier(0.45, 0.05, 0.55, 0.95);
  z-index: ${Z_INDEX.HEADER};

  ${SCREEN.ABOVE_TABLET} {
    padding: 0 ${PADDING.DESKTOP}rem;
  }
`

const LeftContainer = styled.div`
  display: flex;
  align-items: center;
`

const DesktopNavigation = styled.nav`
  display: none;

  ${SCREEN.ABOVE_MOBILE} {
    display: block;
  }
`

const LogoLink = styled(AnimatedLink)`
  display: none;
  width: 5.5rem;
  height: 3.5rem;
  margin-right: 3rem;

  ${MEDIA_IE_HOVER} {
    :hover {
      svg {
        fill: ${COLORS.BLUE};
      }
    }
  }

  ${SCREEN.ABOVE_MOBILE} {
    display: block;
    margin-right: 4rem;
  }
`

// eslint-disable-next-line
const StyledLink = styled(({ isActive, ...props }) => (
  <AnimatedLink {...props} />
))<{
  isActive: boolean
}>`
  position: relative;
  padding: 0.5rem;
  font-size: ${FONT_SIZE.MEDIUM};
  font-weight: ${FONT_WEIGHT.MEDIUM};
  color: ${({ isActive }) => (isActive ? COLORS.BLUE : COLORS.BLACK)};

  :not(:last-child) {
    margin-right: 3rem;
  }

  ::after {
    content: '';
    position: absolute;
    bottom: 0;
    left: 0;
    width: 100%;
    height: 1rem;
    border-bottom: 2px solid blue;
    opacity: 0;
    transform: translateY(-0.7rem);
    transition: opacity 0.2s, transform 0.2s;

    ${({ isActive }) =>
      isActive &&
      css`
        opacity: 1;
        transform: translateY(0);
      `}
  }

  ${MEDIA_IE_HOVER} {
    :hover {
      ::after {
        opacity: 1;
        transform: translateY(0);
      }
    }
  }

  ${SCREEN.ABOVE_MOBILE} {
    :not(:last-child) {
      margin-right: 5rem;
    }
  }
`

const RightContainer = styled.div`
  display: none;
  font-size: ${FONT_SIZE.EXTRA_SMALL};

  ${SCREEN.ABOVE_TABLET} {
    display: flex;
  }
`

const Contact = styled.div`
  margin-right: 4rem;
  color: ${COLORS.BLUE};
  font-weight: ${FONT_WEIGHT.MEDIUM};

  ${MEDIA_IE_HOVER} {
    a {
      :hover {
        opacity: 0.6;
      }
    }
  }
`

const Address = styled.div`
  color: ${COLORS.GREY};
`

const MenuIconContainer = styled.div<{
  isWithStripe: boolean
  isMenuOpen: boolean
}>`
  opacity: ${({ isMenuOpen }) => (isMenuOpen ? 0 : 1)};
  transform: ${({ isWithStripe }) => isWithStripe && 'translateX(-4.5rem)'};
  transition: transform 0.2s ease-in, opacity 0.2s;
  transition-delay: 0s, ${({ isMenuOpen }) => (isMenuOpen ? '0s' : '0.8s')};

  ${SCREEN.ABOVE_MOBILE} {
    display: none;
  }
`

export const ROUTES = [
  { name: 'Team', path: '/team' },
  { name: 'Work', path: '/work' },
  { name: 'Method', path: '/method' },
  { name: 'Research', path: '/research' },
]

export const Header: React.FC = () => {
  const [isMenuOpen, setIsMenuOpen] = useState(false)
  const [isScrollUp, setScrollUp] = useState(true)
  const location = useLocation()
  const [isWithStripe, setIsWithStripe] = useState(location.pathname === '/')

  const headerRef = useRef<HTMLElement>(null)
  const previousPosition = useRef(0)

  const handleScroll = throttle(300, () => {
    const currentOffsetY = window.pageYOffset

    if (currentOffsetY < 100) {
      setScrollUp(true)
    } else if (previousPosition.current > currentOffsetY) {
      setScrollUp(true)
    } else if (previousPosition.current < currentOffsetY && !isMenuOpen) {
      setScrollUp(false)
    }

    if (location.pathname === '/') {
      if (currentOffsetY > 10) {
        setIsWithStripe(false)
      } else {
        setIsWithStripe(true)
      }
    }

    previousPosition.current = currentOffsetY
  })

  useEffect(() => {
    window.addEventListener('scroll', handleScroll)

    return () => {
      window.removeEventListener('scroll', handleScroll)
    }
  }, [isMenuOpen])

  useEffect(
    () => () => {
      if (headerRef.current) {
        clearAllBodyScrollLocks()
      }
    },
    []
  )

  const isPartiallyCurrent = (path: string) =>
    location.pathname.startsWith(path)

  const handleOpenMenu = () => {
    setIsMenuOpen(true)

    if (headerRef.current) {
      disableBodyScroll(headerRef.current)
    }
  }

  const handleCloseMenu = () => {
    setIsMenuOpen(false)

    if (headerRef.current) {
      enableBodyScroll(headerRef.current)
    }
  }

  return (
    <Container isScrollUp={isScrollUp} ref={headerRef}>
      <LeftContainer>
        <LogoLink to="/" aria-label="Homepage">
          <Logo />
        </LogoLink>

        <DesktopNavigation>
          {ROUTES.map(({ path, name }) => (
            <StyledLink
              key={name}
              to={path}
              isActive={isPartiallyCurrent(path)}
            >
              {name}
            </StyledLink>
          ))}
        </DesktopNavigation>
      </LeftContainer>

      <RightContainer>
        <Contact>
          <a href={`mailto:${MAIN_EMAIL}`}>{MAIN_EMAIL}</a>
          <br />
          <a href="tel:+1-347-200-7553">+1 (347) 200-7553</a>
        </Contact>
        <Address>
          44 Tehama St.
          <br />
          San Francisco, CA 94105
        </Address>
      </RightContainer>

      <MenuIconContainer
        onClick={handleOpenMenu}
        isWithStripe={isWithStripe}
        isMenuOpen={isMenuOpen}
      >
        <MenuIcon />
      </MenuIconContainer>

      <MobileMenu
        isMenuOpen={isMenuOpen}
        closeMenu={handleCloseMenu}
        isScrollUp={isScrollUp}
      />
    </Container>
  )
}
