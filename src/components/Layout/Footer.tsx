import React from 'react'
import styled from 'styled-components'
import { OutboundLink } from 'gatsby-plugin-google-analytics'
import {
  COLORS,
  SCREEN,
  FONT_WEIGHT,
  FONT_SIZE,
  MAX_CONTENT_WIDTH,
  MEDIA_IE_HOVER,
  Z_INDEX,
  PADDING,
} from '../../constants'
import Linkedin from '../../assets/images/linkedin.svg'
import {
  MAIN_EMAIL,
  SF_EMAIL,
  PRG_EMAIL,
  NY_EMAIL,
} from '../../constants/contacts'

const Container = styled.footer`
  display: flex;
  flex-direction: column;
  width: 100%;
  max-width: ${MAX_CONTENT_WIDTH}rem;
  margin: 0 auto;

  ${SCREEN.ABOVE_TABLET} {
    flex-direction: row;
    height: 33rem;
  }
`

const LeftContainer = styled.div`
  position: relative;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  flex-basis: 40%;
  height: 30rem;
  padding: 3rem ${PADDING.MOBILE}rem;
  background: ${COLORS.BLUE};
  color: ${COLORS.WHITE};
  font-weight: ${FONT_WEIGHT.MEDIUM};

  ${SCREEN.ABOVE_TABLET} {
    flex-basis: 40%;
    height: auto;
    padding: 5rem ${PADDING.DESKTOP}rem;
    background: none;
  }
`

const EmailWrapper = styled.div`
  font-size: ${FONT_SIZE.EXTRA_LARGE};
  line-height: 1;
`

const Mail = styled.a`
  text-decoration: underline;

  ${MEDIA_IE_HOVER} {
    :hover {
      opacity: 0.7;
    }
  }
`

const Social = styled(OutboundLink)`
  display: inline-block;
  margin-top: 2.5rem;

  ${MEDIA_IE_HOVER} {
    :hover {
      opacity: 0.7;
    }
  }
`

const Copyright = styled.p`
  font-size: ${FONT_SIZE.EXTRA_SMALL};
  font-weight: ${FONT_WEIGHT.MEDIUM};
`

const RightContainer = styled.div`
  display: flex;
  justify-content: space-between;
  flex-grow: 1;
  align-items: center;
  padding: 0;

  ${SCREEN.ABOVE_TABLET} {
    padding: ${PADDING.DESKTOP}rem;
  }
`

const Contacts = styled.div`
  display: none;
  justify-content: space-between;
  align-items: flex-start;
  width: 100%;

  ${SCREEN.ABOVE_TABLET} {
    display: flex;
  }
`

const Contact = styled.div`
  align-self: center;
  text-align: center;

  :not(:last-child) {
    margin-bottom: 4rem;
  }

  ${SCREEN.ABOVE_TABLET} {
    align-self: unset;
    text-align: unset;

    :not(:last-child) {
      margin-bottom: 0;
    }
  }
`

const Address = styled.div`
  margin: 2rem 0;
`

const ContactMail = styled.a`
  color: ${COLORS.BLUE};
  font-size: ${FONT_SIZE.MEDIUM};
  font-weight: ${FONT_WEIGHT.MEDIUM};

  ${MEDIA_IE_HOVER} {
    :hover {
      opacity: 0.6;
    }
  }
`

const Background = styled.div`
  position: absolute;
  right: 0;
  top: 0;
  width: 60vw;
  height: 100%;
  background: ${COLORS.BLUE};
  z-index: ${Z_INDEX.BACKGROUND};
`

export const Footer: React.FC = () => {
  const currentYear = new Date().getFullYear()

  return (
    <Container>
      <LeftContainer>
        <div>
          <EmailWrapper>
            <p>Say Hello at U+</p>
            <Mail href={`mailto:${MAIN_EMAIL}`}>{MAIN_EMAIL}</Mail>
          </EmailWrapper>

          <Social
            aria-label="LinkedIn"
            href="https://www.linkedin.com/company/user-technologies-s-r-o-/"
            rel="noreferrer noopener"
          >
            <Linkedin />
          </Social>
        </div>

        <Copyright>Copyright © {currentYear} U Plus inc.</Copyright>
        <Background />
      </LeftContainer>

      <RightContainer>
        <Contacts>
          <Contact>
            <h4>San Francisco</h4>
            <Address>
              <p>44 Tehama Street,</p>
              <p>San Francisco, CA 94105</p>
              <p>United States</p>
            </Address>

            <ContactMail href={`mailto:${SF_EMAIL}`}>{SF_EMAIL}</ContactMail>
          </Contact>

          <Contact>
            <h4>New York</h4>
            <Address>
              <p>175 Varick Street,</p>
              <p>New York, NY 10014</p>
              <p>United States</p>
            </Address>

            <ContactMail href={`mailto:${NY_EMAIL}`}>{NY_EMAIL}</ContactMail>
          </Contact>

          <Contact>
            <h4>Prague</h4>
            <Address>
              <p>Plynární 1617/10,</p>
              <p>Prague 7, 170 00</p>
              <p>Czech Republic</p>
            </Address>

            <ContactMail href={`mailto:${PRG_EMAIL}`}>{PRG_EMAIL}</ContactMail>
          </Contact>
        </Contacts>
      </RightContainer>
    </Container>
  )
}
