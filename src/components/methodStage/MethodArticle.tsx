import React from 'react'
import { MethodGuide } from '../../types/MethodGuide'
import { formatSlug } from '../../utils/formatSlug'
import { Article } from '../Article'

interface ArticleProps extends MethodGuide {
  isTitleHidden?: boolean
}

export const MethodArticle: React.FC<ArticleProps> = ({
  html,
  frontmatter,
  isTitleHidden,
}) => (
  <Article id={formatSlug(frontmatter.title)}>
    {!isTitleHidden && (
      <>
        <h2>
          {frontmatter.index}. {frontmatter.title}
        </h2>
        {frontmatter.description && (
          <div
            dangerouslySetInnerHTML={{
              __html: frontmatter.description,
            }}
          />
        )}
      </>
    )}

    <div
      key={frontmatter.title}
      dangerouslySetInnerHTML={{
        __html: html,
      }}
    />
  </Article>
)
