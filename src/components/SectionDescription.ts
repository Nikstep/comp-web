import styled, { css } from 'styled-components'
import { FONT_SIZE, SCREEN } from '../constants'

export const SectionDescription = styled.p<{
  maxWidth?: number
  isMain?: boolean
}>`
  ${({ isMain, maxWidth }) =>
    isMain
      ? css`
          max-width: ${maxWidth}rem;
          font-size: 2rem;

          ${SCREEN.ABOVE_MOBILE} {
            font-size: ${FONT_SIZE.LARGE};
          }
        `
      : css`
          margin: 0 auto 4rem;
          max-width: 46rem;
          font-size: ${FONT_SIZE.MEDIUM};
          text-align: center;

          ${SCREEN.ABOVE_TABLET} {
            margin: 0 auto 8rem;
          }

          ${SCREEN.ABOVE_LAPTOP} {
            margin: 0 auto 14rem;
          }
        `};
`
