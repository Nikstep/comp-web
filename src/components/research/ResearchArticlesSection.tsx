import React from 'react'
import styled from 'styled-components'
import { useStaticQuery, graphql } from 'gatsby'
import { RectangleInfographic } from '../RectangleInfographic'
import { COLORS, SCREEN, IE } from '../../constants'
import { getGridColumn, getGridRow } from '../../utils/gridHelpers'
import { Researches } from '../../types/Research'
import { formatSlug } from '../../utils/formatSlug'

const Container = styled.div`
  display: grid;
  display: -ms-grid;
  margin-top: 6rem;

  ${SCREEN.ABOVE_MOBILE} {
    -ms-grid-columns: 1fr 1fr;
    margin-top: 5rem;
  }

  ${SCREEN.ABOVE_LAPTOP} {
    -ms-grid-columns: 1fr 1fr 1fr;
  }

  @supports (display: grid) {
    grid-gap: 5rem;

    ${SCREEN.ABOVE_MOBILE} {
      grid-template-columns: repeat(2, 1fr);
    }

    ${SCREEN.ABOVE_TABLET} {
      grid-gap: 8rem;
    }

    ${SCREEN.ABOVE_LAPTOP} {
      grid-template-columns: repeat(3, 1fr);
    }
  }
`

const CardWrapper = styled.div<{
  column: number
  row: number
  tabletColumn: number
  tabletRow: number
}>`
  ${IE.DEFAULT} {
    margin: 0 0 5rem 5rem;
  }

  ${IE.ABOVE_TABLET} {
    margin: 0 0 8rem 8rem;
    -ms-grid-column: ${({ tabletColumn }) => tabletColumn};
    -ms-grid-row: ${({ tabletRow }) => tabletRow};
  }

  ${IE.ABOVE_LAPTOP} {
    -ms-grid-column: ${({ column }) => column};
    -ms-grid-row: ${({ row }) => row};
  }
`

const Cover = styled.div<{ url: string }>`
  width: calc(100% + 6rem);
  height: 19rem;
  margin: -3rem -3rem 2rem;
  background: ${({ url }) => `url(${url})`};
  background-repeat: no-repeat;
  background-position: center;
  background-size: cover;
`

interface ResearchesQueryProps extends Researches {
  file: {
    childImageSharp: {
      fixed: {
        src: string
      }
    }
  }
}

export const ResearchArticlesSection: React.FC = () => {
  const { allMarkdownRemark, file } = useStaticQuery<
    ResearchesQueryProps
  >(graphql`
    query {
      allMarkdownRemark(
        sort: { fields: frontmatter___date }
        filter: { frontmatter: { type: { eq: "Research" } } }
      ) {
        nodes {
          frontmatter {
            title
            description
            date
            type
            coverImage {
              childImageSharp {
                fixed(jpegQuality: 100, width: 500) {
                  src
                }
              }
            }
          }
          id
        }
      }
      file(relativePath: { eq: "card_default.jpg" }) {
        childImageSharp {
          fixed(jpegQuality: 100, width: 500) {
            src
          }
        }
      }
    }
  `)

  return (
    <Container>
      {allMarkdownRemark.nodes.map(
        (
          { id, frontmatter: { title, description, date, coverImage } },
          index
        ) => {
          const coverUrl = coverImage
            ? coverImage?.childImageSharp.fixed.src
            : file.childImageSharp.fixed.src

          return (
            <CardWrapper
              key={id}
              tabletColumn={getGridColumn(index, 2)}
              column={getGridColumn(index, 3)}
              tabletRow={getGridRow(index, 2)}
              row={getGridRow(index, 3)}
            >
              <RectangleInfographic
                heading={title}
                description={description}
                link={`/research/${formatSlug(title)}`}
                isDescriptionNarrow={false}
                linkText="Read the research"
                color={COLORS.LIGHT_YELLOW}
                tag={new Date(date).getFullYear().toString()}
                isStretched
                maxWidth={36.3}
              >
                <Cover url={coverUrl} />
              </RectangleInfographic>
            </CardWrapper>
          )
        }
      )}
    </Container>
  )
}
