import styled from 'styled-components'
import { SCREEN } from '../constants'

export const ArticlesContainer = styled.div`
  ${SCREEN.ABOVE_LAPTOP} {
    width: 83rem;
    margin: 0 auto;
    padding-left: 15vw;
  }

  @media (min-width: 1500px) {
    padding-left: 10rem;
  }
`
