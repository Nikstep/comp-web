import React from 'react'
import styled from 'styled-components'
import { Button, ButtonProps } from './Button'
import {
  COLORS,
  FONT_SIZE,
  FONT_WEIGHT,
  SCREEN,
  MEDIA_IE_HOVER,
} from '../constants'

const ButtonContainer = styled.div`
  display: none;
  flex-direction: column;
  justify-content: flex-end;
  align-items: center;
  cursor: pointer;

  ${SCREEN.ABOVE_LAPTOP} {
    display: flex;
  }

  ${MEDIA_IE_HOVER} {
    :hover {
      button {
        transform: scale(1.1) translateZ(0px);
      }
    }
  }
`
const Line = styled.div`
  width: 0.1rem;
  height: 6.6rem;
  background: ${COLORS.BLACK};
`

const Text = styled.div`
  margin: 1rem 0 1rem 0;
  font-size: ${FONT_SIZE.BASE};
  font-weight: ${FONT_WEIGHT.MEDIUM};
  writing-mode: tb;
  -ms-writing-mode: tb-lr;
  transform: rotate(-180deg);
`

interface VerticalButtonProps extends ButtonProps {
  className?: string
}

export const VerticalButton: React.FC<VerticalButtonProps> = ({
  children,
  link,
  onClick,
  icon,
  className,
}) => (
  <ButtonContainer onClick={onClick} className={className}>
    <Line />
    <Text>{children}</Text>
    <Button link={link} icon={icon} />
  </ButtonContainer>
)
