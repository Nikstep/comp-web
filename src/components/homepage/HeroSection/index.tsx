import React from 'react'
import styled, { keyframes, css } from 'styled-components'
import { COLORS, SCREEN, FONT_SIZE, HEADER_HEIGHT } from '../../../constants'
import { Strip } from './Strip'
import { VerticalButton } from '../../VerticalButton'

const countryReveal = keyframes`
  from {
    transform: translateX(-250px);
    opacity: 0;
  }

  to {
    transform: translateY(0);
    opacity: 1;
  }
`

const headingReveal = keyframes`
  from {
    opacity: 0;
  }

  to {
    opacity: 1;
  }
`

const generateDelays = (amount: number) => {
  const selectors = []

  for (let i = 0; i <= amount; i++) {
    selectors.push(css`
      > :nth-child(${amount - i}) {
        animation-delay: ${1.5 + 0.04 * i}s;
      }
    `)
  }

  return selectors
}

const TABLET_FONT_SIZE = 7.5
const LAPTOP_FONT_SIZE = 8.5
const DESKTOP_FONT_SIZE = 10

const Container = styled.section`
  margin-bottom: -8rem;
  padding-top: 0;

  ${SCREEN.ABOVE_MOBILE} {
    padding-top: 1.5rem;
  }
`

const Content = styled.div<{ isReadyToRender: boolean }>`
  display: ${({ isReadyToRender }) => (isReadyToRender ? 'flex' : 'none')};
  flex-direction: column;
  justify-content: flex-end;
  height: calc(100vh - ${HEADER_HEIGHT}rem);
  padding: 0 2rem 7rem 0;

  ${SCREEN.ABOVE_MOBILE} {
    padding: 0 0 14rem 0;
  }

  ${SCREEN.ABOVE_TABLET} {
    padding: 0 0 ${DESKTOP_FONT_SIZE}rem 0;

    @media (orientation: landscape) {
      padding: 0 0 14rem 0;
    }
  }
`

const Countries = styled.div<{ amount: number }>`
  width: 9.2rem;
  ${({ amount }) => generateDelays(amount)}

  ${SCREEN.ABOVE_MOBILE} {
    @media (orientation: landscape) {
      display: none;
    }
  }

  ${SCREEN.ABOVE_TABLET} {
    display: block;
  }
`

const Country = styled.p`
  color: ${COLORS.LIGHT_GREY};
  font-size: 1.3rem;
  line-height: 1.6rem;
  transform: translateX(-250px);
  animation: ${countryReveal} 1s cubic-bezier(0.22, 0.61, 0.36, 1) both;

  ${SCREEN.ABOVE_MOBILE} {
    font-size: ${FONT_SIZE.EXTRA_SMALL};
    line-height: 2rem;
  }
`

const HeadingContainer = styled.div`
  display: flex;
  justify-content: space-between;
`

const Heading = styled.h1`
  margin-top: 2rem;
  color: ${COLORS.BLACK};
  animation: ${headingReveal} 1s ease-in;

  ${SCREEN.ABOVE_MOBILE} {
    font-size: ${TABLET_FONT_SIZE}rem;

    @media (orientation: landscape) {
      font-size: 5rem;
    }
  }

  ${SCREEN.ABOVE_TABLET} {
    white-space: pre;
    font-size: ${LAPTOP_FONT_SIZE}rem;
  }

  ${SCREEN.ABOVE_LAPTOP} {
    font-size: ${DESKTOP_FONT_SIZE}rem;
    font-size: min(${DESKTOP_FONT_SIZE}rem, 8vw);
  }
`

const Highlight = styled.span`
  cursor: pointer;
  color: ${COLORS.BLUE};
`

const COUNTRIES = [
  'US',
  'Italy',
  'China',
  'France',
  'Germany',
  'Denmark',
  'Vietnam',
  'Switzerland',
  'South Africa',
  'Czech Republic',
]

interface HeroSectionProps {
  scrollToNextSection: () => void
  scrollToMethod: () => void
  scrollToCases: () => void
  transitionStatus: string
}

export const HeroSection: React.FC<HeroSectionProps> = ({
  scrollToNextSection,
  scrollToMethod,
  scrollToCases,
  transitionStatus,
}) => (
  <Container>
    <Content
      isReadyToRender={
        transitionStatus === 'entered' || transitionStatus === 'exiting'
      }
    >
      <Countries amount={COUNTRIES.length}>
        {COUNTRIES.map(country => (
          <Country key={country}>{country}</Country>
        ))}
      </Countries>

      <HeadingContainer>
        <Heading>
          We help Fortune 1000s {'\n'}
          turn <Highlight onClick={scrollToMethod}>ideas</Highlight> into
          successful
          {'\n'}
          <Highlight onClick={scrollToCases}>products</Highlight>
        </Heading>
        <VerticalButton icon={'downArrow'} onClick={scrollToNextSection}>
          Scroll down
        </VerticalButton>
      </HeadingContainer>
    </Content>

    <Strip isReadyToRender={transitionStatus === 'entered'} />
  </Container>
)
