import React from 'react'
import styled, { css } from 'styled-components'
import LeftArrow from '../../../assets/images/arrow-left-aside-banded.svg'

import RightArrow from '../../../assets/images/arrow-right-aside-banded.svg'
import VerticalArrow from '../../../assets/images/arrow-vertical.svg'

import {
  COLORS,
  FONT_SIZE,
  OFFSET_POSITION_LEFT,
  OFFSET_POSITION_TOP,
  SCREEN,
  Z_INDEX,
  ANIMATIONS,
  TRANSLATE_DISTANCE,
} from '../../../constants'
import { Stage } from '../../../constants/methodStages'

import { UnorderedList } from '../../UnorderedList'
import { BASE_ANIMATION_DELAY } from '.'

const VerticalArrowContainer = styled.div<{ isOnMobile?: boolean }>`
  position: absolute;
  bottom: -7.5rem;
  left: 47%;
  display: ${({ isOnMobile }) => !isOnMobile && 'none'};

  ${SCREEN.ABOVE_MOBILE} {
    display: block;
  }

  ${SCREEN.ABOVE_TABLET} {
    display: none;
  }
`

const Card = styled.div<{
  offsetColor?: string
  index: number
}>`
  position: relative;
  max-width: 100%;
  margin: 0 auto 7rem;
  animation: ${ANIMATIONS.FADE_UP(TRANSLATE_DISTANCE)} 1s both
    cubic-bezier(0.25, 0.46, 0.45, 0.94);
  animation-delay: ${({ index }) => index * BASE_ANIMATION_DELAY}ms;
  z-index: ${({ index }) => -index};

  ::before {
    content: '';
    position: absolute;
    top: -${OFFSET_POSITION_TOP.MOBILE}rem;
    left: -${OFFSET_POSITION_LEFT.MOBILE}rem;
    width: 100%;
    height: 100%;
    background-color: ${({ offsetColor }) => offsetColor};
    z-index: ${Z_INDEX.BACKGROUND};

    ${SCREEN.ABOVE_MOBILE} {
      top: -${OFFSET_POSITION_TOP.DESKTOP}rem;
      left: -${OFFSET_POSITION_LEFT.DESKTOP}rem;
    }
  }

  ${SCREEN.ABOVE_MOBILE} {
    width: 33rem;
    margin: 0 0 7rem auto;
  }

  ${SCREEN.ABOVE_LAPTOP} {
    margin: 0 auto 7rem;
    width: 36rem;

    :last-child {
      margin-bottom: 4.6rem;
    }
  }

  :last-child {
    ${VerticalArrowContainer} {
      ${SCREEN.ABOVE_MOBILE} {
        display: none;
      }
    }
  }
`

const Content = styled.div`
  min-height: 18rem;
  padding: 1.6rem 1.8rem;
  border: 0.3rem solid ${COLORS.BLACK};
  background-color: ${COLORS.WHITE};

  ${SCREEN.ABOVE_LAPTOP} {
    padding: 2.1rem 2.4rem;
  }
`

const SubHeader = styled.p`
  color: ${COLORS.LIGHT_GREY};
  font-size: 1.2rem;
  line-height: ${FONT_SIZE.MEDIUM};
`

const Description = styled.p`
  max-width: 33rem;
  font-size: ${FONT_SIZE.EXTRA_SMALL};
  line-height: 2.2rem;
`

const Header = styled.h3`
  margin-bottom: 1.4rem;
  font-size: 2.2rem;
  line-height: 2.2rem;
`

const Body = styled.div`
  padding-left: 1.8rem;
`

const StyledUnorderedList = styled(UnorderedList)`
  font-size: ${FONT_SIZE.EXTRA_SMALL};
  line-height: 2.2rem;
`

const ArrowContainer = styled.div<{ positioning: 'left' | 'right' }>`
  display: none;

  ${SCREEN.ABOVE_TABLET} {
    display: block;
    position: absolute;
    top: 7.4rem;

    ${({ positioning }) =>
      positioning === 'left'
        ? css`
            left: -4.3rem;
            transform: scale(0.95) translateX(0.25rem);
          `
        : css`
            right: -4.3rem;
            transform: scale(0.95) translateX(-0.25rem);
          `};
  }

  ${SCREEN.ABOVE_LAPTOP} {
    top: 50%;
  }
`

const renderArrow = (arrow?: 'left' | 'right') => {
  switch (arrow) {
    case 'left':
      return <LeftArrow />
    case 'right':
      return <RightArrow />
  }
}

interface MethodStageProps extends Stage {
  index: number
}

export const MethodStage: React.FC<MethodStageProps> = ({
  index,
  stage,
  header,
  body,
  offsetColor,
  arrow,
  mobileArrow,
}) => (
  <Card offsetColor={offsetColor} index={index}>
    <Content>
      <SubHeader>{`Stage ${stage}`}</SubHeader>

      <Header>{header}</Header>

      <Body>
        {Array.isArray(body) ? (
          <StyledUnorderedList list={body} />
        ) : (
          <Description>{body}</Description>
        )}
      </Body>
    </Content>

    {arrow && (
      <ArrowContainer positioning={arrow}>{renderArrow(arrow)}</ArrowContainer>
    )}

    <VerticalArrowContainer isOnMobile={mobileArrow}>
      <VerticalArrow />
    </VerticalArrowContainer>
  </Card>
)
