import React, { forwardRef, useRef } from 'react'
import styled from 'styled-components'
import { stages } from '../../../constants/methodStages'
import { SCREEN } from '../../../constants'

import { VerticalButton } from '../../VerticalButton'
import Dots from '../../../assets/images/dots.svg'
import { Heading } from '../../Heading'
import { SectionDescription } from '../../SectionDescription'
import { MethodStage } from './MethodStage'
import { StagesSummary } from './StagesSummary'
import { useIsInView } from '../../../utils/useIsInView'

export const BASE_ANIMATION_DELAY = 300

const FirstDescription = styled(SectionDescription)`
  margin-bottom: 2rem;

  ${SCREEN.ABOVE_TABLET} {
    margin-bottom: 3.5rem;
  }
`

const SecondDescription = styled(SectionDescription)`
  ${SCREEN.ABOVE_MOBILE} {
    margin-bottom: 6rem;
  }

  ${SCREEN.ABOVE_TABLET} {
    margin-bottom: 15rem;
  }
`

const Section = styled.section`
  position: relative;
`

const Stages = styled.div<{ isInView: boolean }>`
  padding-top: 1rem;
  width: 100%;
  min-height: 100rem;

  > div {
    display: ${({ isInView }) => !isInView && 'none'};
  }

  ${SCREEN.ABOVE_MOBILE} {
    padding-top: 12rem;
  }

  ${SCREEN.ABOVE_TABLET} {
    padding-top: 3rem;
  }
`

const StyledVerticalButton = styled(VerticalButton)`
  position: absolute;
  right: 0;
  bottom: 0;
`

const DotsContainer = styled.div`
  display: flex;
  justify-content: center;
  margin: 0 auto;
`

interface UPlusMethodProps {
  scrollToNextSection: () => void
}

export const UPlusMethod = forwardRef<HTMLElement, UPlusMethodProps>(
  ({ scrollToNextSection }, ref) => {
    const containerRef = useRef<HTMLDivElement>(null)

    const isInView = useIsInView(containerRef, 50)

    return (
      <Section ref={ref}>
        <Heading subheading="The U+Method" textAlign="center">
          How it&#39;s done
        </Heading>

        <FirstDescription>
        The U+Method is a step-by-step product development method that 
        focuses on front-loading the risky parts of product development before starting large builds. 
        </FirstDescription>
        
        <SecondDescription>
        As we progress in the U+Method, the risk of scaling goes down while market metrics support our business case.
        </SecondDescription>

        <Stages ref={containerRef} isInView={isInView}>
          {stages.map((stage, index) =>
            stage.isAside ? (
              <StagesSummary
                key={stage.header}
                index={index}
                header={stage.header}
                body={stage.body}
                positioning={stage.positioning}
                icon={stage.icon}
                techIcons={stage.techIcons}
              />
            ) : (
              <MethodStage
                key={stage.header}
                index={index}
                stage={stage.stage}
                header={stage.header}
                body={stage.body}
                offsetColor={stage.offsetColor}
                positioning={stage.positioning}
                arrow={stage.arrow}
                mobileArrow={stage.mobileArrow}
              />
            )
          )}
        </Stages>

        <StyledVerticalButton icon="downArrow" onClick={scrollToNextSection}>
          See our results
        </StyledVerticalButton>

        <DotsContainer>
          <Dots />
        </DotsContainer>
      </Section>
    )
  }
)
