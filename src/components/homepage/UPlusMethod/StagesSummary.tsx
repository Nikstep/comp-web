import React from 'react'
import styled from 'styled-components'
import Pyramid1 from '../../../assets/images/pyramid1.svg'
import Pyramid2 from '../../../assets/images/pyramid2.svg'
import Pyramid3 from '../../../assets/images/pyramid3.svg'
import Pyramid4 from '../../../assets/images/pyramid4.svg'
import Dot from '../../../assets/images/dot.svg'
import {
  FONT_SIZE,
  SCREEN,
  ANIMATIONS,
  TRANSLATE_DISTANCE,
  PADDING,
  MAX_CONTENT_WIDTH,
  LINK_UNDERLINE,
  MEDIA_IE_HOVER,
  FONT_WEIGHT,
  COLORS,
} from '../../../constants'
import { Stage } from '../../../constants/methodStages'
import { TechIcons } from './TechIcons'
import { BASE_ANIMATION_DELAY } from '.'
import { AnimatedLink } from '../../AnimatedLink'
import { formatSlug } from '../../../utils/formatSlug'

type Positioning = 'left' | 'right'

const Card = styled.div<{
  float?: Positioning
  index: number
}>`
  margin-bottom: 4rem;
  font-size: ${FONT_SIZE.MEDIUM};
  line-height: ${FONT_SIZE.LARGE};
  animation: ${ANIMATIONS.FADE_UP(TRANSLATE_DISTANCE)} 1s both
    cubic-bezier(0.25, 0.46, 0.45, 0.94);
  animation-delay: ${({ index }) => index * BASE_ANIMATION_DELAY}ms;

  ${SCREEN.ABOVE_MOBILE} {
    float: left;
    max-width: 31rem;
    margin-bottom: 0;
    margin-top: -10.5rem;
  }

  ${SCREEN.ABOVE_TABLET} {
    max-width: 35rem;
  }

  ${SCREEN.ABOVE_LAPTOP} {
    float: ${({ float }) => float};
    margin-top: -14.7rem;
    text-align: ${({ float }) => float};
  }
`

const DividerContainer = styled.div<{ margin?: 'left' | 'right' }>`
  position: relative;
  margin-bottom: 1.6rem;

  ${SCREEN.ABOVE_LAPTOP} {
    transform-origin: left;
    transform: ${({ margin }) =>
      margin === 'right' && 'rotate(180deg) translateX(-35rem)'};
  }
`

const Divider = styled.hr`
  border-top: 1px dashed rgba(0, 0, 0, 0.5);

  ${SCREEN.ABOVE_MOBILE} {
    width: calc(50vw - ${PADDING.DESKTOP}rem);
    max-width: ${MAX_CONTENT_WIDTH / 2 - PADDING.DESKTOP}rem;
  }
`

const DotContainer = styled.span`
  display: none;
  position: absolute;
  left: calc(50vw - ${PADDING.DESKTOP + 0.5}rem);
  bottom: -1rem;

  ${SCREEN.ABOVE_MOBILE} {
    display: block;
  }
`

const Description = styled.p`
  max-width: 31rem;
  font-size: ${FONT_SIZE.MEDIUM};
  line-height: ${FONT_SIZE.LARGE};
  padding-bottom: 2rem;

  ${SCREEN.ABOVE_TABLET} {
    max-width: 35rem;
  }
`

const HeadingContainer = styled.div`
  ${SCREEN.ABOVE_MOBILE} {
    display: flex;
    justify-content: flex-end;
    align-items: stretch;
    flex-direction: row-reverse;

    svg {
      margin-left: 2rem;
    }
  }

  ${SCREEN.ABOVE_LAPTOP} {
    display: block;

    svg {
      margin-left: 0;
    }
  }
`

const Heading = styled.h3`
  font-size: 3rem;
  line-height: 4rem;
`

const LinkContainer = styled.div<{ positioning?: Positioning }>`
  display: inline-block;
  position: relative;
  margin-left: -0.5rem;
  padding: 0 0.5rem 0.5rem;
  font-size: ${FONT_SIZE.MEDIUM};
  font-weight: ${FONT_WEIGHT.MEDIUM};
  cursor: pointer;
  transition: color 0.1s ease-in;

  ${LINK_UNDERLINE.DEFAULT};

  ${SCREEN.ABOVE_TABLET} {
    display: inline-block;
    margin: ${({ positioning }) =>
      positioning === 'left' ? '0 0 2.5rem -0.5rem' : '0 -0.5rem 2.5rem 0'};
  }

  ${MEDIA_IE_HOVER} {
    :hover {
      color: ${COLORS.BLUE};
      ${LINK_UNDERLINE.HOVER};
    }
  }
`

const renderIcon = (icon?: string) => {
  switch (icon) {
    case 'pyramid1':
      return <Pyramid1 />
    case 'pyramid2':
      return <Pyramid2 />
    case 'pyramid3':
      return <Pyramid3 />
    case 'pyramid4':
      return <Pyramid4 />
  }
}

interface StageSummaryProps extends Stage {
  index: number
}

export const StagesSummary: React.FC<StageSummaryProps> = ({
  index,
  header,
  body,
  positioning,
  icon,
  techIcons,
}) => (
  <Card float={positioning} index={index}>
    <HeadingContainer>
      {renderIcon(icon)}

      <Heading>{header}</Heading>
    </HeadingContainer>
    <DividerContainer margin={positioning}>
      <Divider />
      <DotContainer>
        <Dot />
      </DotContainer>
    </DividerContainer>

    {Array.isArray(body) ? (
      body.map((el, index) => <Description key={index}>{el}</Description>)
    ) : (
      <Description>{body}</Description>
    )}
    <LinkContainer positioning={positioning}>
      <AnimatedLink to={`/method/${formatSlug(header)}`}>
        Read more
      </AnimatedLink>
    </LinkContainer>

    {techIcons && <TechIcons techIcons={techIcons} positioning={positioning} />}
  </Card>
)
