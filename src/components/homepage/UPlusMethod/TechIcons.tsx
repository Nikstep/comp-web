import React from 'react'
import styled from 'styled-components'
import AWSLogo from '../../../assets/images/aws-logo.svg'
import NodeJsLogo from '../../../assets/images/nodejs-logo.svg'
import ReactLogo from '../../../assets/images/react-logo.svg'
import { COLORS, FONT_SIZE, SCREEN } from '../../../constants'
import { TechLogo } from '../../../constants/methodStages'

export const Container = styled.div<{
  positioning?: 'left' | 'right'
}>`
  display: flex;
  margin-top: 1rem;

  ${SCREEN.ABOVE_MOBILE} {
    margin-top: 0;
  }

  ${SCREEN.ABOVE_LAPTOP} {
    margin-top: 2rem;
    justify-content: ${({ positioning }) =>
      positioning === 'right' && 'flex-end'};
  }
`
export const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;

  :not(:last-child) {
    margin-right: 2.5rem;
  }

  ${SCREEN.ABOVE_LAPTOP} {
    :not(:last-child) {
      margin-right: 3.5rem;
    }
  }
`
export const Icon = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  margin-bottom: 1.5rem;
  width: 5.3rem;
  height: 3.2rem;

  ${SCREEN.ABOVE_MOBILE} {
    height: 4.2rem;
    width: 100%;
  }

  ${SCREEN.ABOVE_LAPTOP} {
    width: 6.2rem;
    height: 6.2rem;
  }
`
export const IconTitle = styled.span`
  margin-top: auto;
  font-size: 1.2rem;
  line-height: ${FONT_SIZE.MEDIUM};
  color: ${COLORS.LIGHT_GREY};
`
export const renderFooterIcon = (icon: TechLogo) => {
  switch (icon.name) {
    case 'aws':
      return <AWSLogo />
    case 'react':
      return <ReactLogo />
    case 'nodejs':
      return <NodeJsLogo />
  }
}

interface TechIconsProps {
  techIcons: TechLogo[]
  positioning?: 'left' | 'right'
}

export const TechIcons: React.FC<TechIconsProps> = ({
  techIcons,
  positioning,
}) => (
  <Container positioning={positioning}>
    {techIcons.map(icon => (
      <Wrapper key={icon.name}>
        <Icon>{renderFooterIcon(icon)}</Icon>
        <IconTitle>{icon.title}</IconTitle>
      </Wrapper>
    ))}
  </Container>
)
