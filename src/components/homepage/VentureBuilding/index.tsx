import React, { forwardRef } from 'react'
import styled from 'styled-components'
import { Heading } from '../../Heading'
import { NumberStatistic } from '../../NumberStatistic'
import { MethodInfographic } from './MethodInfographic'
import { FONT_WEIGHT, COLORS, SCREEN, MEDIA_IE_HOVER } from '../../../constants'
import { CoreTeam } from './CoreTeam'
import { VerticalButton } from '../../VerticalButton'
import { Statistic } from '../../../types/Statistic'
import { SectionDescription } from '../../SectionDescription'

const Statistics = styled.div`
  display: grid;
  grid-template-columns: repeat(2, 1fr);
  grid-row-gap: 5rem;
  margin: 4rem 0 4rem 1rem;
  max-width: 60rem;

  ${SCREEN.ABOVE_MOBILE} {
    display: -ms-grid;
    grid-template-columns: repeat(4, 1fr);
    -ms-grid-columns: 1fr 1fr 1fr 1fr;
  }
`

export const FlexContainer = styled.div`
  display: flex;
  align-items: stretch;
  justify-content: space-between;
  flex-direction: column;

  ${SCREEN.ABOVE_TABLET} {
    flex-direction: row;
  }
`

const StyledNumberStatistic = styled(NumberStatistic)<{ column: number }>`
  -ms-grid-column: ${({ column }) => column};
`

const TextHighlight = styled.span`
  display: block;
  color: ${COLORS.BLUE};
  font-weight: ${FONT_WEIGHT.MEDIUM};
  cursor: pointer;

  ${MEDIA_IE_HOVER} {
    :hover {
      text-decoration: underline;
    }
  }
`

const statisticData: Array<Statistic> = [
  {
    number: '$1B',
    text: ['Value', 'created'],
  },
  {
    number: '70+',
    text: ['Products', 'developed'],
  },
  {
    number: '10',
    text: ['Years on', 'the market'],
  },
  {
    number: '3',
    text: ['San Francisco', 'New York', 'Prague'],
  },
]

interface VentureBuildingProps {
  scrollToNextSection: () => void
}

export const VentureBuilding = forwardRef<HTMLElement, VentureBuildingProps>(
  ({ scrollToNextSection }, ref) => (
    <section ref={ref}>
      <Heading>
        Product Development & <br />
        Venture Building
      </Heading>

      <FlexContainer>
        <div>
          <Statistics>
            {statisticData.map(({ number, text }, index) => (
              <StyledNumberStatistic
                key={number}
                number={number}
                text={text}
                column={index + 1}
              />
            ))}
          </Statistics>
          <SectionDescription maxWidth={60} isMain>
            We focus on creating new experiences for existing or new customers
            using a standardized product development process. We typically work
            with Corporate Innovation Labs or Corporate VCs or Startups
            directly, delivering
            <TextHighlight onClick={scrollToNextSection}>
              Experience & Mindset, Process, Resources.
            </TextHighlight>
          </SectionDescription>
        </div>
        <MethodInfographic />
      </FlexContainer>

      <FlexContainer>
        <CoreTeam />
        <VerticalButton icon="downArrow" onClick={scrollToNextSection}>
          See our method
        </VerticalButton>
      </FlexContainer>
    </section>
  )
)
