import React from 'react'
import styled from 'styled-components'
import Stage1 from '../../../assets/images/pyramid1.svg'
import Stage2 from '../../../assets/images/pyramid2.svg'
import Stage3 from '../../../assets/images/pyramid3.svg'
import Stage4 from '../../../assets/images/pyramid4.svg'
import { SCREEN } from '../../../constants'
import { RectangleInfographic } from '../../RectangleInfographic'

const Container = styled.div`
  margin: 8rem 0 5rem 0rem;

  ${SCREEN.ABOVE_MOBILE} {
    margin: 8rem 0 0 4rem;
  }

  ${SCREEN.ABOVE_LAPTOP} {
    margin: 1.5rem 0 0;
  }
`

const Stages = styled.div`
  display: flex;
  flex-wrap: wrap;
  justify-content: center;
  width: 12rem;
  margin: 0 auto;
  padding: 2.5rem 0 3rem;

  ${SCREEN.ABOVE_MOBILE} {
    display: block;
    width: auto;
    padding: 2.5rem 5.5rem 3rem;
  }
`

export const MethodInfographic: React.FC = () => (
  <Container>
    <RectangleInfographic
      heading="U+Method guides"
      description="A step-by-step resource for building a product that takes you from the inception of ideas to handling the future growth of a company."
      link="/method"
      linkText="Check out the guides"
      maxWidth={38}
    >
      <Stages>
        <Stage1 />
        <Stage2 />
        <Stage3 />
        <Stage4 />
      </Stages>
    </RectangleInfographic>
  </Container>
)
