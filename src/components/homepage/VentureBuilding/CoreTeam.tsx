import React from 'react'
import styled from 'styled-components'
import { useStaticQuery, graphql } from 'gatsby'
import { People } from '../../../types/Person'
import {
  FONT_SIZE,
  FONT_WEIGHT,
  LINK_UNDERLINE,
  COLORS,
  MEDIA_IE_HOVER,
  SCREEN,
} from '../../../constants'
import { AnimatedLink } from '../../AnimatedLink'

const Container = styled.div`
  max-width: 52rem;

  ${SCREEN.ABOVE_MOBILE} {
    margin-top: 7.5rem;
  }
`

const PeopleContainer = styled.div`
  margin-top: 3rem;
`

const PersonContainer = styled.div`
  display: flex;
  align-items: center;
  line-height: 1.8rem;

  :not(:last-child) {
    margin-bottom: 2.5rem;
  }
`

const PhotoContainer = styled.div`
  width: 5rem;
  min-width: 5rem;
  height: 5rem;
  margin-right: 1rem;
  border-radius: 50%;
  overflow: hidden;
`

const Photo = styled.img`
  width: 100%;
`

const Name = styled.div`
  font-size: ${FONT_SIZE.MEDIUM};
  font-weight: ${FONT_WEIGHT.MEDIUM};
  margin-bottom: 0.3rem;
`

const Description = styled.div`
  font-size: ${FONT_SIZE.SMALL};
  line-height: 2.2rem;
`

const StyledLink = styled(AnimatedLink)`
  display: inline-block;
  position: relative;
  margin-top: 3.5rem;
  padding: 0 0.5rem 0.5rem;
  font-size: ${FONT_SIZE.MEDIUM};
  font-weight: ${FONT_WEIGHT.MEDIUM};
  transition: color 0.1s ease-in;

  ${LINK_UNDERLINE.DEFAULT};

  ${MEDIA_IE_HOVER} {
    :hover {
      color: ${COLORS.BLUE};
      ${LINK_UNDERLINE.HOVER};
    }
  }
`

export const CoreTeam: React.FC = () => {
  const { allStrapiPerson } = useStaticQuery<People>(
    graphql`
      query {
        allStrapiPerson(
          filter: { shortDescription: { ne: null } }
          sort: { fields: priority }
        ) {
          nodes {
            name
            id
            shortDescription
            photo {
              publicURL
            }
          }
        }
      }
    `
  )

  const { nodes } = allStrapiPerson

  return (
    <Container>
      <h3>Experienced Core Team</h3>
      <PeopleContainer>
        {nodes.map(({ name, id, shortDescription, photo }) => (
          <PersonContainer key={id}>
            <PhotoContainer>
              <Photo src={`${photo.publicURL}`} alt={`${name} photo`} />
            </PhotoContainer>
            <div>
              <Name>{name}</Name>
              <Description>{shortDescription}</Description>
            </div>
          </PersonContainer>
        ))}
      </PeopleContainer>

      <StyledLink to="/team">Read on about our team</StyledLink>
    </Container>
  )
}
