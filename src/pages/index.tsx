import React, { useRef } from 'react'
import { PageProps } from 'gatsby'
import SEO from '../components/Seo'
import { Layout } from '../components/Layout'
import { HeroSection } from '../components/homepage/HeroSection'
import { UPlusMethod } from '../components/homepage/UPlusMethod'
import { CaseStudySection } from '../components/work/CaseStudySection'
import { VentureBuilding } from '../components/homepage/VentureBuilding'
import { scrollToElement } from '../utils/scrollToElement'

export interface AnimatedPageProps<DataType = object, PageContextType = object>
  extends PageProps<DataType, PageContextType> {
  transitionStatus: string
}

const IndexPage: React.FC<AnimatedPageProps> = ({ transitionStatus }) => {
  const ventureRef = useRef<HTMLElement>(null)
  const uPlusMethodRef = useRef<HTMLElement>(null)
  const caseStudiesRef = useRef<HTMLElement>(null)

  return (
    <Layout>
      <SEO title="Venture Building & Product Development" />

      <HeroSection
        scrollToNextSection={() => scrollToElement(ventureRef.current)}
        scrollToMethod={() => scrollToElement(uPlusMethodRef.current)}
        scrollToCases={() => scrollToElement(caseStudiesRef.current)}
        transitionStatus={transitionStatus}
      />
      <VentureBuilding
        ref={ventureRef}
        scrollToNextSection={() => scrollToElement(uPlusMethodRef.current)}
      />
      <UPlusMethod
        ref={uPlusMethodRef}
        scrollToNextSection={() => scrollToElement(caseStudiesRef.current)}
      />
      <CaseStudySection ref={caseStudiesRef} />
    </Layout>
  )
}

export default IndexPage
