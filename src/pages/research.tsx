import React from 'react'
import styled from 'styled-components'

import SEO from '../components/Seo'
import { FloatingText } from '../components/FloatingText'
import { Heading } from '../components/Heading'
import { Layout } from '../components/Layout'
import { SectionDescription } from '../components/SectionDescription'
import { ResearchArticlesSection } from '../components/research/ResearchArticlesSection'

const FlexContainer = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: flex-start;
`

const description =
  'We regularly conduct research in verticals of our active customers to inform the value propositions we are testing as well as possible new products to introduce.'

const ResearchPage: React.FC = () => (
  <Layout>
    <SEO title="Research" description={description} />

    <section>
      <FlexContainer>
        <div>
          <Heading isMain>Research</Heading>
          <SectionDescription maxWidth={70} isMain>
            {description}
          </SectionDescription>
        </div>
        <FloatingText>Read on</FloatingText>
      </FlexContainer>
    </section>

    <ResearchArticlesSection />
  </Layout>
)

export default ResearchPage
