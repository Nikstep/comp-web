import React, { useRef } from 'react'
import { Layout } from '../components/Layout'
import SEO from '../components/Seo'
import { JobSection } from '../components/team/JobSection'
import { CoreTeamSection } from '../components/team/CoreTeamSection'
import { TeamSetupSection } from '../components/team/TeamSetupSection'
import { scrollToElement } from '../utils/scrollToElement'

const TeamPage: React.FC = () => {
  const jobRef = useRef<HTMLElement>(null)

  return (
    <Layout>
      <SEO
        title="Our Team"
        description="We are former CEOs, Product Managers, Backend Developers, DevOps Engineers focused on helping new products and companies grow. Join a great international team and work with amazing companies operating in the US and EU."
      />

      <CoreTeamSection
        scrollToNextSection={() => scrollToElement(jobRef.current)}
      />
      <TeamSetupSection />
      <JobSection ref={jobRef} />
    </Layout>
  )
}

export default TeamPage
