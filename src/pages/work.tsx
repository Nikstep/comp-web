import React from 'react'
import { Layout } from '../components/Layout'
import SEO from '../components/Seo'
import { CaseStudySection } from '../components/work/CaseStudySection'

const CaseStudiesPage: React.FC = () => (
  <Layout>
    <SEO
      title="Our Work"
      description="Over $1 billion in new value across 70+ products built"
    />

    <CaseStudySection isCaseStudyPage />
  </Layout>
)

export default CaseStudiesPage
