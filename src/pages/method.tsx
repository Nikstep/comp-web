import React from 'react'
import { Layout } from '../components/Layout'
import SEO from '../components/Seo'
import { StageSection } from '../components/methodGuide/StageSection'
import { WorkshopSection } from '../components/methodGuide/WorkshopSection'

const MethodPage: React.FC = () => (
  <Layout>
    <SEO
      title="U+Method"
      description="The U+Method is a step-by-step product development methodology that focuses on front–loading the risky parts of product development before starting large build–outs."
    />

    <StageSection />
    <WorkshopSection />
  </Layout>
)

export default MethodPage
