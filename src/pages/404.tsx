import React from 'react'
import styled from 'styled-components'
import { Layout } from '../components/Layout'
import SEO from '../components/Seo'
import { VerticalButton } from '../components/VerticalButton'
import { SCREEN, COLORS } from '../constants'

const Container = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: flex-end;
  height: calc(100vh - 14rem);

  ${SCREEN.ABOVE_TABLET} {
    height: calc(100vh - 20rem);
  }
`

const HeadingContainer = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: flex-end;
`

const Heading = styled.h1`
  color: ${COLORS.BLACK};
  font-size: calc(12vw);
  white-space: pre;

  span {
    color: ${COLORS.BLUE};
  }

  ${SCREEN.ABOVE_MOBILE} {
    font-size: 8rem;
    font-size: min(8rem, 11vw);

    @media (orientation: landscape) {
      font-size: 5rem;
    }
  }

  ${SCREEN.ABOVE_TABLET} {
    font-size: 10rem;
  }
`

const StyledVerticalButton = styled(VerticalButton)`
  position: relative;
  top: -10rem;
  right: 5rem;
  display: flex;
  padding-bottom: 1rem;

  ${SCREEN.ABOVE_MOBILE} {
    position: static;
    padding-bottom: 0;
  }
`

const NotFoundPage: React.FC = () => (
  <Layout>
    <SEO title="404: Not found" />

    <Container>
      <HeadingContainer>
        <Heading>
          <span>404:</span> {'\n'}
          Page not found
        </Heading>
        <StyledVerticalButton link={'/'}>Back to main</StyledVerticalButton>
      </HeadingContainer>
    </Container>
  </Layout>
)

export default NotFoundPage
